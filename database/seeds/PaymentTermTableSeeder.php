<?php

use Illuminate\Database\Seeder;
use App\Model\PaymentTerm;

class PaymentTermTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pt = ['Cash On Delivery', 'Open Account'];
        foreach($pt as $p){
            PaymentTerm::create([
                'name' => $p
            ]);
        }
    }
}
