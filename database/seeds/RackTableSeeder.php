<?php

use App\Model\Rack;
use Illuminate\Database\Seeder;

class RackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {
            Rack::create([
                'name' => 'Rack ' . $i,
                'rows' => 5,
                'columns' => 5,
                'rackable_type' => 'App\Model\Branch',
                'rackable_id' => 99,
            ]);
        }

    }
}
