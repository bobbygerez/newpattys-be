<?php

use Illuminate\Database\Seeder;
use App\Model\CreditCard;

class CreditCardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i=0; $i < 5;$i++){
            CreditCard::create([
                'name' => 'credit card ' . $i,
                'card_no' => $i.$i.$i.$i,
            ]); 
        }
    }
}
