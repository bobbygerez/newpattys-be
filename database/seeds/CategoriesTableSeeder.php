<?php

use Illuminate\Database\Seeder;
use App\Model\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Stock', 'Non-stock'];

        $categories1 = ['Ingredients', 'For Sale'];

        $categroiesAdjusted =  ['Products', 'Non-Bread'];

        $categories2 = ['Supplies', 'Equipment'];

        $ingredients = ['Flour', 'shortening', 'leavener', 'yeast', 'liquids'];

        $categories4 = ['biscuit', 'bread', 'brownie', 'cake', 'casserole', 'cookie', 'cracker', 'custard', 'milk', 'pastry', 'pie', 'pizza', 'pudding', 'Roast', 'Tart', 'Twice baked foods', 'Viennoiserie'];

        $categories5 = ['Drinks'];
        $categories6 = ['Bakery Boxes and Containers', 'Bakery Cases, Displays, and Storage Bins', 'Cake Decorating Supplies', 'disposable bakeshop supplies', 'Measuring and Mixing Supplies'];

        $categories7 = ['oven', 'mixers', 'holding / proofing cabinet', 'racks', 'slicers'];

        $flour = ['bread flour', 'cake flour', 'all purpose', 'self rising flour', 'rye flour'];

        $shortening = ['ultrafine', 'granulated', 'refined', 'brown'];

        $leavener = ['baking powder', 'baking soda'];

        $yeast = ['compressed', 'dry'];

        $liquids = ['water', 'milk'];

        $bread = ['wheat', 'whole'];

        $cake = ['shortened', 'layer', 'pound', 'FOAM AND SPONGE', 'FLOURLESS OR LOW-FLOUR CAKES', 'TORTES', 'UNBAKED CAKES', 'YEAST CAKES '];

        $drinks = ['Softdrinks', 'Mineral Water', 'Distelled water', 'Coffe'];

        $categories31 = ['box', 'pads', 'boards', 'packaging', 'plastic bag', 'paper bag'];

        $categories32 = ['combination', 'unrefrigerated', 'refrigerated', 'display stand'];

        $categories33 = ['pastry bags', 'cookie', 'pastry cutters', 'garnishing tools', 'pastry tube sets', 'cake decoarting stand', 'cake levelers'];

        $categories34 = ['aluminom foil cakes pans & Lids', 'aluminom foil pie Pans', 'aluminom foil muffin oans', 'paper & pan liners', 'baking cups', 'paper hot cups', 'cling wrap and Plastic Food wrap', 'bun pan covers' ];

        $categories35 = ['ingredients bowl', 'measuring cups', 'measuring spoons', 'mixing bowls'];

       

        foreach($categories as $category){

            Category::create([
                    'name' => $category,
                    'desc' => $category,
                    'parent_id' => 0
                ]);
        }

        foreach($categories1 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 1
            ]);
        }

         foreach($categroiesAdjusted as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 4
            ]);
        }

        foreach($categories2 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 2
            ]);
        }

        foreach($ingredients as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 3
            ]);
        }

        foreach($categories4 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 5
            ]);
        }

        foreach($categories5 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 6
            ]);
        }

        foreach($categories6 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 7
            ]);
        }

        foreach($categories7 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 8
            ]);
        }

        foreach($flour as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 9
            ]);
        }

        foreach($shortening as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 10
            ]);
        }

        foreach($leavener as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 11
            ]);
        }

        foreach($yeast as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 12
            ]);
        }

        foreach($liquids as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 13
            ]);
        }

        foreach($bread as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 15
            ]);
        }

        foreach($cake as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 17
            ]);
        }

        foreach($drinks as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 31
            ]);
        }

        foreach($categories31 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 32
            ]);
        }

        foreach($categories32 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 33
            ]);
        }

        foreach($categories33 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 34
            ]);
        }

        foreach($categories34 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 35
            ]);
        }

        foreach($categories35 as $c){

            Category::create([
                'name' => $c,
                'desc' => $c,
                'parent_id' => 36
            ]);
        }
        
    }
}
