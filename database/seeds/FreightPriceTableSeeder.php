<?php

use Illuminate\Database\Seeder;
use App\Model\FreightPrice;

class FreightPriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i=1; $i < 10; $i++){

            FreightPrice::create([
                'freightable_id' => 1,
                'freightable_type' => 'App\Model\Company',
                'from_id' => 1,
                'from_type' => 'App\Model\Branch',
                'to_id' => $i,
                'to_type' => 'App\Model\Branch',
                'price' => rand(100, 500)

            ]);
        }
    }
}
