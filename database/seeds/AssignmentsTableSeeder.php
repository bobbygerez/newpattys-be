<?php

use Illuminate\Database\Seeder;
use App\Model\Assignment;

class AssignmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Assignment::create([
            'user_id' => 1,
            'assignmentable_type' => 'App\Model\Branch',
            'assignmentable_id' => 1,
            'is_default' => 0
        ]);

        Assignment::create([
            'user_id' => 1,
            'assignmentable_type' => 'App\Model\Company',
            'assignmentable_id' => 1,
            'is_default' => 1
        ]);
        
    }
}
