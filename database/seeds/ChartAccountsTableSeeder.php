<?php

use App\Imports\ChartAccountImport;
use Illuminate\Database\Seeder;
use App\Model\ChartAccount;
class ChartAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($x = 1; $x <= 1; $x++) {
            Excel::import(new ChartAccountImport($x), public_path() . '/csv/chartofaccounts.csv');
        }

    }
}
