<?php

use Illuminate\Database\Seeder;
use App\Model\EmployeeType;
class EmployeeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employeeTypes = ['Full-time', 'Part-time', 'Casual', 'Fixed term', 'Shift workers', 'Daily hire', 'Weekly hire', 'Probation', 'Outworkers'];

        foreach($employeeTypes as $t){
            EmployeeType::create([
                'name' => $t,
                'desc' => $t
            ]);
        }
    }
}
