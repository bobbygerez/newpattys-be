<?php

use Illuminate\Database\Seeder;
use App\Model\Customer;
use App\Model\Address;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($i=0; $i <= 1; $i++){

            $customer = Customer::create([
                'customerable_id' => 1,
                'customerable_type' => 'App\Model\Company',
                'name' => 'Walk-in'. $i,
                'desc' => 'Walk-in' . $i
            ]);
            
        }
    }
}
