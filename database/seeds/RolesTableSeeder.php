<?php

use App\Model\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['CloudProCorp Admin'];

        foreach ($roles as $value) {
            $role = Role::create([
                'name' => $value,
                'roleable_id' => 1,
                'roleable_type' => 'App\Model\Apec',
                'parent_id' => 0,

            ]);
        }

    }
}
