<?php

use Illuminate\Database\Seeder;
use App\Model\Apec;

class ApecTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $apecs = ['CloudProCorp'];

        foreach($apecs as $apec){
            Apec::create([
                'name' => $apec
            ]);
        }
    }
}
