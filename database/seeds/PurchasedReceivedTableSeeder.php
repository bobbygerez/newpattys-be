<?php
use Illuminate\Database\Seeder;
use App\Model\PurchaseReceived;
use Carbon\Carbon;
class PurchasedReceivedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        for ($i=1; $i < 1 ; $i++) { 
            $purchaseReceived = PurchaseReceived::create([
                'purchasable_id' => rand(1, 2),
                'purchasable_type' => 'App\Model\Branch',
                'sale_invoice_id' => rand(1, 294),
                'chart_account_id' => rand(1, 569),
                'freight' => rand(1, 20),
                'discount' => rand(1, 100),
                'grand_total' => rand(5, 2000),
                'received_date' => Carbon::now(),
                'received_by' => rand(1, 102),
                'token' => $i . str_random(60) 
            ]);
            
            $purchaseReceived->products()->attach($purchaseReceived->id,[
                'product_id' => rand(1, 784),
                'purchase_received_id' => $purchaseReceived->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000),
            ]);
            $purchaseReceived->products()->attach($purchaseReceived->id,[
                'product_id' => rand(1, 784),
                'purchase_received_id' => $purchaseReceived->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000),
            ]);
            $purchaseReceived->products()->attach($purchaseReceived->id,[
                'product_id' => rand(1, 784),
                'purchase_received_id' => $purchaseReceived->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000),
            ]);

            $purchaseReceived = PurchaseReceived::create([
                'purchasable_id' => $i,
                'purchasable_type' => 'App\Model\Branch',
                'sale_invoice_id' => rand(1, 294),
                'chart_account_id' => rand(1, 569),
                'freight' => rand(1, 20),
                'discount' => rand(1, 100),
                'grand_total' => rand(5, 2000),
                'received_date' => Carbon::now(),
                'received_by' => rand(1, 102),
                'token' => $i . str_random(60) 
            ]);
            
            $purchaseReceived->products()->attach($purchaseReceived->id,[
                'product_id' => rand(1, 784),
                'purchase_received_id' => $purchaseReceived->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000),
            ]);
            $purchaseReceived->products()->attach($purchaseReceived->id,[
                'product_id' => rand(1, 784),
                'purchase_received_id' => $purchaseReceived->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000),
            ]);
            $purchaseReceived->products()->attach($purchaseReceived->id,[
                'product_id' => rand(1, 784),
                'purchase_received_id' => $purchaseReceived->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000),
            ]);
        }
      
    }
}