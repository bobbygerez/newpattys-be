<?php

use App\Model\Address;
use App\Model\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $user = User::create([
            'userable_id' => 1,
            'userable_type' => 'App\Model\Company', 
            'information_id' => rand(1, 101),
            'username' => 'super-admin',
            'status' => 1,
            'email' => 'superAdmin@pattys.com',
            'password' => Hash::make('23456789'),

        ]);

        $user->roles()->attach($user->id, [
                'user_id' => $user->id,
                'role_id' => 1,
            ]);

       
    }
}
