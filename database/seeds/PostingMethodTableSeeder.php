<?php

use Illuminate\Database\Seeder;
use App\Model\PostingMethod;

class PostingMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pm = ['Real Time', 'Batch'];

        foreach($pm as $v){
            PostingMethod::create([
                'name' => $v
            ]);
        }
    }
}
