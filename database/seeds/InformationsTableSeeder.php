<?php

use Illuminate\Database\Seeder;
use App\Model\Information;
use App\Model\Address;

class InformationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=1; $i < 102 ; $i++) { 
            Information::create([
                'informationable_id' => rand(1, 3),
                'informationable_type' => 'App\Model\Branch',
                'firstname' => $faker->firstName('male'|'female'),
                'lastname' => $faker->lastName,
                'middlename' => $faker->firstNameFemale,
                'gender_id' => rand(1, 2),
                'birthdate' => $faker->dateTimeThisCentury->format('Y-m-d'),
                'mobile' => $faker->phoneNumber,
                'nationality' => 'Filipino',
                'civil_status_id' => rand(1, 4)
            ]);
        }

        for ($i = 1; $i < 102; $i++) {

            $information = Information::find($i);
            $newAddress = new Address();
            $newAddress->street_lot_blk = $faker->streetAddress;
            $newAddress->latitude = $faker->latitude($min = 9, $max = 11);
            $newAddress->longitude = $faker->longitude($min = 122, $max = 124);
            $newAddress->country_id = 173;
            $newAddress->region_id = rand(1, 17);
            $newAddress->province_id = rand(1, 88);
            $newAddress->city_id = rand(1, 1647);
            $newAddress->brgy_id = rand(1, 42029);
            $information->address()->save($newAddress);

        }
        
    }
}
