<?php
use App\Model\AccessRight;
use App\Model\AccountingStandard;
use App\Model\Address;
use App\Model\Apec;
use App\Model\Area;
use App\Model\AreaBranch;
use App\Model\BankAccount;
use App\Model\Branch;
use App\Model\BranchType;
use App\Model\BusinessInfo;
use App\Model\BusinessType;
use App\Model\Category;
use App\Model\ChartAccount;
use App\Model\CivilStatus;
use App\Model\Company;
use App\Model\Designation;
use App\Model\EmployeeStatus;
use App\Model\EmployeeType;
use App\Model\Gender;
use App\Model\Holding;
use App\Model\Image;
use App\Model\Ingredient;
use App\Model\Item;
use App\Model\Menu;
use App\Model\ModelListing;
use App\Model\OtherVendor;
use App\Model\Package;
use App\Model\PaymentMethod;
use App\Model\PaymentTerm;
use App\Model\Rack;
use App\Model\Relationship;
use App\Model\Role;
use App\Model\ShippingMethod;
use App\Model\ShippingTerm;
use App\Model\TAccount;
use App\Model\Tax;
use App\Model\Trademark;
use App\Model\User;
use App\Model\VatType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Rack::truncate();
        ModelListing::truncate();
        AreaBranch::truncate();
        Area::truncate();
        PaymentTerm::truncate();
        ShippingTerm::truncate();
        ShippingMethod::truncate();
        BranchType::truncate();
        Apec::truncate();
        Relationship::truncate();
        Tax::truncate();
        PaymentMethod::truncate();
        CivilStatus::truncate();
        Gender::truncate();
        BusinessType::truncate();
        VatType::truncate();
        Address::truncate();
        User::truncate();
        Role::truncate();
        Image::truncate();
        Menu::truncate();
        Holding::truncate();
        Branch::truncate();
        Image::truncate();
        BusinessInfo::truncate();
        AccessRight::truncate();
        Company::truncate();
        BankAccount::truncate();
        Category::truncate();
        Package::truncate();
        Trademark::truncate();
        OtherVendor::truncate();
        Item::truncate();
        Ingredient::truncate();
        AccountingStandard::truncate();
        ChartAccount::truncate();
        TAccount::truncate();
        Designation::truncate();
        EmployeeStatus::truncate();
        EmployeeType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(AddColumnTableSeeder::class);
        $this->call(DenominationTableSeeder::class);
        $this->call(CustomerRewardTableSeeder::class);
        $this->call(CreditCardTableSeeder::class);
        $this->call(RackTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(PaymentTermTableSeeder::class);
        $this->call(ShippingTermsTableSeeder::class);
        $this->call(ShippingMethodTableSeeder::class);
        $this->call(BranchTypesTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
        $this->call(ApecTableSeeder::class);
        $this->call(DesignationTableSeeder::class);
        $this->call(RelationshipTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(TaxesTableSeeder::class);
        $this->call(TaxTypesTableSeeder::class);
        $this->call(AccountingMethodsTableSeeder::class);
        $this->call(TaccountsTableSeeder::class);
        $this->call(TransactionTypesTableSeeder::class);
        $this->call(CivilStatusTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(VatTypesTableSeeder::class);
        $this->call(BusinessTypesTableSeeder::class);
        $this->call(InformationsTableSeeder::class);
        $this->call(EmployeeStatusTableSeeder::class);
        $this->call(EmployeeTypeTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(HoldingsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(BranchesTableSeeder::class);
        $this->call(AreaBranchTableSeeder::class);
        $this->call(AccessRightTableSeeder::class);
        $this->call(AccessableTableSeeder::class);
        $this->call(AssignmentsTableSeeder::class);
        $this->call(AccountingStandardsTableSeeder::class);
        $this->call(ChartAccountsTableSeeder::class);
        $this->call(PackagesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CompanyItemTableSeeder::class);
        $this->call(BusinessInfosTableSeeder::class);
        $this->call(FreightPriceTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
        $this->call(PurchaseRequestTableSeeder::class);
        $this->call(CompanyItemVendorRankTableSeeder::class);
        $this->call(PurchaseStatusTableSeeder::class);
        $this->call(ModelListingTableSeeder::class);
        $this->call(PostingMethodTableSeeder::class);

    }
}
