<?php

use Illuminate\Database\Seeder;
use App\Model\PurchaseOrder;
use Carbon\Carbon;
class PurchaseOrderTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        for ($i=1; $i < 1 ; $i++) { 
            $purchase = PurchaseOrder::create([
                'purchase_request_id' => rand(1, 294),
                'purchasable_id' => $i,
                'purchasable_type' => 'App\Model\Branch',
                'freight' => rand(5, 500),
                'sub_total' => rand(5, 500),
                'vat_total' => rand(1, 50),
                'grand_total' => rand(5, 2000),
                'approved_by' => $i
            ]);
            $rand = rand(1,2);
            $purchase->items()->attach($purchase->id,[
                'item_id' => $i,
                'purchase_order_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 500),
                'freight' => rand(100, 200),
                'vendor_approved_by' =>  $rand === 1 ? null : $faker->firstName('male'|'female') . ' ' . $faker->firstNameFemale . ' ' . $faker->lastName ,
                'vendor_date_approved' =>  $rand === 1 ? null : Carbon::now(),
                'vendor_date_delivery' => $rand === 1 ? null : Carbon::now()
            ]);
            $purchase->items()->attach($purchase->id,[
                'item_id' => $i,
                'purchase_order_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 500),
                'freight' => rand(100, 200),
                'vendor_approved_by' =>  $rand === 1 ? null : $faker->firstName('male'|'female') . ' ' . $faker->firstNameFemale . ' ' . $faker->lastName ,
                'vendor_date_approved' =>  $rand === 1 ? null : Carbon::now(),
                'vendor_date_delivery' => $rand === 1 ? null : Carbon::now()
            ]);
            $purchase->items()->attach($purchase->id,[
                'item_id' => $i,
                'purchase_order_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 500),
                'freight' => rand(100, 200),
                'vendor_approved_by' =>  $rand === 1 ? null : $faker->firstName('male'|'female') . ' ' . $faker->firstNameFemale . ' ' . $faker->lastName ,
                'vendor_date_approved' =>  $rand === 1 ? null : Carbon::now(),
                'vendor_date_delivery' => $rand === 1 ? null : Carbon::now()
            ]);
        }


    }
}