<?php

use App\Model\Menu;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $menus = ['System Settings', 'Inventories', 'Productions', 'General Ledgers', 'Breeds', 'Accounting', 'Purchases', 'Company', 'Branch', 'Sales', 'CRM', 'HRIS', 'Reports'];
        $sub_menu1 = [
            'Users', 'Roles', 'Access Rights', 'Menus', 'Holdings', 'Companies', 'Branches', 'Trademarks', 'Franchisees', 'Logistics', 'Commissaries', 'Company Statutory Table', 'Payroll Setup', 'Customers', 'Sales Representative', 'Tax Codes', 'Taxes Authoritie', 'Vendor', 'Inventory Items', 'Employees', 'Chart of Accounts', 'Item Prices', 'Employee Billing Rates', 'Sub-Contractor', 'Jobs', 'KYC', 'User Type'];

        $sub_menu2 = [
            'Other Vendors', 'Packages', 'Categories', 'Items', 'Purchase Request',
        ];
        $sub_menu3 = [
            'Ingredients', 'Production Boards', 'Scalers', 'Mixers and Molders', 'Proofing', 'Ovens',
        ];

        $sub_menu4 = [
            'Transactions', 'Receipts', 'Debit Memo', 'Disbursement Journal', 'Credit Memo', 'General Journal',
        ];

        $sub_menu5 = [
            'Company Items', 'Vendors', 'Freight Prices', 'Purchase Requests', 'Canvasses', 'Purchase Orders', 'Purchase Received',
        ];

        $sub_menu6 = [
            'Chart of Accounts', 'Categories', 'Areas', 'Racks',
        ];

        $sub_menu7 = [
            'Branch Items',
            'Branch Chart of Accounts',
            'Transaction Setup',
        ];

        $sub_menu8 = [
            'Point of Sales', 'Shifting/Turnover', 'Product transfer', 'Sales Order',
        ];
        foreach ($menus as $value) {

            Menu::create([
                'parent_id' => 0,
                'path' => $value,
                'name' => $value,
            ]);
        }
        foreach ($sub_menu1 as $value) {
            Menu::create([
                'parent_id' => 1,
                'path' => 'system-settings/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }
        foreach ($sub_menu2 as $value) {
            Menu::create([
                'parent_id' => 2,
                'path' => 'inventories/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }
        foreach ($sub_menu3 as $value) {
            Menu::create([
                'parent_id' => 3,
                'path' => 'productions/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }
        foreach ($sub_menu4 as $value) {
            Menu::create([
                'parent_id' => 4,
                'path' => 'general-ledgers/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }

        foreach ($sub_menu5 as $value) {
            Menu::create([
                'parent_id' => 7,
                'path' => 'purchases/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }

        foreach ($sub_menu6 as $value) {
            Menu::create([
                'parent_id' => 8,
                'path' => 'company/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }

        foreach ($sub_menu7 as $value) {
            Menu::create([
                'parent_id' => 9,
                'path' => 'branch/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }

        foreach ($sub_menu8 as $value) {
            Menu::create([
                'parent_id' => 10,
                'path' => 'sales/' . str_slug($value, '-'),
                'name' => $value,
            ]);
        }

    }
}
