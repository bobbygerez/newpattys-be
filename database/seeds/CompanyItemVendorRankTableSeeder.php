<?php

use Illuminate\Database\Seeder;
use App\Model\CompanyItemVendorRank;

class CompanyItemVendorRankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i=1; $i < 19; $i++){

            CompanyItemVendorRank::create([
                'company_item_id' => $i,
                'vendorable_id' => 1,
                'vendorable_type' => 'App\Model\Branch',
                'rank' => 1,
                'percentage' => rand(1, 40)
            ]);
            
            CompanyItemVendorRank::create([
                'company_item_id' => $i,
                'vendorable_id' => 1,
                'vendorable_type' => 'App\Model\Vendor',
                'rank' => 2,
                'percentage' => rand(1, 40)
            ]);
        }
    }
}
