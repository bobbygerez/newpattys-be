<?php

use Illuminate\Database\Seeder;
use App\Model\AreaBranch;

class AreaBranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($x = 1; $x < 97; $x++){
                AreaBranch::create([
                'area_id' => rand(1, 14),
                'branch_id' => $x
                ]);

        }
    }
}
