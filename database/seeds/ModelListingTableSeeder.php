<?php

use Illuminate\Database\Seeder;
use App\Model\ModelListing;

class ModelListingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modelListings = [
            'App\Model\PurchaseReceived' => 'Purchase Received'
        ];

        foreach($modelListings as $k => $v){

            ModelListing::create([
                'model' => $k,
                'name' => $v
            ]);
        }
    }
}
