<?php

use Illuminate\Database\Seeder;
use App\Model\BranchType;

class BranchTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branchTypes = ['Branch', 'Logistic', 'Commissary'];

        foreach($branchTypes as $type){
            BranchType::create([
                'name' => $type
            ]);
        }
    }
}
