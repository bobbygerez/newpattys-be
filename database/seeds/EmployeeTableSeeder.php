<?php

use Illuminate\Database\Seeder;
use App\Model\Employee;
use Carbon\Carbon;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $faker = Faker\Factory::create();
        for ($i = 1; $i < 102; $i++) {
            Employee::create([
                'employable_id' => rand(1, 3),
                'employable_type' => 'App\Model\Branch',
                'salary_rate' => rand(387, 1000),
                'date_hired' => Carbon::now()->addDays(rand(1, 5)),
                'date_expired' => Carbon::now()->addDays(rand(6, 29)),
                'pag_ibig' => $faker->text($maxNbChars = 20),
                'sss' => $faker->text($maxNbChars = 20),
                'tin' => $faker->text($maxNbChars = 20),
                'prefix_employee_no' => $faker->text($maxNbChars = 5),
                'employee_no' => $faker->randomNumber($nbDigits = NULL, $strict = false),
                'employee_type_id' => rand(1, 9),
                'employee_status_id' => rand(1, 4),
                'designation_id' => rand(1, 6),
                'information_id' => $i
            ]);

        }
    }
}
