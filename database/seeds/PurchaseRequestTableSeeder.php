<?php

use App\Model\PurchaseRequest;
use Illuminate\Database\Seeder;

class PurchaseRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        $dateString = date('Ymd');
        $branchNumber = 1;
        $requestId = PurchaseRequest::max('id') + 1;

        for ($i = 1; $i <= 1; $i++) {
            $purchase = PurchaseRequest::create([
                'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                'purchase_request_no' => $dateString . '-' . $branchNumber . '-' . $requestId,
                'purchase_request_code' => str_random(32),
                'purchasable_id' => 1,
                'purchasable_type' => 'App\Model\Branch',
                'prepared_by' => 1,
                'checked_by' => 1,
                'approved_by' => 1,
                'remarks' => 'Remarks' . $i,
            ]);

            $purchase = PurchaseRequest::find($purchase->id);
            $purchase->purchaseRequestCompanyItems([
                'company_item_id' => rand(1, 19),
                'purchase_request_id' => $purchase->id,
                'qty' => rand(1, 10),
            ]);
            $purchase->purchaseRequestCompanyItems([
                'company_item_id' => rand(1, 19),
                'purchase_request_id' => $purchase->id,
                'qty' => rand(1, 10),
            ]);
            $purchase->purchaseRequestCompanyItems([
                'company_item_id' => rand(1, 19),
                'purchase_request_id' => $purchase->id,
                'qty' => rand(1, 10),
            ]);
        }

    }
}
