<?php

use Illuminate\Database\Seeder;
use App\Model\AccessRight;
use App\Model\Company;
use App\Model\Holding;
use App\Model\Menu;
use App\Model\Branch;
use App\Model\Trademark;
use App\Model\Franchisee;
use App\Model\Logistic;
use App\Model\Commissary;
use App\Model\OtherVendor;
use App\Model\Item;
use App\Model\Ingredient;

class AccessRightTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $accessRights = ['Index', 'Create', 'View', 'Update', 'Delete', 'Restore', 'Override'];

        foreach ($accessRights as $value) {
        	
        	$accessRight = AccessRight::create([
        			'name' => $value
        		]);
            
        }
        
    }
}
