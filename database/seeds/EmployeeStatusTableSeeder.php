<?php

use Illuminate\Database\Seeder;
use App\Model\EmployeeStatus;

class EmployeeStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employeeStatus = ['Active', 'Inactive', 'Resigned', 'Terminated'];
        foreach($employeeStatus as $e){
            EmployeeStatus::create([
                'name' => $e,
                'desc' => $e
            ]);
        }
    }
}
