<?php

use Illuminate\Database\Seeder;
use App\Model\Area;
class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i = 1; $i < 15; $i++){
            Area::create([
                'name' => 'area-'. $i,
                'areable_id' => 1,
                'areable_type' => 'App\Model\Company'
            ]);
        }
    }
}
