<?php

use Illuminate\Database\Seeder;
use App\Model\ShippingTerm;

class ShippingTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $st = ['Pick-up', 'Delivery'];
        foreach($st as $s){
            ShippingTerm::create([
                'name' => $s
            ]);
        }
    }
}
