<?php

use Illuminate\Database\Seeder;
use App\Model\VendorProduct;

class VendorProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i=1; $i < 15; $i++) { 
            VendorProduct::create([
                'vendor_id' => 1,
                'company_item_id' => $i,
                'price' => rand(30, 200)
            ]);
        }
    }
}
