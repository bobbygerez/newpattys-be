<?php

use Illuminate\Database\Seeder;
use App\Model\PurchaseStatus;

class PurchaseStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $purchaseStatus = ['Waiting Approval', 'Waiting for Delivery', 'Close', 'Force Close'];

        foreach($purchaseStatus as $status){
            PurchaseStatus::create([
                'name' => $status
            ]);
        }
    }
}
