<?php

use Illuminate\Database\Seeder;
use App\Model\SaleInvoice;
use Carbon\Carbon;

class SaleInvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
       
        

        for ($i=1; $i <= 1 ; $i++) { 
            $purchase = SaleInvoice::create([
                'sale_order_id' => $i,
                'sale_invoice_no' => rand(6000, 8000),
                'sale_invoice_code' => str_replace(' ', '', str_replace('0.', '', microtime() . uniqid(true))),
                'salable_id' => $i,
                'salable_type' => 'App\Model\Branch',
                'freight' => rand(1, 50),
                'sub_total' => rand(5, 500),
                'vat_total' => rand(1, 50),
                'tax_total' => rand(1, 50),
                'grand_total' => rand(5, 2000),
                'date_due' => Carbon::now(),
                'prepared_by' => $i,
                'approved_by' => $i,
                'verified_by' => $i,
                'is_posted' => false
            ]);
            $rand = rand(1,2);
            $purchase->products()->attach($purchase->id,[
                'product_id' => $i,
                'sale_invoice_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 98),
                'sale_invoice_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 98),
                'sale_invoice_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);

            $purchase = SaleInvoice::create([
                'sale_order_id' => $i,
                'sale_invoice_no' => rand(6000, 8000),
                'sale_invoice_code' => str_replace(' ', '', str_replace('0.', '', microtime() . uniqid(true))),
                'salable_id' => rand(1,2),
                'salable_type' => 'App\Model\Branch',
                'freight' => rand(1, 50),
                'sub_total' => rand(5, 500),
                'vat_total' => rand(1, 50),
                'tax_total' => rand(1, 50),
                'grand_total' => rand(5, 2000),
                'date_due' => Carbon::now(),
                'prepared_by' => $i,
                'approved_by' => $i,
                'verified_by' => $i,
                'is_posted' => false
            ]);
            $rand = rand(1,2);
            $purchase->products()->attach($purchase->id,[
                'product_id' => $i,
                'sale_invoice_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 98),
                'sale_invoice_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 98),
                'sale_invoice_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
        }

    }
}
