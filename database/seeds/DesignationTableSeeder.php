<?php

use Illuminate\Database\Seeder;
use App\Model\Designation;

class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designations = ['Baker', 'Scaler', 'Cashier', 'Checker', 'Frontliner', 'Supervisor'];

        foreach($designations as $d){
            Designation::create([
                'name' => $d,
                'desc' => $d
            ]);
        }
    }
}
