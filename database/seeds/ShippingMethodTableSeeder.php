<?php

use Illuminate\Database\Seeder;
use App\Model\ShippingMethod;

class ShippingMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $shippingMethods = ['Land', 'Air', 'Sea'];
        foreach($shippingMethods as $s){
            ShippingMethod::create([
                'name' => $s
            ]);
        }
    }
}
