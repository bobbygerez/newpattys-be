<?php

use App\Imports\LendersImport;
use Illuminate\Database\Seeder;

class LendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new LendersImport, public_path() . '/csv/lending.csv');
    }
}
