<?php

use Illuminate\Database\Seeder;
use App\Model\SaleOrder;

class SaleOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
       

        for ($i=1; $i < 1 ; $i++) { 
            $purchase = SaleOrder::create([
                'purchase_order_id' => $i,
                'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                'sale_order_no' => rand(6000, 8000),
                'sale_order_code' => str_replace('0.', '', microtime() . uniqid(true)),
                'orderable_id' => $i,
                'orderable_type' => 'App\Model\Branch',
                'prepared_by' => $i,
                'freight' => rand(1, 50),
                'sub_total' => rand(5, 500),
                'vat_total' => rand(1, 50),
                'tax_total' => rand(1, 50),
                'grand_total' => rand(5, 2000),
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 194),
                'sale_order_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 500),
                'freight' => rand(100, 200)
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 194),
                'sale_order_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
            $purchase->products()->attach($purchase->id,[
                'product_id' => rand(1, 194),
                'sale_order_id' => $purchase->id,
                'qty' => rand(1, 50),
                'price' => rand(5, 5000),
                'freight' => rand(100, 2000)
            ]);
        }

    }
}
