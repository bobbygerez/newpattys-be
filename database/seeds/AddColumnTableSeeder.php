<?php

use Illuminate\Database\Seeder;

class AddColumnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ADDED LATELY
        Schema::table('company_items', function ($table) {
            if (!Schema::hasColumn('company_items', 'desc')) {
                $table->integer('desc')->nullable();
            }

        });

    }
}
