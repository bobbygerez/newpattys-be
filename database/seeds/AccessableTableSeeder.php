<?php

use Illuminate\Database\Seeder;
use App\Model\Accessable;
use App\Model\AccessRight;
use App\Model\Role;
use App\Model\Menu;
class AccessableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
             //Seeding Accessables
            for($m=1; $m <= 59; $m++) {

                for($aaa=1; $aaa<=5; $aaa++){
                        $ac = Accessable::create([
                            'access_right_id' => $aaa,
                            'accessable_id' => 1,
                            'accessable_type' => 'App\Model\Company',
                            'menu_id' => $m
                        ]);
                    
                }
                

            }
            for($m=1; $m <= 59; $m++) {

                for($aaa=1; $aaa<=5; $aaa++){
                        $ac = Accessable::create([
                            'access_right_id' => $aaa,
                            'accessable_id' => 1,
                            'accessable_type' => 'App\Model\Branch',
                            'menu_id' => $m
                        ]);
                    
                }
                

            }

            for($a=1; $a<=589; $a++){
                for($r=1; $r<=2; $r++){
                    $role = Role::find($r);
                    if($role != null){
                        $role->accessables()->attach($role->id, [
                            'role_id' => $role->id,
                            'accessable_id' => $a
                        ]);
                    }
                   
                }
            }

            

         
        
    }
}
