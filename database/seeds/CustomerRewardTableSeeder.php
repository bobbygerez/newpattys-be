<?php

use Illuminate\Database\Seeder;
use App\Model\CustomerReward;
class CustomerRewardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i=0; $i < 5; $i++){
            CustomerReward::create([
                'name' => 'Customer ' . $i,
                'no' => $i.$i.$i.$i
            ]);
        }
    }
}
