<?php

use Illuminate\Database\Seeder;
use App\Model\Denomination;

class DenominationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $denominations = [
            'Thousand' => 1000,
            'Five hundred' => 500,
            'Two hundred' => 200,
            'One hundred' => 100,
            'Fifty' => 50,
            'Twenty' => 20,
            'Ten' => 10,
            'Five' => 5,
            'Peso' => 1,
            'Twenty-five cent' => '.25'
        ];

        foreach($denominations as $k => $v){
            Denomination::create([
                'name' => $k,
                'amount' => $v
            ]);
        }
    }
}
