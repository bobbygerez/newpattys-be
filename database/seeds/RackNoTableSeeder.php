<?php

use Illuminate\Database\Seeder;
use App\Model\RackNo;

class RackNoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i=0; $i < 100; $i++){
            RackNo::create([
                'rack_id' => rand(1, 19),
                'no' => $i
            ]);
        }
    }
}
