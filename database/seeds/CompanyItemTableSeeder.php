<?php

use App\Model\CompanyItem;
use Illuminate\Database\Seeder;

class CompanyItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 1; $i < 20; $i++) {
            $ci = new CompanyItem();
            $ci->category_id = rand(1, 98);
            $ci->company_id = 1;
            $ci->package_id = rand(1, 16);
            $ci->name = 'item-' . $i;
            $ci->desc = 'item-' . $i;
            $ci->amount = rand(50, 400);
            $ci->sku = str_random(10);
            $ci->barcode = $faker->ean13;
            $ci->min_qty = rand(5, 20);
            $ci->reorder_qty = rand(20, 100);
            $ci->max_qty = rand(100, 200);
            $ci->save();
        }

    }
}
