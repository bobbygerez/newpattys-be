<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('from')->unsigned()->nullable();
            $table->foreign('from')->references('id')
                ->on('branches');
            $table->integer('to')->unsigned()->nullable();
            $table->foreign('to')->references('id')
                ->on('branches');
            $table->integer('request_by')->unsigned()->nullable();
            $table->foreign('request_by')->references('id')
                ->on('users');
            $table->integer('approved_from')->unsigned()->nullable();
            $table->foreign('approved_from')->references('id')
                ->on('users');
            $table->integer('approved_to')->unsigned()->nullable();
            $table->foreign('approved_to')->references('id')
                ->on('users');
            $table->integer('prepared_by')->unsigned()->nullable();
            $table->foreign('prepared_by')->references('id')
                ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_transfers');
    }
}
