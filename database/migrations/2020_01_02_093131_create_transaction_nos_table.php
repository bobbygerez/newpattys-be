<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_nos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no')->unique();
            $table->boolean('is_completed')->default(0);
            $table->boolean('is_void')->default(0);
            $table->integer('save_by')->unsigned()->nullable();
            $table->foreign('save_by')->references('id')
                ->on('users');
            $table->integer('transact_by')->unsigned()->nullable();
            $table->foreign('transact_by')->references('id')
                ->on('users');
            $table->integer('transactable_id');
            $table->string('transactable_type');
            $table->decimal('grand_total')->default(0);
            $table->decimal('enter_amount')->default(0);
            $table->bigInteger('credit_card_id')->unsigned()->nullable();
            $table->foreign('credit_card_id')->references('id')
                ->on('credit_cards');
             $table->bigInteger('customer_reward_id')->unsigned()->nullable();
            $table->foreign('customer_reward_id')->references('id')
                ->on('customer_rewards');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_nos');
    }
}
