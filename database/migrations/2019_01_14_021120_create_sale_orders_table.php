<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_order_id')->unsigned()->nullable();
            $table->foreign('purchase_order_id')->references('id')
                ->on('purchase_orders');
            $table->string('sale_order_no')->nullable();
            $table->string('sale_order_code')->nullable();
            $table->string('name');
            $table->integer('orderable_id');
            $table->string('orderable_type');
            $table->integer('prepared_by')->unsigned()->nullable();
            $table->foreign('prepared_by')->references('id')
                ->on('users');
            $table->datetime('noted_date')->nullable();
            $table->integer('noted_by')->unsigned()->nullable();
            $table->foreign('noted_by')->references('id')
                ->on('users');
            $table->decimal('freight')->nullable();
            $table->decimal('sub_total')->nullable();
            $table->decimal('vat_total')->nullable();
            $table->decimal('tax_total')->nullable();
            $table->decimal('grand_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_orders');
    }
}
