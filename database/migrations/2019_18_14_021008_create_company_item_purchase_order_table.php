<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyItemPurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_item_purchase_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('purchase_order_no_id')->unsigned()->nullable();
            $table->foreign('purchase_order_no_id')->references('id')
                ->on('purchase_order_nos');
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items');
            $table->integer('purchase_order_id')->unsigned()->nullable();
            $table->foreign('purchase_order_id')->references('id')
                ->on('purchase_orders');
            $table->integer('qty');
            $table->decimal('price', 12, 2);
            $table->decimal('freight', 12, 2)->nullable();
            $table->decimal('tax', 12, 2)->nullable();
            $table->integer('vendorable_id');
            $table->string('vendorable_type');
            $table->integer('orderable_id');
            $table->string('orderable_type');
            $table->string('token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_item_purchase_order');
    }
}
