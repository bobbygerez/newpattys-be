<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequestCompanyItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_request_company_items', function (Blueprint $table) {
            $table->bigIncrements('id');          
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items');
            $table->integer('purchase_request_id')->unsigned()->nullable();
            $table->foreign('purchase_request_id')->references('id')
                ->on('purchase_requests');
            $table->integer('qty_left');
            $table->integer('qty_order');
            $table->integer('qty_max');
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_request_company_items');
    }
}
