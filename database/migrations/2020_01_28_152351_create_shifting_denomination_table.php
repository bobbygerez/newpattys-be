<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftingDenominationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifting_denomination', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shifting_id')->unsigned()->nullable();
            $table->foreign('shifting_id')->references('id')
                ->on('shifting');
            $table->bigInteger('denomination_id')->unsigned()->nullable();
            $table->foreign('denomination_id')->references('id')
                ->on('denominations');
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifting_denomination');
    }
}
