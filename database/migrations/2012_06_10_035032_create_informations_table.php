<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('informationable_id');
            $table->string('informationable_type');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('middlename');
            $table->integer('gender_id')->unsigned()->nullable();
            $table->foreign('gender_id')->references('id')->on('gender');
            $table->date('birthdate');
            $table->string('mobile');
            $table->string('nationality');
            $table->integer('civil_status_id')->unsigned()->nullable();
            $table->foreign('civil_status_id')->references('id')->on('civil_status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informations');
    }
}
