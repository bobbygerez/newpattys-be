<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchItemTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_item_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('transactable_id');
            $table->string('transactable_type');
            $table->bigInteger('transaction_no_id')->unsigned()->nullable();
            $table->foreign('transaction_no_id')->references('id')
                ->on('transaction_nos');
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items');
            $table->integer('qty_in')->default(0);
            $table->integer('qty_out')->default(0);
            $table->decimal('amount');
            $table->boolean('is_void')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_item_transactions');
    }
}
