<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReceivedPurchasesItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_received_purchases_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('purchase_received_id')->unsigned()->nullable();
            $table->foreign('purchase_received_id')->references('id')
                ->on('purchase_received');
            $table->bigInteger('company_item')->unsigned()->nullable();
            $table->foreign('company_item')->references('id')
                ->on('company_items');
            $table->decimal('amount');
            $table->integer('order_qty');
            $table->integer('received_qty');
            $table->integer('branch_chart_of_account_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_received_purchases_items');
    }
}
