<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_received', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('purchase_order_no_id')->unsigned()->nullable();
            $table->foreign('purchase_order_no_id')->references('id')
                ->on('purchase_order_nos');
            $table->string('purchase_received_no');
            $table->string('invoice_no')->nullable();
            $table->string('delivery_receipt_no');
            $table->integer('purchasable_id');
            $table->string('purchasable_type');
            $table->integer('vendorable_id');
            $table->string('vendorable_type');
            $table->integer('received_by')->unsigned()->nullable();
            $table->foreign('received_by')->references('id')
                ->on('users');
            $table->string('token')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_received');
    }
}
