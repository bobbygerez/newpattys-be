<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSaleOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sale_order', function (Blueprint $table) {
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')
                ->on('products');
            $table->integer('sale_order_id')->unsigned()->nullable();
            $table->foreign('sale_order_id')->references('id')
                ->on('sale_orders');
            $table->integer('qty');
            $table->decimal('price', 12, 2);
            $table->decimal('freight', 12, 2)->nullable();
            $table->decimal('tax', 12, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sale_order');
    }
}
