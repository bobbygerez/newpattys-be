<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIiProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ii_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('qty');
            $table->decimal('price');
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items');
            $table->bigInteger('invoice_internal_id')->unsigned()->nullable();
            $table->foreign('invoice_internal_id')->references('id')
                ->on('invoice_internal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ii_products');
    }
}
