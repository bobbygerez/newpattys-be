<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_receipt', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('receiptable_id');
            $table->string('receiptable_type');
            $table->string('no')->nullable();
            $table->bigInteger('invoice_internal_id')->unsigned()->nullable();
            $table->foreign('invoice_internal_id')->references('id')
                ->on('invoice_internal');
            $table->bigInteger('product_transfer_id')->unsigned()->nullable();
            $table->foreign('product_transfer_id')->references('id')
                ->on('product_transfers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_receipt');
    }
}
