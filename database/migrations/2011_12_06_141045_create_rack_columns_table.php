<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRackColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rack_columns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rack_id')->unsigned()->nullable();
            $table->foreign('rack_id')->references('id')
                ->on('racks');
            $table->integer('no');
            $table->integer('rackable_id');
            $table->string('rackable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rack_columns');
    }
}
