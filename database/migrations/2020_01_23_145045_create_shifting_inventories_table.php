<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftingInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifting_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shifting_id')->unsigned()->nullable();
            $table->foreign('shifting_id')->references('id')
                ->on('shifting');
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items'); 
            $table->integer('qty'); 
            $table->decimal('amount'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifting_inventories');
    }
}
