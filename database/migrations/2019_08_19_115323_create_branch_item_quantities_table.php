<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchItemQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_item_quantities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('branch_item_id')->unsigned()->nullable();
            $table->foreign('branch_item_id')->references('id')
                ->on('branch_items');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_item_quantities');
    }
}
