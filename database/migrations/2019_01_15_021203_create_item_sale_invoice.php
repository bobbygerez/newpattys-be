<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSaleInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_sale_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned()->nullable();
            $table->foreign('item_id')->references('id')
                ->on('items');
            $table->integer('sale_invoice_id')->unsigned()->nullable();
            $table->foreign('sale_invoice_id')->references('id')
                ->on('sale_invoices');
            $table->integer('qty');
            $table->decimal('price', 12, 2);
            $table->decimal('freight', 12, 2)->nullable();
            $table->decimal('tax', 12, 2)->nullable();
            $table->string('approved_by')->nullable();
            $table->datetime('date_approved')->nullable();
            $table->datetime('date_delivery')->nullable();
            $table->string('token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_sale_invoice');
    }
}
