<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employable_id');
            $table->string('employable_type');
            $table->decimal('salary_rate');
            $table->date('date_hired');
            $table->date('date_expired');
            $table->string('pag_ibig');
            $table->string('sss');
            $table->string('tin');
            $table->string('prefix_employee_no')->nullable();
            $table->integer('employee_no');
            $table->integer('information_id')->unsigned()->nullable();
            $table->foreign('information_id')->references('id')
                ->on('informations');
            $table->bigInteger('employee_type_id')->unsigned()->nullable();
            $table->foreign('employee_type_id')->references('id')
                ->on('employee_types');
            $table->bigInteger('employee_status_id')->unsigned()->nullable();
            $table->foreign('employee_status_id')->references('id')
                    ->on('employee_status');
            $table->bigInteger('designation_id')->unsigned()->nullable();
            $table->foreign('designation_id')->references('id')
                ->on('designations');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
