<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionSetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_setup', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('model_listing_id')->unsigned()->nullable();
            $table->foreign('model_listing_id')->references('id')
                ->on('model_listings');
            $table->string('model');
            $table->integer('transactable_id');
            $table->string('transactable_type');
            $table->integer('dr_chart_account_id')->nullable();
            $table->integer('cr_chart_account_id')->nullable();
            $table->boolean('defaultt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_setup');
    }
}
