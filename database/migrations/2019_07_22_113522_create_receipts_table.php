<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('receiptable_id');
            $table->string('receiptable_type');
            $table->integer('chart_account_id')->unsigned()->nullable();
            $table->foreign('chart_account_id')->references('id')
                ->on('chart_accounts');
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->foreign('payment_method_id')->references('id')
                ->on('payment_methods');
            $table->date('trans_date');
            $table->date('deposit_date')->nullable();
            $table->string('refnum');
            $table->string('receipt_num')->nullable();
            $table->decimal('total_amount', 12, 2)->nullable();
            $table->decimal('total_debit', 12, 2)->nullable();
            $table->decimal('total_discount', 12, 2)->nullable();
            $table->string('checknumber')->nullable();
            $table->string('remarks')->nullable();
            $table->boolean('status')->nullable()->default(1);
            $table->date('date_due')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')
                ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
