<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dr_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('qty');
            $table->decimal('price');
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items');
            $table->bigInteger('delivery_receipt_id')->unsigned()->nullable();
            $table->foreign('delivery_receipt_id')->references('id')
                ->on('delivery_receipt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dr_products');
    }
}
