<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchase_request_no')->nullable();
            $table->string('purchase_request_code')->nullable();
            $table->string('name');
            $table->integer('purchasable_id');
            $table->string('purchasable_type');
            $table->integer('prepared_by')->unsigned()->nullable();
            $table->foreign('prepared_by')->references('id')
                ->on('users');
            $table->integer('checked_by')->unsigned()->nullable();
            $table->foreign('checked_by')->references('id')
                ->on('users');
            $table->integer('approved_by')->unsigned()->nullable();
            $table->foreign('approved_by')->references('id')
                    ->on('users');
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requests');
    }
}
