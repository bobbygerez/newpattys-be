<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->foreign('branch_id')->references('id')
                ->on('branches');
            $table->bigInteger('company_item_id')->unsigned()->nullable();
            $table->foreign('company_item_id')->references('id')
                ->on('company_items');
            $table->bigInteger('rack_id')->unsigned()->nullable();
            $table->foreign('rack_id')->references('id')
                ->on('racks');
            $table->bigInteger('rack_row_id')->unsigned()->nullable();
            $table->foreign('rack_row_id')->references('id')
                ->on('rack_rows');
            $table->bigInteger('rack_column_id')->unsigned()->nullable();
            $table->foreign('rack_column_id')->references('id')
                ->on('rack_columns');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_items');
    }
}
