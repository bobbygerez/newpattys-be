<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shiftable_id');
            $table->string('shiftable_type');
            $table->integer('from')->unsigned()->nullable();
            $table->foreign('from')->references('id')
                ->on('users');
            $table->integer('to')->unsigned()->nullable();
            $table->foreign('to')->references('id')
                ->on('users');
            $table->boolean('is_accepted')->nullable();
            $table->dateTime('open')->nullable();
            $table->dateTime('close')->nullable();
            $table->decimal('cash_on_hand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifting');
    }
}
