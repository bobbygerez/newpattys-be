<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_invoices', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('salable_id');
            $table->string('salable_type');
            $table->integer('sale_order_id')->unsigned()->nullable();
            $table->foreign('sale_order_id')->references('id')
                ->on('sale_orders');
            $table->string('sale_invoice_no')->nullable();
            $table->string('sale_invoice_code')->nullable();
            $table->decimal('freight')->nullable();
            $table->decimal('sub_total')->nullable();
            $table->decimal('vat_total')->nullable();
            $table->decimal('tax_total')->nullable();
            $table->decimal('grand_total')->nullable();
            $table->integer('prepared_by')->unsigned()->nullable();
            $table->foreign('prepared_by')->references('id')
                ->on('users');
            $table->integer('approved_by')->unsigned()->nullable();
            $table->foreign('approved_by')->references('id')
                ->on('users');
            $table->integer('verified_by')->unsigned()->nullable();
            $table->foreign('verified_by')->references('id')
                    ->on('users');
            $table->string('token')->nullable();
            $table->boolean('is_posted');
            $table->date('date_due');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_invoices');
    }
}
