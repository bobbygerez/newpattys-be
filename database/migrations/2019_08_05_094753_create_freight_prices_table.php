<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreightPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freight_prices', function (Blueprint $table) {
            $table->bigIncrements('id');

            /***
             * Freight price work slow... Franchisee and Branch are mixed
             */
            $table->string('freightable_type');
            $table->integer('freightable_id');
            $table->string('from_type');
            $table->integer('from_id');
            $table->string('to_type');
            $table->integer('to_id');
            $table->bigInteger('shipping_method_id')->unsigned()->nullable();
            $table->foreign('shipping_method_id')->references('id')
                ->on('shipping_methods');
            $table->bigInteger('shipping_term_id')->unsigned()->nullable();
            $table->foreign('shipping_term_id')->references('id')
                ->on('shipping_terms');
            $table->bigInteger('payment_term_id')->unsigned()->nullable();
            $table->foreign('payment_term_id')->references('id')
                ->on('payment_terms');
            $table->decimal('price');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freight_prices');
    }
}
