<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use app\Traits\Obfuscate\Optimuss;
use App\Model\User;
class UserFormRequest extends FormRequest
{
    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->get('id') != null){
            $user = User::find($this->optimus()->encode($this->get('id')));
            return [
                'assignments' => 'required',
                'roles' => 'required',
                'user.username' => 'required|unique:users,username,'.$user->id,
                'user.email' => 'required|email|unique:users,email,'.$user->id,
            ];
        }

        return [
            'assignments' => 'required',
            'roles' => 'required',
            'user.email' => 'required|email|unique:users,email',
            'user.username' => 'required|unique:users,username',
            'user.password' => 'min:8|required_with:password_confirmation|same:user.confirm_password',
            'user.confirm_password' => 'min:8'
        ];

        
    }
}
