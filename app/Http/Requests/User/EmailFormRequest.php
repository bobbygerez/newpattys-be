<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use app\Traits\Obfuscate\Optimuss;
class EmailFormRequest extends FormRequest
{

    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'unique:users,email,'. $this->removeStringEncode($this->get('id'))
        ];
    }
}
