<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use app\Traits\Obfuscate\Optimuss;
class RoleFormRequest extends FormRequest
{
    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->get('id') == null){
            return [
                'name' => 'required|unique:roles,name',
                'description' => 'required',
                'parent_id' => 'required|integer'
            ];
        }
        return [
            'name' => 'required|unique:roles,name,'. $this->optimus()->encode($this->get('id')),
            'description' => 'required',
            'parent_id' => 'required|integer'
        ];
    }
}
