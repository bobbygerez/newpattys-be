<?php

namespace App\Http\Requests\BranchChartAccount;

use Illuminate\Foundation\Http\FormRequest;
use app\Traits\Obfuscate\Optimuss;

class BranchChartAccountRequest extends FormRequest
{

    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chart_account_id' => 'unique:branch_chart_accounts,chartable_id,NULL,id,'.$this->removeStringEncode($this->get('id')),
        ];
    }
}
