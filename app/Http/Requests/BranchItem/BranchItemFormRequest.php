<?php

namespace App\Http\Requests\BranchItem;

use Illuminate\Foundation\Http\FormRequest;
use app\Traits\Obfuscate\Optimuss;
class BranchItemFormRequest extends FormRequest
{
    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = app()->make('request');
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        return [
            'id' => 'unique:branch_items,company_item_id,NULL,id,branch_id,'. $this->removeStringEncode($accessables['accessable_id'])
        ];
    }

}
