<?php

namespace App\Http\Requests\CompanyItem;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Obfuscate\Optimuss;
class BarcodeFormRequest extends FormRequest
{

    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'barcode' => 'unique:company_items,barcode,'.$this->removeStringEncode($this->get('id')),
        ];
    }
}
