<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Company\CompanyInterface;
class AccountingStandardController extends Controller
{
    protected $company;
    public function __construct(CompanyInterface $company){
        $this->company = $company;
    }
    
    public function index(){
        return response()->json([
            'accountingStandards' => $this->company->accountingStandards()
        ]);
    }
}
