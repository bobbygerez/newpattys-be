<?php

namespace App\Http\Controllers\Api\Information;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Information\InformationInterface;
use App\Model\Information;
class InformationRestoreController extends Controller
{

    protected $information;
    public function __construct(InformationInterface $information){
        // $this->authorizeResource(Information::class);
        $this->information = $information;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Information::class);
        $request = app()->make('request');
        return response()->json([
            'informations' =>  $this->information->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Information $info)
    {
        $this->authorize('restore', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Information $info)
    {
        $this->authorize('view', $info);
        return response()->json([
            'information' => $this->information->withTrashed($request)->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $info)
    {
        $this->authorize('restore', $info);
        $this->information->restore($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Information $info)
    {
        $this->authorize('delete', $info);
        $this->information->destroy($request);
        return response()->json([
            'success' => true
        ]);
    }
}
