<?php

namespace App\Http\Controllers\Api\ChartAccount;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChartAccountFormRequest;
use App\Repo\ChartAccount\ChartAccountInterface;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Http\Request;

class ChartAccountController extends Controller
{
    use Optimuss;
    protected $chartAccount;
    public function __construct(ChartAccountInterface $chartAccount)
    {
        $this->chartAccount = $chartAccount;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        
        return response()->json([
            'chartAccounts' => $this->chartAccount->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'parentAccount' => $this->chartAccount->whereNoObfuscate('id', request('id'))->first(),
            'tAccounts' => $this->chartAccount->tAccounts(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChartAccountFormRequest $request)
    {
        

        return response()->json([
            'success' => $this->chartAccount->create($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'chartAccount' => $this->chartAccount->whereNoObfuscate('id', request('id'))->first(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        return response()->json([
            'chartAccount' => $this->chartAccount->whereNoObfuscate('id', request('id'))->with(['parent', 'tAccount'])->first(),
            'tAccounts' => $this->chartAccount->tAccounts()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $this->chartAccount->update($request);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->chartAccount->where('id', request('id'))->first()->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
