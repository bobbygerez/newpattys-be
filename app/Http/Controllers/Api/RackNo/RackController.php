<?php

namespace App\Http\Controllers\Api\RackNo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Rack;

class RackController extends Controller
{
    
    public function index(){
        return response()->json([
            'racks' => Rack::all()
        ]);
    }
}
