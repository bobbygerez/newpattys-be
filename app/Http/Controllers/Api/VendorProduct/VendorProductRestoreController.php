<?php

namespace App\Http\Controllers\Api\VendorProduct;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\VendorProduct\VendorProductInterface;

class VendorProductRestoreController extends Controller
{

    protected $vendorProduct;
    public function __construct(VendorProductInterface $vendorProduct){

        $this->vendorProduct = $vendorProduct;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'vendorProducts' => $this->vendorProduct->index( app()->make('request') )
        ]);
    }

    public function update(Request $request)
    {
        $this->vendorProduct->restore($request);
        return response()->json([
            'success' => true
        ]);
    }

}
