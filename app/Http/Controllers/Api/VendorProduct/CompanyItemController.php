<?php

namespace App\Http\Controllers\Api\VendorProduct;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CompanyItem;
use App\Model\Vendor;

class CompanyItemController extends Controller
{
    
    public function index(){
        $request = app()->make('request');
        $vendor = Vendor::where('token', $request->token)->with('vendorable.companyItems')->first();

        return response()->json([
            'companyItems' =>  $vendor->vendorable->companyItems->map(function($v){
                return $v->only('name', 'optimus_id');  
            })->values()->all()
        ]);
    }
}
