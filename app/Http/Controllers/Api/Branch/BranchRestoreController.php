<?php

namespace App\Http\Controllers\Api\Branch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Branch\BranchInterface;
use App\Model\Branch;
class BranchRestoreController extends Controller
{
    protected $branch;
    public function __construct(BranchInterface $branch){

        $this->branch = $branch;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Branch::class);
        $request = app()->make('request');
        return response()->json([
            'branches' => $this->branch->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Branch $branch)
    {
        $this->authorize('restore', $branch);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        $this->authorize('restore', $branch);
        $this->branch->restore($request);
        return response()->json([
            'succes' => true
        ]);
    }

    
}
