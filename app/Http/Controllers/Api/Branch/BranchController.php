<?php

namespace App\Http\Controllers\Api\Branch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Branch\BranchInterface;
use App\Http\Requests\BranchFormRequest;
use App\Model\Branch;

class BranchController extends Controller
{
    protected $branch;
    public function __construct(BranchInterface $branch){

        $this->branch = $branch;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('index', Branch::class);
        $request = app()->make('request');
        return response()->json([
            'branches' => $this->branch->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Branch $branch)
    {
        $this->authorize('create', $branch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchFormRequest $request, Branch $branch)
    {
        $this->authorize('create', $branch);
        $this->branch->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);
        return response()->json([
            'branch' => $this->branch->where('id',$request->id)
                ->with(['company','address.country','address.region', 'address.province', 'address.city', 'address.brgy', 'branchType'])
                ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        $this->authorize('update', $branch);
        $this->branch->update($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Branch $branch)
    {
        $this->authorize('delete', $branch);
        $this->branch->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function getBranches($companyId){

        return response()->json([
            'branches' => $this->branch->getBranches($companyId)->get()
        ]);
    }

    public function getBranchMenus($branchId){

        return response()->json([
            'menus' => $this->branch->getBranchMenus($branchId)
        ]);

    }
}
