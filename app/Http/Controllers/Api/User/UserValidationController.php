<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UsernameFormRequest;
use App\Http\Requests\User\EmailFormRequest;

class UserValidationController extends Controller
{
    
    public function userName(UsernameFormRequest $request){

    }

    public function email(EmailFormRequest $request){

    }

    
}
