<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Repo\User\UserInterface;
use App\Http\Requests\User\UserFormRequest;
use Hash;
use Auth;
class UserController extends Controller
{

    protected $user;

    public function __construct(UserInterface $user){
        // $this->authorizeResource(User::class);
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $this->authorize('index', User::class);
        $request = app()->make('request');
        return response()->json([
            'users' => $this->user->index($request)
        ]);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        
        $this->user->store($request);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Request $request)
    {
        //
    }

    /**
     *  Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request, User $user)
    {

        $this->authorize('view', $user);
        return response()->json([
            'user' => $this->user->edit($request)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, UserFormRequest $request)
    {

        $this->authorize('update', $user);
        return response()->json([
            'success' => $this->user->update($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Request $request)
    {
        $this->authorize('delete', $user);
        return $this->user->destroy($request);
    }


    public function changePassword(Request $request){

        $user = $this->user->find($request->id);
        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function getAllUsers(){

        return response()->json([
            'users' => $this->user->with(['roles.accessRights'])->get()
        ]);
    }

    
}
