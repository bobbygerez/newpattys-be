<?php

namespace App\Http\Controllers\Api\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Menu\MenuInterface;

class DashboardMenuController extends Controller
{

    protected $menu;
    public function __construct(MenuInterface $menu){
        $this->menu = $menu;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        return response()->json([
            'roles' => $this->menu->paginate( 
                $this->menu->whereLike('name', 'like', '%' . $request->filter . '%')
                    ->with('allChildren')
                    ->orderBy('created_at', 'desc')
                    ->get() 
            )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'menus' => $this->menu->orWhereNoObfuscate('parent_id', 0)
                ->with('allChildren')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->menu->create($request->all());
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return response()->json([
            'menu' => $this->menu->where('id', $request->id)->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return response()->json([
            'menu' => $this->menu->where('id', $request->id)->with('parent')->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->menu->find($request->id)->update($request->all());
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       
        $this->menu->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function search(){

        return response()->json([
            'menus' => $this->menu->whereNoObfuscate('name', 'LIKE', '%'.request('string').'%')
                ->when( request('string') == null, function($q){
                    return $q->where('parent_id', 0);
                })
                ->with('allChildren')->get()
        ]);
    }
}
