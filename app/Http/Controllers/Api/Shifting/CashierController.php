<?php

namespace App\Http\Controllers\Api\Shifting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Branch;
use Auth;

class CashierController extends Controller
{
    
    public function index(){

        return response()->json([
            'cashiers' => Branch::access()
                ->with(['roles' => function($q){
                        $q->where('name', 'Cashier');
                    }, 'roles.users' => function($q){
                        return $q->where('users.id', '!=', Auth::User()->id);
                    }])
                ->first()->roles->first()->users
                ->map(function($v){
                    return [
                        'label' => $v->name,
                        'value' => $v->optimus_id
                    ];
                })->values()->all()
        ]);
    }
}
