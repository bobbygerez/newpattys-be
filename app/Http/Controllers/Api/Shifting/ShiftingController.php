<?php

namespace App\Http\Controllers\Api\Shifting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repo\Shifting\ShiftingInterface;

class ShiftingController extends Controller
{
     protected $shifting;
    public function __construct(ShiftingInterface $shifting){

        $this->shifting = $shifting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'shiftings' => $this->shifting->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->shifting->store($request);

        return response()->json([
            'shifting' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        return response()->json([
            'shifting' => $this->shifting->where('id', $request->id)->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accept(Request $request){

        $this->shifting->accept( $request );
        return response()->json([
            'success' => true
        ]);
    }

    public function decline(Request $request){

        $this->shifting->decline( $request );
        return response()->json([
            'success' => true
        ]);
    }

    public function coa(Request $request){

        return response()->json([
            'denominations' => $this->shifting->coa($request)
        ]);
    }
}
