<?php

namespace App\Http\Controllers\Api\Shifting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ShiftingInventory;
use App\Traits\Obfuscate\Optimuss;
class ShiftingInventoryController extends Controller
{
    use Optimuss;
    public function index(Request $request){
        $si = ShiftingInventory::where('shifting_id', 
            $this->removeStringEncode($request->id))
            ->with(['companyItem'])
            ->get()
            ->map(function($v){
                return [
                    'label' => $v->companyItem['name'],
                    'image' => $v->companyItem['default_image']['path_url'],
                    'price' => $v->amount,
                    'qty' => $v->qty,
                    'total' => $v->amount * $v->qty
                ];
            })->values()->all();
        return response()->json([
            'shiftingInventories' => $si
        ]);
    }
}
