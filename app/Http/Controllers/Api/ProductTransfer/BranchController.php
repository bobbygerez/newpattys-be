<?php

namespace App\Http\Controllers\Api\ProductTransfer;

use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Repo\ProductTransfer\ProductTransferInterface;
use Illuminate\Http\Request;

class BranchController extends Controller
{

    protected $pt;

    public function __construct(ProductTransferInterface $pt)
    {
        $this->pt = $pt;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response()->json([
            'branches' => $this->pt->index(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function branch(Request $request)
    {

        return response()->json([
            'branch' => $this->pt->branch($request),
            'transferee' => $this->pt->transferee($request),
            'authorize' => $this->pt->approvedTo($request),
            'prepare' => $this->pt->preparedBy($request),
        ]);
    }
}
