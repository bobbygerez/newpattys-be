<?php

namespace App\Http\Controllers\Api\ProductTransfer;

use App\Http\Controllers\Controller;
use App\Model\DeliveryReceipt;
use App\Model\PTProducts;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Http\Request;

class PtProductController extends Controller
{

    use Optimuss;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ptProducts = PTProducts::where('product_transfer_id', $this->removeStringEncode($request->id))
            ->with(['companyItem.package'])
            ->get();

        $dr = DeliveryReceipt::where('product_transfer_id', $this->removeStringEncode($request->id))
            ->with(['invoiceInternal.products.package'])
            ->first();

        $products = collect($ptProducts)->map(function ($v) {
            return [
                'id' => $v->companyItem['id'],
                'label' => $v->companyItem['name'],
                'image' => $v->companyItem['default_image']['path_url'],
                'price' => $v->companyItem['amount'],
                'qty' => $v->qty,
                'ui' => $v->companyItem['package']['name'],
                'sku' => $v->companyItem['sku'],
                'total' => $v->companyItem['amount'] * $v->qty,
            ];
        })->values()->all();

        $drNo = '';
        if ($dr) {
            $drNo = $dr['no'];
        }
        $invoiceNo = '';
        $invoiceId = '';
        if ($dr) {
            $invoiceNo = $dr->invoiceInternal['invoice_no'];
            $invoiceId = $dr->invoiceInternal['id'];
        }
        return response()->json([
            'products' => $products,
            'deliveryReceiptNo' => $drNo,
            'invoiceNo' => $invoiceNo,
            'invoiceId' => $invoiceId,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view(Request $request)
    {

        $dr = DeliveryReceipt::where('product_transfer_id', $this->removeStringEncode($request->id))
            ->with(['invoiceInternal.products.package'])
            ->first();

        $products = collect($dr->invoiceInternal['products'])->map(function ($v) {
            return [
                'id' => $v->pivot['company_item_id'],
                'label' => $v->name,
                'image' => $v->default_image['path_url'],
                'price' => $v->pivot['price'],
                'qty' => $v->pivot['qty'],
                'ui' => $v->package['name'],
                'sku' => $v->sku,
                'total' => $v->pivot['price'] * $v->pivot['qty'],
            ];
        })->values()->all();
        return response()->json([
            'products' => $products,
            'deliveryReceiptNo' => $dr->no,
            'invoiceNo' => $dr->invoiceInternal['invoice_no'],

        ]);

    }
}
