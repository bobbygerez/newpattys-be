<?php

namespace App\Http\Controllers\Api\Accessable;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Accessable\AccessableInterface;

class AccessableController extends Controller
{
    protected $accessable;
    public function __construct(AccessableInterface $accessable)
    {
        $this->accessable = $accessable;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->accessable->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accessableModels(){

        return response()->json([
            'accessables' => $this->accessable->accessableModels()
        ]);
    }

    public function accessRightsModel(Request $request){

        return response()->json([
            'accessRights' => $this->accessable->accessRightsModel($request)
        ]);
    }

    public function getUserModelNames(Request $request){

        return response()->json([
            'models' => $this->accessable->getUserModelNames($request)
        ]);
    }

    public function getAccessableModelName(Request $request){

        return response()->json([
            'accessables' => $this->accessable->getAccessableModelName($request)
        ]);
    }

}
