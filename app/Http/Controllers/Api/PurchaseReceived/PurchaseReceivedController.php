<?php

namespace App\Http\Controllers\Api\PurchaseReceived;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PurchaseReceived;
use App\Repo\PurchaseReceived\PurchaseReceivedInterface;
class PurchaseReceivedController extends Controller
{

    protected $purchaseReceived;

    public function __construct(PurchaseReceivedInterface $purchaseReceived){
        $this->purchaseReceived = $purchaseReceived;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'purchaseReceived' => $this->purchaseReceived->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'success' =>  $this->purchaseReceived->store($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pr =  $this->purchaseReceived->where('id', $request->id)
                        ->with(['vendorable.address.province', 'vendorable.address.city', 'vendorable.address.brgy',
                        'purchaseReceivedPurchasesItems.companyItem.package','purchaseReceivedItems.companyItem.package', 'purchaseOrderNo' => function($q){
                            $q->select('id', 'po_no as label' );
                        }])
                        ->first();
        return response()->json([
            'purchaseReceived' =>  $pr,
            'glAccount' => $pr->glAccount()

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->json([
            'success' =>  $this->purchaseReceived->updatee($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $pr = $this->purchaseReceived->where('id', $request->id)->first();
        $pr->delete();

         return response()->json([
            'success' => true
        ]);
    }
}
