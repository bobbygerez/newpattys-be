<?php

namespace App\Http\Controllers\Api\PurchaseReceived;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CompanyItemPurchaseOrder;
use App\Model\PurchaseOrderNo;
use App\Traits\Obfuscate\Optimuss;
class CompanyItemPurchaseOrderController extends Controller
{

    use Optimuss;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchaseOrderNos = PurchaseOrderNo::all()->take(100);
        $cipos = CompanyItemPurchaseOrder::whereIn('purchase_order_no_id', $purchaseOrderNos->pluck('id'))
        ->with(['companyItem.package', 'vendorable.address.province', 'vendorable.address.city', 'vendorable.address.brgy'])
        ->get();
        return response()->json([
            'companyItemPurchaseOrders' => $cipos,
            'purchaseOrderNos' => $cipos->unique('purchase_order_no_id')->sortByDesc('created_at')->values()->all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $purchaseOrderNos = PurchaseOrderNo::where('po_no', 'like', '%'.$request->id.'%')->orderBy('created_at', 'desc')->get();
        $cipos = CompanyItemPurchaseOrder::whereIn('purchase_order_no_id', $purchaseOrderNos->pluck('id'))
        ->get();
        
        return response()->json([
            'companyItemPurchaseOrders' => $cipos,
            'purchaseOrderNos' => $cipos->unique('purchase_order_no_id')->values()->all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
