<?php

namespace App\Http\Controllers\Api\PurchaseReceived;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PurchaseOrder;
use App\Traits\Obfuscate\Optimuss;
class PurchaseOrderController extends Controller
{
    use Optimuss;
    public function index(){
        $request = app()->make('request');
        $po = PurchaseOrder::where('purchase_order_no', 'like', '%'.$request->orderNo.'%')->orderBy('created_at', 'desc')->get();
        return response()->json([
            'purchaseOrders' => $po
        ]);
    }

    public function edit(Request $request){

        $po = PurchaseOrder::where('id', $this->removeStringEncode($request->id))
            ->with(['purchaseRequest', 'companyItemPurchaseOrder.vendorable', 'companyItemPurchaseOrder.companyItem.companyItemVendorRanks', 'companyItemPurchaseOrder.companyItem.package'])
            ->first();
        return response()->json([
            'purchaseOrder' => $po,
            'companyItemPurchaseOrder' => $po->companyItemPurchaseOrder
        ]);
    }
}
