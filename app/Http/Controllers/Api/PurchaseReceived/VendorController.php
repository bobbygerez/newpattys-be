<?php

namespace App\Http\Controllers\Api\PurchaseReceived;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Vendor;

class VendorController extends Controller
{
    public function index(){
        //2 for logistics see branch_types table
        $logistics =  Branch::where('branch_type_id', 2)
            ->withoutGlobalScope('App\Scopes\BranchScope')
            ->get()
            ->toArray();
        $vendors = Vendor::all()->toArray();

        $merge = array_merge($logistics, $vendors);
        return response()->json([
            'vendors' => collect($merge)->map(function($v){
                return [
                    'label' => $v['label'],
                    'value' => $v['id'],
                    'entity' => $v['entity']
                ];
            })->values()->all()
        ]);
    }
}
