<?php

namespace App\Http\Controllers\Api\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Transaction;
use App\Repo\Transaction\TransactionInterface;
use App\Http\Request\TransactionFormRequest;

class TransactionController extends Controller
{
    protected $transaction;
    public function __construct(TransactionInterface $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Transaction::class);
        return response()->json([
            'transactions' => $this->transaction->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            // 'chartAccounts' => $this->transaction->chartAccounts($request->modelType, $request->modelId),
            // 'transactionTypes' => $this->transaction->transactionTypes($request->modelType, $request->modelId),
            // 'createdBy' => Auth::User(),
            // 'companies' => $this->transaction->userCompanies()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return response()->json([
            'transaction' => $this->transaction->where('id', $request->id)->relTable()->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    // public function chartAccounts(){

    //     return response()->json([
    //         'chartAccounts' => $this->transaction->chartAccounts(request('modelType'), request('modelId'))
    //     ]);
    // }

    public function purchaseReceived(){

        return response()->json([
            'purchaseReceived' => $this->transaction->purchaseReceived(request('modelType'), request('modelId'))
        ]);
    }


    public function disbursement(Request $request){

        return response()->json([
            'success' => true,
            'transaction' =>  $this->transaction->disbursement($request)
        ]);
    }

    public function updateDisbursement(Request $request){
        $this->transaction->updateDisbursement($request);
        return response()->json([
            'success' => true
        ]);
    }

    public function getAssignedInvoices (Request $request){

        return response()->json([
            'purchaseReceived' => $this->transaction->getAssignedInvoices($request->modelType, $request->modelId),
            'entityProducts' => $this->transaction->getAssignedProducts($request)
        ]);
    }

    // public function getEntityProducts(Request $request){

    //     return response()->json([
    //         'entityProducts' => $this->transaction->getVendorProducts($request)
    //     ]);
    // }

    
}
