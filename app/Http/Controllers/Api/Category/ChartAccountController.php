<?php

namespace App\Http\Controllers\Api\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ChartAccount;

class ChartAccountController extends Controller
{
    
    public function index(){

        return response()->json([
            'chartAccounts' => ChartAccount::orderBy('name', 'asc')->get()->map(function($v){
                return [
                    'value' => $v['value'],
                    'label' => $v['label']
                ];
            })
        ]);
    }
}
