<?php

namespace App\Http\Controllers\Api\TransactionSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BranchChartAccount;

class BranchChartAccountController extends Controller
{
    
    public function index(){

        return response()->json([
            'branchChartAccounts' => BranchChartAccount::postable()->get()->map(function($v){
                return $v->only(['label', 'value']);
            })
        ]);
    }
}
