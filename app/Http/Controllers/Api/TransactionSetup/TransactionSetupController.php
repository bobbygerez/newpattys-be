<?php

namespace App\Http\Controllers\Api\TransactionSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\TransactionSetup\TransactionSetupInterface;
class TransactionSetupController extends Controller
{

    protected $transactionSetup;
    public function __construct(TransactionSetupInterface $transactionSetup)
    {
        $this->transactionSetup = $transactionSetup;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $request = app()->make('request');
        
        return response()->json([
            'transactionSetup' => $this->transactionSetup->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->transactionSetup->store( $request );
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        return response()->json([
            'transactionSetup' => $this->transactionSetup->where('id', $request->id)
                ->with(['modelListing', 'drBranchChartAccount', 'crBranchChartAccount'])
                ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->transactionSetup->update($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->transactionSetup->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
