<?php

namespace App\Http\Controllers\Api\CompanyItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VendorProduct;
use App\Model\BranchItem;

use App\Traits\Obfuscate\Optimuss;
class VendorProductController extends Controller
{

    use Optimuss;
    
    public function index(){

        $request = app()->make('request');

        $vendors = VendorProduct::where('company_item_id', $this->removeStringEncode($request->id))
                ->with(['vendor'])
                ->get()
                ->filter(function($v){
                    return $v->vendor != null;
                })
                ->map(function($v){
                    return [
                        'optimus_id' => $v->vendor['optimus_id'],
                        'id' => $v->vendor['id'] . $v->vendor['name'],
                        'label' => $v->vendor['name'],
                        'value' => $v->vendor['optimus_id'] . $v->vendor['name'],
                        'price' => $v->string_price,
                        'entity' => $v->vendor['entity'],
                    ];
                })
                ->values()
                ->all();

        $branches = BranchItem::where( 'company_item_id', $this->removeStringEncode($request->id) )
                ->with(['branch', 'companyItem'])
                ->get()
                ->map(function($v){
                    return [
                        'optimus_id' => $v->branch['optimus_id'],
                        'id' => $v->branch['id'] . $v->branch['name'],
                        'label' => $v->branch['name'],
                        'value' => $v->branch['optimus_id'] . $v->branch['name'],
                        'price' => $v->companyItem->string_price,
                        'entity' => $v->branch['entity']
                    ];
                })
                ->values()
                ->all();
                
        return response()->json([
            'vendors' => array_merge($vendors, $branches)
        ]);
    }
}
