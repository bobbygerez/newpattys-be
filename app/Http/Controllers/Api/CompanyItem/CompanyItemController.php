<?php

namespace App\Http\Controllers\Api\CompanyItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\CompanyItem\CompanyItemInterface;
use App\Model\CompanyItem;
class CompanyItemController extends Controller
{

    protected $companyItem;
    public function __construct(CompanyItemInterface $companyItem){
        $this->companyItem = $companyItem;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json([
            'companyItems' => $this->companyItem->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        return response()->json([
            'success' => $this->companyItem->store($request)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return response()->json([
            'companyItem' => $this->companyItem->edit($request),
            'vendors' => $this->companyItem->vendors($request)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
        return response()->json([
            'success' =>  $this->companyItem->update($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $this->companyItem->delete($request);
        return response()->json([
            'success' => true
        ]);
    }
}
