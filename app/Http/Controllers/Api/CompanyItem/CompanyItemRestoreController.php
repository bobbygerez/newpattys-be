<?php

namespace App\Http\Controllers\Api\CompanyItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\CompanyItem\CompanyItemInterface;
use App\Model\CompanyItem;

class CompanyItemRestoreController extends Controller
{

    protected $companyItem;
    public function __construct(CompanyItemInterface $companyItem){
        $this->companyItem = $companyItem;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json([
            'companyItems' => $this->companyItem->index( app()->make('request') )
        ]);
    }

    public function create()
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->companyItem->restore($request);
        return response()->json([
            'succes' => true
        ]);
    }

    
}
