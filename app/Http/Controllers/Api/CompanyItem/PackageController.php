<?php

namespace App\Http\Controllers\Api\CompanyItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Package;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'packages' => Package::all()
        ]);
    }

}
