<?php

namespace App\Http\Controllers\Api\CompanyItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyItem\SkuFormRequest;
use App\Http\Requests\CompanyItem\BarcodeFormRequest;

class CompanyItemValidationController extends Controller
{
    public function sku(SkuFormRequest $request){

    }

    public function barcode(BarcodeFormRequest $request){

    }
}
