<?php

namespace App\Http\Controllers\Api\CompanyItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Vendor;
use App\Traits\Obfuscate\Optimuss;
class VendorController extends Controller
{
    use Optimuss;
    
    public function index(){

        $request = app()->make('request');
        return response()->json([
            'vendors' => Vendor::whereHas('vendorProducts', function($q) use ($request){
                $q->where('company_item_id', $this->removeStringEncode($request->id));
            })->get()
        ]);
    }
}
