<?php

namespace App\Http\Controllers\Api\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Vendor\VendorInterface;
use App\Model\Vendor;
class VendorController extends Controller
{
    protected $vendor;
    public function __construct(VendorInterface $vendor){

        $this->vendor = $vendor;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Vendor::class);
       return response()->json([
           'vendors' => $this->vendor->index( app()->make('request') )
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Vendor $vendor)
    {
        $this->authorize('create', $vendor);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Vendor $vendor)
    {
        $this->authorize('create', $vendor);
        $this->vendor->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vendor $vendor)
    {
        $this->authorize('view', $vendor);
        return response()->json([
            'vendor' => $this->vendor->where('id', $request->id)
                ->with(['address.country', 'address.region', 'address.province', 'address.city', 'address.brgy',])
                ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        $this->authorize('update', $vendor);
        $this->vendor->update($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Vendor $vendor)
    {

        $this->authorize('delete', $vendor);
        $this->vendor->destroy($request);
        return response()->json([
            'success' => true
        ]);
    }

    public function getVendor($token){
        $vendor = $this->vendor->whereNoObfuscate('token', $token)->first();
        if($vendor === null){
            return response([
                'message' => 'Not Found'
            ], 422);
        }
        return response()->json([
            'vendor' => $vendor
        ]);
    }
}
