<?php

namespace App\Http\Controllers\Api\FreightPrice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\FreightPrice\FreightPriceInterface;
use App\Model\FreightPrice;

class FreightPriceController extends Controller
{

    protected $freightPrice;
    public function __construct(FreightPriceInterface $freightPrice){
        $this->freightPrice = $freightPrice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return response()->json([
            'freightPrices' => $this->freightPrice->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->freightPrice->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        return response()->json([
            'freightPrice' => $this->freightPrice->edit($request)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->freightPrice->update($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $this->freightPrice->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
