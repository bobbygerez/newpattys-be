<?php

namespace App\Http\Controllers\Api\FreightPrice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\FreightPrice\FreightPriceInterface;

class BranchFranchiseController extends Controller
{

    protected $freightPrice;
    public function __construct(FreightPriceInterface $freightPrice){
        $this->freightPrice = $freightPrice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json([
            'branchFranchise' => $this->freightPrice->index()
        ]);
    }

}
