<?php

namespace App\Http\Controllers\Api\FreightPrice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\FreightPrice\FreightPriceInterface;

class BranchController extends Controller
{

    protected $freightPrice;
    public function __construct(FreightPriceInterface $freightPrice){
        $this->freightPrice = $freightPrice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $branches = Branch::where('branch_type_id', '!=', 2)->orderBy('name', 'asc')->get();
        return response()->json([
            'branches' => $branches
        ]);
    }

}
