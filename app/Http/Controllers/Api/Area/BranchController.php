<?php

namespace App\Http\Controllers\Api\Area;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\AreaBranch;

class BranchController extends Controller
{
    
    public function index(){
        $abIds = AreaBranch::all()->pluck('branch_id');
        return response()->json([
            'branches' => Branch::whereNotIn('id', $abIds)->get()->map(function($v){
                return $v->only(['value', 'label']);
            })
        ]);
    }
}
