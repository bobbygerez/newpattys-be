<?php

namespace App\Http\Controllers\Api\BranchChartAccount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\BranchChartAccount\BranchChartAccountInterface;
use App\Http\Requests\BranchChartAccount\BranchChartAccountRequest;
class BranchChartAccountController extends Controller
{

    protected $branchChartAccount;
    public function __construct(BranchChartAccountInterface $branchChartAccount){

        $this->branchChartAccount = $branchChartAccount;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        
        return response()->json([
            'chartAccounts' => $this->branchChartAccount->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->branchChartAccount->store($request);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        return response()->json([
            'chartAccount' => $this->branchChartAccount
                ->whereNoObfuscate('id', $request->id)
                ->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ca = $this->branchChartAccount->remove($request);
        return response()->json([
            'success' => $ca 
        ]);
    }
}
