<?php

namespace App\Http\Controllers\Api\Ecommerce;

use App\Http\Controllers\Controller;
use App\Model\Branch;

class BranchController extends Controller
{

    public function index()
    {
        return response()->json([
            'branches' => Branch::all(),
        ]);
    }
}
