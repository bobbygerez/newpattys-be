<?php

namespace App\Http\Controllers\Api\Ecommerce;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Traits\Obfuscate\Optimuss;

class CategoryController extends Controller
{
    use Optimuss;

    public function index()
    {

        $request = app()->make('request');
        $categories = Category::whereHas('companyItem.branchItem', function ($q) use ($request) {
            $q->where('branch_id', $this->removeStringEncode($request->id));
        })->get();
        //$childs = $this->recursiveCategory($categories->children);
        return response()->json([
            'categories' => $categories,
        ]);

    }

    public function recursiveCategory($array)
    {

        $result = [];
        foreach ($array as $item) {
            if ($item['id'] != null) {
                $result[] = [
                    'label' => $item['label'],
                    'value' => $item['id'],
                ];
            }
            $result = array_merge($result, $this->recursiveCategory($item['children']));
        }
        return array_filter($result);
    }
}
