<?php

namespace App\Http\Controllers\Api\Ecommerce;

use App\Http\Controllers\Controller;
use App\Repo\Ecommerce\EcommerceInterface;

class BranchItemController extends Controller
{
    protected $ecommerce;
    public function __construct(EcommerceInterface $ecommerce)
    {

        $this->ecommerce = $ecommerce;
    }

    public function index()
    {

        return response()->json([
            'branchItems' => $this->ecommerce->branchItems(app()->make('request')),
        ]);

    }

}
