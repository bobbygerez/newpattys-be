<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Employee\EmployeeInterface;
use App\Model\Employee;
class EmployeeInformationController extends Controller
{

    protected $employee;
    public function __construct(EmployeeInterface $employee){
        $this->employee = $employee;
    }
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', $employee);
        $this->employee->store($request);
        return response()->json([
            'success' => true
        ]);
    }

}
