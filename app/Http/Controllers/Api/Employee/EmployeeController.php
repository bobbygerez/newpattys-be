<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Employee\EmployeeInterface;
use App\Model\Employee;

class EmployeeController extends Controller
{

    protected $employee;
    public function __construct(EmployeeInterface $employee){
        $this->employee = $employee;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Employee::class);
        $request = app()->make('request');
        return response()->json([
            'employees' => $this->employee->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee)
    {
        $this->authorize('create', $employee);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', $employee);
        $this->employee->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->authorize('view', $employee);
        return response()->json([
            'employee' => $this->employee->where('id', $request->id)->with('information')->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);
        return response()->json([
            'employee' => $this->employee->where('id', $request->id)
                ->with([
                    'information.gender',
                    'information.civilStatus',
                    'information.address.country',
                    'information.address.region',
                    'information.address.province',
                    'information.address.city',
                    'information.address.brgy',
                    'employeeType', 
                    'employeeStatus', 
                    'designation'
                ])
                ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $this->authorize('update', $employee);
        $this->employee->update($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Employee $employee)
    {
        
        $this->authorize('delete', $employee);
       $employee = $this->employee->find($request->id);
       $employee->delete();

       return response()->json([
           'success' => true
       ]);
    }
}
