<?php

namespace App\Http\Controllers\Api\Pos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\TransactionNo;
use App\Repo\Pos\PosInterface;
use App\Model\BranchItemTrans;
use Auth;

class TransactionNoController extends Controller
{
    protected $pos;

    public function __construct(PosInterface $pos){
        $this->pos = $pos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'transactionNos' => $this->pos->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $branchItemTrans = $this->pos->edit( $request );
        return response()->json([
            'branchItemTrans' => $branchItemTrans,
            'grandTotal' => $branchItemTrans->sum('subtotal'),
            'branch' => $this->pos->getBranchInfo($request),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $t = $this->pos->where('id', $request->id )->with(['branchItemTrans'])->first();

        $saveBy = null;
        if($t->saveBy){
            $saveBy = $t->saveBy->id;
        }
        $transactBy = null;
        if($t->transactBy){
            $transactBy = $t->transactBy->id;
        }
        $newT = TransactionNo::create([
            'no' => $t->no.'-V',
            'save_by' => $saveBy,
            'transact_by' => $transactBy,
            'transactable_id' => $t->transactable_id,
            'transactable_type' => $t->transactable_type,
            'grand_total' => $t->grand_total,
            'enter_amount' => $t->enter_amount,
            'credit_card_id' => $t->credit_card_id,
            'customer_reward_id' => $t->customer_reward_id,
            'is_void' => 1
        ]);
        $newT = TransactionNo::where('id', $newT->id)->first();

        foreach($t->branchItemTrans as $i){
            BranchItemTrans::create([
                'transactable_id' => $i->transactable_id,
                'transactable_type' => $i->transactable_type,
                'transaction_no_id' => $newT->id,
                'company_item_id' => $i->company_item_id,
                'qty_in' => $i->qty_out,
                'amount' => $i->amount
            ]); 
        }
        $t->update([
            'is_completed' => false,
            'is_void' => true
        ]);
        
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payNow(Request $request){

        $t = $this->pos->where('id', $request->id)->first();
        $t->update([
            'grand_total' => $request->grandTotal,
            'enter_amount' => $request->enterAmount,
            'is_completed' => true,
            'is_void' => false,
            'transact_by' => Auth::User()->id
        ]);

        return response()->json([
            'success' => true
        ]);

    }

     public function creditCard(Request $request){

       $creditCard = $this->pos->creditCard( $request );

        $t = $this->pos->where('id', $request->id)->first();

        $t->update([
            'grand_total' => $request->grandTotal,
            'enter_amount' => $request->enterAmount,
            'is_completed' => true,
            'is_void' => false,
            'transact_by' => Auth::User()->id,
            'credit_card_id' => $creditCard->id
        ]);


        return response()->json([
            'success' => $creditCard
        ]);

    }

     public function customerReward(Request $request){

        $customerReward = $this->pos->customerReward( $request );

        $t = $this->pos->where('id', $request->id)->first();

        $t->update([
            'grand_total' => $request->grandTotal,
            'enter_amount' => $request->enterAmount,
            'is_completed' => true,
            'is_void' => false,
            'transact_by' => Auth::User()->id,
            'customer_reward_id' => $customerReward->id
        ]);


        return response()->json([
            'success' => true
        ]);

    }
}
