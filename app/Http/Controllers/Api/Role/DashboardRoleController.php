<?php

namespace App\Http\Controllers\Api\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Role\RoleInterface;
use App\Http\Requests\RoleFormRequest;
use App\Model\Role;
use App\Model\User;

class DashboardRoleController extends Controller
{

    protected $role;
    public function __construct(RoleInterface $role){
        // $this->authorizeResource(Role::class);
        $this->role = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Role::class);
        $request = app()->make('request');
        return response()->json([
            'roles' => $this->role->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Role $role)
    {
        $this->authorize('create', $role);
        $request = app()->make('request');
        return response()->json([
            'roles' => $this->role->create($request)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleFormRequest $request, Role $role)
    {
        $this->authorize('create', $role);
        $this->role->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Role $role)
    {   

        $this->authorize('view', $role);
        return response()->json([
            'role' => $this->role->where('id', $request->id)->with('parent')->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleFormRequest $request, Role $role)
    {

        $this->authorize('update', $role);
        return $this->role->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Role $role)
    {
        $this->authorize('delete', $role);
        $this->role->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
