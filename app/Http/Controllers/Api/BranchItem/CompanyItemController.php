<?php

namespace App\Http\Controllers\Api\BranchItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CompanyItem;

class CompanyItemController extends Controller
{
    public function index(){

        return response()->json([
            'companyItems' =>  CompanyItem::all()->map(function($v){
                return $v->only('name', 'id', 'optimus_id');  
            })->values()->all()
        ]);
    }
}
