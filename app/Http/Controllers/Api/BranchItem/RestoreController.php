<?php

namespace App\Http\Controllers\Api\BranchItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\BranchItem\BranchItemInterface;

class RestoreController extends Controller
{
    protected $branchItem;

    public function __construct(BranchItemInterface $branchItem){

        $this->branchItem = $branchItem;
    }

    public function index()
    {
        
        return response()->json([
            'branchItems' => $this->branchItem->index( app()->make('request') )
        ]);
    }

    public function update(Request $request)
    {
        $this->branchItem->restore($request);
        return response()->json([
            'succes' => true
        ]);
    }
}
