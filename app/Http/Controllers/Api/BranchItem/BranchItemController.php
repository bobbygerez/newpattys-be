<?php

namespace App\Http\Controllers\Api\BranchItem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\BranchItem\BranchItemInterface;
use App\Model\Branch;
use App\Traits\Obfuscate\Optimuss;
use App\Http\Requests\BranchItem\BranchItemFormRequest;

class BranchItemController extends Controller
{
    use Optimuss;

    protected $branchItem;
    public function __construct(BranchItemInterface $branchItem){

        $this->branchItem = $branchItem;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'branchItems' => $this->branchItem->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchItemFormRequest $request)
    {
        
        $this->branchItem->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        return response()->json([
            'branchItem' => $this->branchItem
                ->where('id', $request->id)
                ->with(['companyItem'])
                ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $this->branchItem->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
