<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PurchaseRequest;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class CheckedController extends Controller
{
    use Optimuss;

    public function checked(Request $request){

        $pr = PurchaseRequest::find( $this->removeStringEncode($request->id) );
        // if($pr->prepared_by === Auth::User()->id ){
        //     return response()->json([
        //         'message' => 'Unauthorized action.'
        //     ], 401);
        // }
        $pr->checked_by = Auth::User()->id;
        $pr->update();

        return response()->json([
            'success' => true
        ]);
    }
}
