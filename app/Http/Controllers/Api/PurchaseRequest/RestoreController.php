<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\PurchaseRequest\PurchaseRequestInterface;

class RestoreController extends Controller
{
    protected $purchaseRequest;

    public function __construct(PurchaseRequestInterface $purchaseRequest){
        $this->purchaseRequest = $purchaseRequest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'purchaseRequests' => $this->purchaseRequest->index( app()->make('request') )
        ]);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->purchaseRequest->restore($request);
        return response()->json([
            'succes' => true
        ]);
    }

}
