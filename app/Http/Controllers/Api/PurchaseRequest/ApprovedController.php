<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PurchaseRequest;
use App\Model\PurchaseOrder;
use App\Model\CompanyItemPurchaseOrder;
use App\Traits\Obfuscate\Optimuss;
use App\Model\FreightPrice;
use App\Model\Area;
use Auth;
use Carbon\Carbon;
use App\Model\PurchaseOrderNo;

class ApprovedController extends Controller
{
    use Optimuss;

    protected $vendorPo = [];

    public function approved(Request $request){

        $pr = PurchaseRequest::find( $this->removeStringEncode($request->id) );
        // if($pr->prepared_by === Auth::User()->id || $pr->checked_by === Auth::User()->id ){
        //     return response()->json([
        //         'message' => 'Unauthorized action.'
        //     ], 401);
        // }
        // $pr->approved_by = Auth::User()->id;
        // $pr->update();


        $pr = PurchaseRequest::where('id', $pr->id)->with([
            'purchaseRequestCompanyItems.companyItem.branchItemNoGlobalScope.branch', 'purchaseRequestCompanyItems.companyItem.vendorProduct.vendor',
            'purchaseRequestCompanyItems.companyItem.companyItemVendorRanks',
            'purchasable'
            ])->first();
        
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        
        $branch = $accessables['accessable_type']::find($this->removeStringEncode($accessables['accessable_id']));

        $po = PurchaseOrder::create([
            'purchase_request_id' => $pr->id,
            'purchasable_id' => $pr->purchasable_id,
            'purchasable_type' => $pr->purchasable_type,
            'name' => $pr->name,
            'purchase_order_code' => str_random(32)
        ]);
        
        $po = PurchaseOrder::find($po->id);
        
        $vendorPoNo = [];
        //get purchaaseRequestCompanyItems
        foreach($pr->purchaseRequestCompanyItems as $prci){

            //get all the vendorRanks
            $vrs = $prci->companyItem['companyItemVendorRanks'];

            $pluckPercentage =  $vrs->pluck('percentage')->toArray();
             $rank1 = $vrs->filter(function($v){
                return $v->rank === 1;
            })->first();
            
            //get total quantity requested per item
            $totalQty =  $prci->qty_order;
            $token = str_random(32);

            //Branch Type 2 is for LOGISTIC See branch_types table
            if($pr->purchasable_type == 'App\Model\Branch' && $pr->purchasable['branch_type_id'] == 2){
                
                //Check if there is the same percentage
                if( count(array_unique($pluckPercentage)) < count($pluckPercentage) ) {
                    // same percentage
                    //loop all the vendor
                    foreach($vrs as $k => $vr){
                        $newVr = $vr;
                       
                        $vendorProducts = collect($prci->companyItem['vendorProduct'])->filter(function($v) use ($newVr) {
                            return $v->vendor['id']==  $newVr->vendorable_id && $v->vendor['entity'] == $newVr->vendorable_type;
                            
                        })->values()->all();

                        $percentage =  $vr->percentage;
                        $tQuantity = round( ($percentage * $totalQty)/100 );

                        if($totalQty % 2 != 0){
                            if($rank1->vendorable_id != $vr->vendorable_id ){
                                $tQuantity--;
                            }
                        }

                        if(count($vendorProducts) > 0){
                            $vendorProduct = head($vendorProducts);
                            $this->createCIPO( $po->id, $prci->companyItem->id, $tQuantity, $vendorProduct['price'],  0, 0, $token, $newVr['vendorable_id'], $newVr['vendorable_type']);
                        }else{
                            $this->createCIPO( $po->id, $prci->companyItem->id, $tQuantity, $prci->companyItem['float_amount'],  0, 0, $token, $newVr['vendorable_id'], $newVr['vendorable_type']);
                        }
                        
                    }
                }
                else{
                    // not the same percentage
                    //loop all the vendor
                    foreach($vrs as $vr){
                        $newVr = $vr;
                       
                        $vendorProducts = collect($prci->companyItem['vendorProduct'])->filter(function($v) use ($newVr) {
                            return $v->vendor['id']==  $newVr->vendorable_id && $v->vendor['entity'] == $newVr->vendorable_type;
                            
                        })->values()->all();

                        $percentage =  $vr->percentage;
                        $tQuantity = round( ($percentage * $totalQty)/100 );

                         if(count($vendorProducts) > 0){
                            $vendorProduct = head($vendorProducts);
                            $this->createCIPO( $po->id, $prci->companyItem->id, $tQuantity, $vendorProduct['price'],  0, 0, $token, $newVr['vendorable_id'], $newVr['vendorable_type']);
                        }else{
                            $this->createCIPO( $po->id, $prci->companyItem->id, $tQuantity, $prci->companyItem['float_amount'],  0, 0, $token, $newVr['vendorable_id'], $newVr['vendorable_type']);
                        }
                        
                    }
                }
            }

            /*** Branch Purchase Request */
        //Branch Type 1 is for BRANCH See branch_types table
        if($pr->purchasable_type == 'App\Model\Branch' && $pr->purchasable['branch_type_id'] == 1){
                //E add ang branch sa area... 
                 //vendor Ranks
                 $cp = $prci->companyItem['amount'];
                    $areaBranch = Area::whereHas('areaBranches', function($q) use ($pr){
                        $q->where('branch_id', $pr->purchasable_id);
                    })->first();


                    $withFreight = null;

                    //NEED TO LOOP ALL THE VENDOR to get match on freight..
                    foreach($prci->companyItem['companyItemVendorRanks'] as $vendorRank){

                        $dbFreight = FreightPrice::withoutGlobalScope('App\Scopes\FreightPriceScope')
                        ->where('to_id', $areaBranch->id)
                        ->where('to_type', $areaBranch->entity)
                        ->where('from_id', $vendorRank['vendorable_id'])
                        ->where('from_type', $vendorRank['vendorable_type'])
                        ->first();

                        if($dbFreight){
                            $withFreight = $dbFreight;
                        }

                    }

                    $freight = 0;

                  if($withFreight){
                    $freight = $withFreight->price_float;
                  }
                
                  
                   //company item + freight
                    $totalCompanyPrice = $cp + $freight;
                    $lowPrices = collect( $prci->companyItem['vendorProduct'] )->filter(function($v) use ($totalCompanyPrice) {
                            return $v->price < $totalCompanyPrice;
                        })->sortBy('price')->values()->all();
                    
                    $lowestPrice = head($lowPrices);
                  

                if($lowestPrice['vendor'] != null){
                    $this->createCIPO( 
                            $po->id, 
                            $prci->companyItem['id'], 
                            $totalQty, 
                            $lowestPrice['price'], 0, 0, 
                            $token, 
                            $branch['id'],  
                            $branch['entity'],
                            $lowestPrice->vendor['id'], 
                            $lowestPrice->vendor['entity']
                    );
                }else{
                    $logistics = collect($prci->companyItem['companyItemVendorRanks'])->filter(function($v){
                        //didn't understand why filter to Branch... 
                        //need to double check...
                        //return $v->vendorable_type === 'App\Model\Branch';
                        return $v->vendorable_type === 'App\Model\Vendor';
                    })->values()->all();

                    $logistic = head($logistics);
                    $this->createCIPO( 
                            $po->id, 
                            $prci->companyItem['id'], 
                            $totalQty, 
                            $prci->companyItem['amount'], 
                            $freight, 
                            0, 
                            $token, 
                            $branch['id'],  
                            $branch['entity'],
                            $logistic->vendorable_id, 
                            $logistic->vendorable_type
                    );
                }
                 
                
        }
        
            /*** Branch Purchase Request */

        }

        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        $branch = $accessables['accessable_type']::find($this->removeStringEncode($accessables['accessable_id']));
        
        $arrayVendors = array_values(collect($this->vendorPoNo)->groupBy('orderable_id', 'orderable_type')->values()->all());


        foreach( $arrayVendors as $vendor){

            $poOnThisDate = PurchaseOrderNo::where('created_at', '>', Carbon::now()->subDays(1) )
            ->get();
            $no = 1;
            if(count($poOnThisDate) > 0){
                $no = count($poOnThisDate) + 1;
            }else{
                $no = 1;
            }
            $poCode = $branch->acronym.'-'. date('Ymd') . '-' . str_pad($no, 4, '0', STR_PAD_LEFT);
            $vendorableId = '';
            foreach($vendor as $v){
                
                if($vendorableId != $v['orderable_id']){
                    $poNo = PurchaseOrderNo::create([
                        'po_no' => $poCode,
                        'orderable_id' => $v['orderable_id'],
                        'orderable_type' => $v['orderable_type']
                    ]);
                }
                

                $cipo = CompanyItemPurchaseOrder::where('id', $v['cipo_id'])->first();
                $cipo->purchase_order_no_id = $poNo->id;
                $cipo->update();

                $vendorableId = $v['orderable_id'];
            }


            
        }
        return response()->json([
            'success' => true
        ]);

    }

    public function createCompanyItems($po, $ci, $vr, $tQuantity, $token){

        if($vr->vendorable_type === 'App\Model\Branch'){
            $this->createCIPO( $po->id, $ci->id, $tQuantity, $ci->amount,  0, 0, $token, $vr->vendorable_id, $vr->vendorable_type);
        }

        if($vr->vendorable_type === 'App\Model\Vendor'){
            $this->createCIPO( $po->id, $ci->id, $tQuantity, $ci->vendorProduct['price'], 0, 0, $token, $vr->vendorable_id, $vr->vendorable_type);
        }
    }

    public function createCIPO($poId, $ciId, $qty, $price, $freight, $tax, $token, $orderableId, $orderableType, $vendorableId, $vendorableType){

        

        $cipo = CompanyItemPurchaseOrder::create([
            'purchase_order_id' => $poId,
            'company_item_id' => $ciId,
            'qty' => $qty,
            'price' => $price, 
            'freight' => $freight, 
            'tax' => $tax, 
            'token' => $token,
            'orderable_id' => $orderableId,
            'orderable_type' => $orderableType,
            'vendorable_id' => $vendorableId,
            'vendorable_type' => $vendorableType
        ]);

          $this->vendorPoNo[] = [ 'cipo_id' => $cipo->id, 'orderable_id' => $vendorableId, 'orderable_type' => $vendorableType ];

    }
}