<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\PurchaseRequest\PurchaseRequestInterface;
class CompanyItemController extends Controller
{
    protected $purchaseRequest;

    public function __construct(PurchaseRequestInterface $purchaseRequest){
        $this->purchaseRequest = $purchaseRequest;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'companyItems' => $this->purchaseRequest->with(['package'])->get()
        ]);
    }

}
