<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BranchItem;
use App\Traits\Obfuscate\Optimuss;
class BranchItemController extends Controller
{

    use Optimuss;
    
    public function index(){
        $branchItems = BranchItem::with(['companyItem.package'])->get()
            ->filter(function($v){
                return $v->total_qty < $v->companyItem['reorder_qty'];
            })
            ->unique('company_item_id')
            ->values()
            ->all();

        return response()->json([
            'branchItems' => $branchItems
        ]);
    }
    public function show(Request $request){
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        $bi = BranchItem::withoutGlobalScopes()
        ->where('company_item_id', $this->removeStringEncode($request->id) )
        ->where('branch_id', $this->removeStringEncode($accessables['accessable_id']))
        ->with('companyItem')
        ->first();
        $maxQty = $bi->companyItem->max_qty;
        $tQty = $bi->total_qty;

        return response()->json([
            'totalQty' => $maxQty - $tQty
        ]);
    }
}
