<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\PurchaseRequest\PurchaseRequestInterface;

class PurchaseRequestController extends Controller
{

    protected $purchaseRequest;

    public function __construct(PurchaseRequestInterface $purchaseRequest){
        $this->purchaseRequest = $purchaseRequest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'purchaseRequests' => $this->purchaseRequest->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->purchaseRequest->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return response()->json([
            'purchaseRequest' => $this->purchaseRequest->where('id', $request->id)
                ->with(['purchaseRequestCompanyItems.companyItem.package'])
                ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $this->purchaseRequest->update($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $this->purchaseRequest->find($request->id)->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
