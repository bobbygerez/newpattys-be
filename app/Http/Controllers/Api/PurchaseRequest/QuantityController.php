<?php

namespace App\Http\Controllers\Api\PurchaseRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Hash;

class QuantityController extends Controller
{


    public function store(Request $request){


        $user = User::where('username', $request->username)
                ->with(['roles.accessables.accessRight', 'roles.accessables.menu'])
                ->first();
        
        if (Hash::check($request->password, $user->password)){
            $accessables = collect($user->roles)->map(function($val){
                return $val->accessables;
            })->flatten(1)->filter(function($val) {
                return $val->accessRight['name'] == 'Create' && $val->Menu['name'] == 'Purchase Request';
            })->values()->all();
    
            if(count($accessables) > 0){
                return response()->json([
                    'user_id' => $user->optimus_id
                ]);
            }

            return response()->json([
                'user_id' => null
            ], 401);

        }else{
            return response()->json([
                'user_id' => null
            ], 401);
        }

        
    }
}
