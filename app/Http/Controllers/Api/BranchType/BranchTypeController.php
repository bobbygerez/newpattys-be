<?php

namespace App\Http\Controllers\Api\BranchType;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BranchType;

class BranchTypeController extends Controller
{
    
    public function index(){
        return response()->json([
            'branchTypes' => BranchType::all()
        ]);
    }
}
