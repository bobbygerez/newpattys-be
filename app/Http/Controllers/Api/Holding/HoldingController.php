<?php

namespace App\Http\Controllers\Api\Holding;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Holding\HoldingInterface;
use App\Http\Requests\HoldingFormRequest;
class HoldingController extends Controller
{

    protected $holding;
    public function __construct(HoldingInterface $holding){
        $this->holding = $holding;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        return response()->json([
            'holdings' => $this->holding->paginate( 
                $this->holding->whereLike('name', 'like', '%' . $request->filter . '%')
                    ->orderBy('created_at', 'desc')
                    ->get() 
            )
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HoldingFormRequest $request)
    {
        $holding = $this->holding->create($request->all());
        $this->holding->findNoObfuscate($holding->id)->address()->create($request->address);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        return response()->json([
            'holding' => $this->holding->where('id',$request->id)->relTable()->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $this->holding->update($request);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userHoldings(){

        return response()->json([
            'holdings' => $this->holding->userHoldings()
        ]);
    }
}
