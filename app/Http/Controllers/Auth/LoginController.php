<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Menu;
use App\Model\Accessable;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => request('username'), 'password' => request('password'), 'status' => 1])) {
            return $this->response();
        } else if (Auth::attempt(['username' => request('username'), 'password' => request('password'), 'status' => 1])) {
            return $this->response();
        } else {
            return response()->json(['error' => 'Invalid Username or Password.'], 401);
        }
    }

    public function logout(Request $request)
    {

        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function getUserModels()
    {
    //    if(Auth::User()->isSuperAdmin()){
    //         $accessables = Accessable::with(['accessable'])->get();
    //         return $accessables->map(function($val){
    //             return $val->accessable;
    //         })->filter(function($val){
    //             return $val != null;
    //         })->unique('id')->values()->all();
    //    }

      $user = User::where('id', Auth::User()->id)->with('roles.accessables.accessable', 'roles.accessables.accessRight')
            ->first();
      $accessables = collect($user->roles)->flatMap(function ($value) {
            return $value->accessables;
        });

       $createAccess = collect($accessables)->filter(function($val){
            return $val->accessRight->name === "Create";
        })->unique('id')->values()->all();

        return collect($createAccess)->map(function($val){
            return $val->accessable;
        })
        ->filter(function($val){
            return $val != null;
        })->unique('letter_optimus')->values()->all();

    }

    public function defaultAssignment()
    {

        $user = User::where('id', Auth::User()->id)->with('assignments.assignmentable')->first();
        
        return head( collect($user->assignments)->filter(function($val){
                return $val->is_default == 1;
            })->values()->all()
        );
        
    }


    public function response()
    {
        $user = Auth::user();
        $success['token'] = $user->createToken('MyApp')->accessToken;

        return response()->json([
            'success' => $success,
            'user' => Auth::User(),
            'userLogin' => true,
            'userModels' => $this->getUserModels(),
            'assign' => $this->defaultAssignment(),
        ]);
    }

    public function getMenus(){
        if (Auth::User()->isSuperAdmin()) {
            return Menu::with('allChildren')->where('parent_id', 0)->get();
        }
        $user = User::where('id', Auth::User()->id)->with(['roles.accessables'])->first();

        $accessables = collect($user->roles)->flatMap(function($val){
            return $val->accessables;
        })
        ->filter(function($val){
            return $val->accessable_type === 'App\Model\Menu';
        })
        ->unique('accessable_id')
        ->filter(function($val){
            return $val->morph['parent_id'] === 0;
        });
    }

    protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }
}
