<?php

namespace App\Observers;
use App\Traits\Obfuscate\Optimuss;
use App\Model\BranchItem;

class BranchItemObserver
{
    use Optimuss;

    public function creating(BranchItem $branchItem)
    {

        $request = app()->make('request');
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        $branchItem->branch_id = $this->removeStringEncode($accessables['accessable_id']);
    }
}
