<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShiftingInventory extends Model
{
    protected $table = 'shifting_inventories';
    protected $fillable = ['shifting_id', 'company_item_id', 'qty', 'amount'];

    public function companyItem(){
        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }
}
