<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BranchItemTrans extends Model
{
    protected $table = 'branch_item_transactions';
    protected $fillable = [
        'transactable_id',
        'transactable_type', 
        'transaction_no_id',
        'company_item_id', 
        'qty_in', 
        'qty_out', 
        'amount',
        'is_void'
    ];

    public function companyItem(){
        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }
}
