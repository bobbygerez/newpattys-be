<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use App\Scopes\RackScope;
class Rack extends Model
{
    use Globals, Optimuss;
    protected $table = 'racks';
    protected $fillable = ['name', 'rows', 'columns', 'rackable_id', 'rackable_type'];
    protected $appends = ['optimus_id', 'label', 'value'];

    
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new RackScope);

    }

      public function getValueAttribute(){
        return $this->id;
    }

    public function getLabelAttribute(){
      return $this->name;
    }
}
