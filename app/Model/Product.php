<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
class Product extends Model
{
    use Optimuss;

    protected $table = 'products';
    protected $fillable = [
        'chart_account_id',
        'tax_type_id',
        'productable_id',
        'productable_type',
        'sku',
        'barcode',
        'name',
        'desc',
        'price',
        'qty',
        'discount',
        'package_id'
    ];
    protected $appends = ['optimus_id', 'discount_amt', 'is_primary'];
    public function getPriceAttribute($val){
        return (float)$val;
    }

    public function getDiscountAmtAttribute(){
        return (float)number_format( ($this->discount/100) * $this->price, 2);
    }

    public function products()
    {
        return $this->morphTo();
    }

    public function taxType(){
        return $this->hasOne('App\Model\TaxType', 'id', 'tax_type_id');
    }

    public function chartAccount(){
        return $this->hasOne('App\Model\ChartAccount', 'id', 'chart_account_id');
    }

    public function images(){
        return $this->morphMany('App\Model\Image', 'imageable');
    }

    public function getIsPrimaryAttribute(){
        return  url(collect( $this->images )->filter(function($val){
            return $val->is_primary === 1;
        })->first()->path);
    }
}
