<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\FreightPriceScope;
use App\Traits\Obfuscate\Optimuss;

class FreightPrice extends Model
{
    
    use Optimuss, SoftDeletes;

    protected $table = 'freight_prices';
    protected $fillable = [
        'freightable_type',
        'freightable_id',
        'from_type',
        'from_id',
        'to_type',
        'to_id',
        'price',
        'shipping_method_id',
        'shipping_term_id',
        'payment_term_id'
    ];

    protected $appends = ['from', 'to', 'optimus_id', 'price_float', 'string_price'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new FreightPriceScope);
    }

    public function shippingMethod(){
        return $this->hasOne('App\Model\ShippingMethod', 'id', 'shipping_method_id');
    }

    public function shippingTerm(){
        return $this->hasOne('App\Model\ShippingTerm', 'id', 'shipping_term_id');
    }

    public function paymentTerm(){
        return $this->hasOne('App\Model\PaymentTerm', 'id', 'payment_term_id');
    }

    public function getFromAttribute(){
        return $this->from_type::find($this->from_id)->only('name', 'optimus_id');
    }

    public function getToAttribute(){
        return $this->to_type::find($this->to_id)->only('name', 'optimus_id');
    }

    public function getStringPriceAttribute(){
        return number_format($this->price,2,".",",");
    }

    public function getPriceFloatAttribute(){
        return (float)$this->price;
    }

}
