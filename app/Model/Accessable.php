<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;

class Accessable extends Model
{

    use Optimuss;

    protected $table = 'accessables';
    protected $fillable = ['access_right_id', 'accessable_id', 'accessable_type', 'menu_id'];
    protected $appends = ['entity', 'optimus_id'];

    public function getEntityAttribute(){

        return [
            'entity' => $this->accessable_type,
            'model' => substr($this->accessable_type, 10)
        ];
    }

    public function roles(){
        return $this->belongsToMany('App\Model\Role', 'accessable_role', 'accessable_id', 'role_id')
                ->withTimestamps();
    }

    public function menu(){

        return $this->hasOne('App\Model\Menu', 'id', 'menu_id');
    }

    public function accessRight(){

        return $this->hasOne('App\Model\AccessRight', 'id', 'access_right_id');
    }

    public function branches(){
        return $this->belongsToMany('App\Model\Branch', 'accessable_branch', 'accessable_id', 'branch_id')
                ->withTimestamps();
    }

    public function accessable()
    {
        return $this->morphTo();
    }
}
