<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class ProductTransfer extends Model
{
    use Optimuss, Globals;
    protected $table = 'product_transfers';
    protected $fillable = [
        'from', 'to', 'request_by', 'approved_from', 'approved_to', 'prepared_by',
    ];

    protected $appends = ['optimus_id'];

    public function fromBranch()
    {
        return $this->hasOne('App\Model\Branch', 'id', 'from');
    }

    public function toBranch()
    {
        return $this->hasOne('App\Model\Branch', 'id', 'to');
    }

    public function approvedFrom()
    {
        return $this->hasOne('App\Model\User', 'id', 'approved_from');
    }

    public function approvedTo()
    {
        return $this->hasOne('App\Model\User', 'id', 'approved_to');
    }

    public function preparedBy()
    {
        return $this->hasOne('App\Model\User', 'id', 'prepared_by');
    }

    public function deliveryReceipt()
    {
        return $this->hasOne('App\Model\DeliveryReceipt', 'product_transfer_id', 'id');
    }

}
