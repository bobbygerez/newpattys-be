<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\BranchChartAccountScope;
use App\Traits\Obfuscate\Optimuss;

class BranchChartAccount extends Model
{
    
    use Optimuss;

    protected $table = 'branch_chart_accounts';
    protected $fillable = [
        'id',
        'idd',
        'chartable_id',
        'chartable_type',
        'posting_code',
        'series_no',
        'parent_id',
        'parent_code',
        'taccount_id',
        'name',
        'account_code',
        'account_display',
        'postable',
        'trial_balance'
    ];

    protected $appends = ['value', 'label'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new BranchChartAccountScope);
    }

    public function scopePostable($q){
        $q->where('postable', '=', 0);
    }
    
     public function parent(){
        return $this->hasOne('App\Model\BranchChartAccount', 'account_code', 'parent_code');
    }
    public function children() {

        return $this->hasMany('App\Model\BranchChartAccount', 'parent_code', 'account_code');
    }

    public function allChildren()
    {
        return $this->children()->with(['allChildren', 'tAccount']);
    } 

    public function tAccount(){
        return $this->hasOne('App\Model\TAccount', 'id', 'taccount_id');
    }

    public function getLabelAttribute(){
        return  $this->name . '-' . $this->account_code;
    }
}
