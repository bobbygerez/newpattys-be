<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Model\Globals;
class Customer extends Model
{
    use Globals, Optimuss, SoftDeletes;
    protected $table = 'customers';
    protected $fillable = [
        'customerable_type',
        'customerable_id',
        'name',
        'desc'
    ];
    protected $appends = ['optimus_id'];
    public function address()
    {
        return $this->morphOne('App\Model\Address', 'addressable');
    }
}
