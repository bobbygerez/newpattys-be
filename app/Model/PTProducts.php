<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PTProducts extends Model
{
    
    protected $table = 'pt_products';
    protected $fillable = [
        'qty',
        'price',
        'company_item_id',
        'product_transfer_id'
    ];

    public function companyItem(){

        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }
}
