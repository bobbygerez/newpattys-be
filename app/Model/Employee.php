<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use Carbon\Carbon;

class Employee extends Model
{

    use Optimuss, SoftDeletes;
    
    protected $table = 'employees';
    protected $fillable = [
        'employable_id', 'employable_type', 'salary_rate', 'date_hired', 'date_expired', 'pag_ibig', 'sss', 'tin',
        'prefix_employee_no', 'employee_no', 'employee_type_id', 'employee_status_id',
        'designation_id', 'information_id'
    ];
    protected $appends = ['name', 'mobile', 'string_birth', 'optimus_id'];

    public function address()
    {
        return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function employable(){
        
        return $this->morphTo();
    }

    public function information(){

    	return $this->hasOne('App\Model\Information', 'id', 'information_id');
    }

    public function employeeType(){

    	return $this->hasOne('App\Model\EmployeeType', 'id', 'employee_type_id');
    }

    public function employeeStatus(){

    	return $this->hasOne('App\Model\EmployeeStatus', 'id', 'employee_status_id');
    }

    public function designation(){

    	return $this->hasOne('App\Model\Designation', 'id', 'designation_id');
    }

    public function scopeRelTable($q){
        return $q->with(['information', 'employeeType', 'employeeStatus', 'designation']);
    }

    public function getNameAttribute(){
        if($this->information !== null){
            return $this->information->firstname . ' ' . $this->information->lastname;
        }
       
    }

    public function getMobileAttribute(){
        if($this->information !== null){
            return $this->information->mobile;
        }
    }

    public function getStringBirthAttribute(){
        if($this->information !== null){
            return Carbon::parse($this->information->birthdate)->toFormattedDateString();
        }
    }

    public function getSalaryRateAttribute($val){
        return (float)$val;
    }

    public function getCreatedAtAttribute($val){
        return Carbon::parse($val)->toDayDateTimeString();
    }

}
