<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
class PaymentTerm extends Model
{
    use Optimuss;
    protected $table = 'payment_terms';
    protected $fillable = ['name'];
    protected $appends = ['label', 'value'];

    public function getLabelAttribute(){
        return $this->name;
    }
}
