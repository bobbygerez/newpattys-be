<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class PurchaseOrderNo extends Model
{
    use Optimuss, Globals;
    protected $table = 'purchase_order_nos';
    protected $fillable = ['po_no', 'orderable_id', 'orderable_type'];
    protected $appends = ['optimus_id'];
}
