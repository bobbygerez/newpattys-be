<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PurchaseOrderScope;
use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use Globals, Optimuss, SoftDeletes;
    protected $table = 'purchase_orders';
    protected $fillable = [
        'purchase_request_id', 'purchasable_id' , 'purchasable_type', 'name', 'purchase_order_code', 'purchase_order_no_id'
    ];

    protected $appends = ['optimus_id', 'label', 'value'];
    
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new PurchaseOrderScope);
    }

    public function companyItemPurchaseOrder(){

        return $this->hasMany('App\Model\CompanyItemPurchaseOrder', 'purchase_order_id', 'id');
    }

    public function purchaseRequest(){

        return $this->hasOne('App\Model\PurchaseRequest', 'id', 'purchase_request_id');
    }

    public function getLabelAttribute(){
        return $this->purchase_order_no;
    }

   
}
