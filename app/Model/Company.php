<?php

namespace App\Model;

use App\Traits\Model\Globals;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;
class Company extends Model
{

    use Globals, Optimuss, SoftDeletes;

    protected $table = 'companies';
    protected $fillable = [
        'name', 'desc', 'holding_id'
    ];

    protected $appends = ['optimus_id', 'letter_optimus', 'entity', 'entity_model'];

    public static function boot(){
        parent::boot();
        // static::addGlobalScope('hotel_created_user', function(Builder $builder){
        //     return $builder->where('created_by_user_id', auth()->id() );	
        // });
    }

    public function companyItems(){
        return $this->hasMany('App\Model\CompanyItem', 'company_id', 'id');
    }
    public function users(){

        return $this->morphMany('App\Model\User', 'userable');
    }

    public function roles(){

        return $this->morphMany('App\Model\Role', 'roleable');
    }

    public function employees(){

        return $this->morphMany('App\Model\Employee', 'employable');
    }
    
    public function transactables(){

        return $this->morphMany('App\Model\Transaction', 'transactable');
    }

    public function accessables(){

        return $this->morphMany('App\Model\Accessable', 'accessable');
    }

    public function informations(){

        return $this->morphMany('App\Model\Information', 'informationable');
    }

    public function vendors()
    {

        return $this->hasMany('App\Model\Vendor', 'id', 'company_id');
    }

    public function branches()
    {

        return $this->hasMany('App\Model\Branch', 'company_id', 'id');
    }

    public function chartAccounts()
    {

        return $this->hasMany('App\Model\ChartAccount', 'company_id', 'id');
    }

    public function AccountingStandard()
    {

        return $this->hasMany('App\Model\AccountingStandard', 'id', 'company_id');
    }

    public function trademarks()
    {

        return $this->hasMany('App\Model\Trademark', 'id', 'company_id');
    }

    public function holding()
    {

        return $this->belongsTo('App\Model\Holding');
    }


    public function businessInfo()
    {

        return $this->morphOne('App\Model\BusinessInfo', 'businessable');
    }

    public function address()
    {

        return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function getEntityAttribute()
    {

        return 'App\Model\Company';
    }

    public function getEntityModelAttribute()
    {

        return 'Company';
    }

    public function getLetterOptimusAttribute(){
        return substr($this->name, 0, 5)  . $this->optimus()->decode($this->id);
    }


    // TODO: Commented transaction Type related causing bug atm
    //
    // public function transactionTypes(){

    //     return $this->hasMany('App\Model\TransactionType', 'company_id', 'id');
    // }
    public function scopeRelTable($q)
    {

        return $q->with(['businessInfo', 'branches', 'holding', 'address.country', 'address.region', 'address.province', 'address.city', 'address.brgy', 'chartAccounts']); // , 'transactionTypes']);
    }

}
