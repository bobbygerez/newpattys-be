<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;

class RackColumn extends Model
{
     use Globals, Optimuss;
    protected $table = 'rack_columns';
    protected $fillable = ['rack_id', 'no', 'rackable_id', 'rackable_type'];
    protected $appends = ['optimus_id', 'value', 'label'];
    
    public function rack(){
        return $this->hasOne('App\Model\Rack', 'id', 'rack_id');
    }

    public function getValueAttribute(){
        return $this->id;
    }

    public function getLabelAttribute(){
        return $this->no;
    }
}
