<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PurchaseReceivedPurchaseItem extends Model
{
    protected $table = 'purchase_received_purchases_items';
    protected $fillable = ['purchase_received_id', 'company_item', 'amount', 'order_qty', 'received_qty', 'branch_chart_of_account_id'];

    public function companyItem(){

        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item');
    }
}
