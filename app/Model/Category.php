<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use Globals, Optimuss, SoftDeletes;

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($category) {
            $category->children()->delete();
        });
    }

    protected $table = 'categories';
    protected $fillable = ['name', 'desc', 'parent_id', 'chart_account_id'];
    protected $appends = ['optimus_id', 'label', 'value', 'children_count'];

    public function companyItem()
    {
        return $this->hasOne('App\Model\CompanyItem', 'category_id', 'id');
    }

    public function categorable()
    {

        return $this->morphTo();
    }
    public function parent()
    {
        return $this->hasOne('App\Model\Category', 'id', 'parent_id');
    }
    public function chartAccount()
    {
        return $this->hasOne('App\Model\ChartAccount', 'id', 'chart_account_id');
    }
    public function relChildren()
    {

        return $this->hasMany('App\Model\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->relChildren()->with('children');
    }

    public function scopeRelTable($query)
    {

        return $query->with(['relChildren', 'parent']);
    }

    public function getLabelAttribute()
    {
        return ucfirst(strtolower($this->name));
    }

    public function getChildrenCountAttribute()
    {
        return $count = $this->children->count();
    }

}
