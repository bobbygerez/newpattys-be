<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestCompanyItem extends Model
{
    protected $table = 'purchase_request_company_items';
    
    protected $fillable = [
        'company_item_id',
        'purchase_request_id', 
        'qty_left',
        'qty_order',
        'qty_max',
        'comment'
    ];

    public function companyItem(){
        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }
}
