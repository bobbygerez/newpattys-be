<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;

class ModelListing extends Model
{

    use Optimuss;
    protected $table = 'model_listings';
    protected $fillable = ['model', 'name'];
    protected $appends = ['label', 'value'];

    public function getLabelAttribute(){
        return $this->name;
    }

}
