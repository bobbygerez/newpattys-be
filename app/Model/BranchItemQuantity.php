<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BranchItemQuantity extends Model
{
    
    protected $table = 'branch_item_quantities';
    protected $fillable = ['branch_item_id', 'quantity'];
}
