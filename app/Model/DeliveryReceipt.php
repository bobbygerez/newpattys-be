<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeliveryReceipt extends Model
{

    protected $table = 'delivery_receipt';
    protected $fillable = [
        'invoice_internal_id',
        'receiptable_id',
        'receiptable_type',
        'product_transfer_id',
        'no',
    ];

    public function invoiceInternal()
    {
        return $this->hasOne('App\Model\InvoiceInternal', 'id', 'invoice_internal_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Model\CompanyItem', 'dr_products', 'delivery_receipt_id', 'company_item_id')
            ->withPivot(['qty', 'price'])
            ->withTimestamps();
    }
}
