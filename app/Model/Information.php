<?php

namespace App\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\Obfuscate\Optimuss;
class Information extends Model
{

    use Optimuss, SoftDeletes;
    
    protected $table = 'informations';
    protected $fillable = ['informationable_id', 'informationable_type', 'firstname', 'middlename', 'lastname', 'employee_id', 'user_id', 'gender_id', 'birthdate', 'mobile', 'nationality', 'civil_status_id'];
    protected $appends = ['optimus_id', 'name', 'string_birth', 'informationable_optimus'];

    public function address(){

    	return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function gender(){

    	return $this->hasOne('App\Model\Gender', 'id', 'gender_id');
    }

    public function civilStatus(){

    	return $this->hasOne('App\Model\CivilStatus', 'id', 'civil_status_id');
    }

    public function informationable()
    {
        return $this->morphTo();
    }

    public function getNameAttribute(){
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getStringBirthAttribute(){
        return Carbon::parse($this->birthdate)->toFormattedDateString();
    }

    public function getCreatedAtAttribute($val){
        return Carbon::parse($val)->toDayDateTimeString();
    }

    public function scopeRelTable($q){
        $q->with(['address.country', 'address.region', 'address.province', 'address.city', 'address.brgy', 'gender', 'civilStatus', 'informationable']);
    }

    public function getInformationableOptimusAttribute(){
        return $this->optimus()->decode($this->informationable_id);
    }
}
