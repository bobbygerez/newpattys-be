<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AreaBranch extends Model
{
    protected $table = 'area_branches';
    protected $fillable =[
        'area_id',
        'branch_id'
    ];
    

    public function branch(){
        return $this->hasOne('App\Model\Branch', 'id', 'branch_id');
    }

    
}
