<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SaleOrder extends Model
{
    
    protected $table = 'sale_orders';
    protected $fillable = ['purchase_order_id', 'sale_order_no', 'sale_order_code', 'name', 'orderable_id', 'orderable_type', 'prepared_by', 'noted_date', 'noted_by', 'freight', 'sub_total', 'vat_total', 'tax_total', 'grand_total'];

    public function preparedBy(){
        return $this->hasOne('App\Model\User', 'id', 'prepared_by');
    }

    public function notedBy(){
        return $this->hasOne('App\Model\User', 'id', 'noted_by');
    }

    public function purchaseOrder(){
        return $this->hasOne('App\Model\PurchaseOrder', 'id', 'purchase_order_id');
    }

    public function products(){
        return $this->belongsToMany('App\Model\Product', 'product_sale_order', 'sale_order_id', 'product_id')
                    ->withPivot('id', 'qty', 'price', 'freight', 'sub_total')
                    ->withTimestamps();
    }
}
