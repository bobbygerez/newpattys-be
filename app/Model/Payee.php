<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payee extends Model
{
    
    protected $table = 'payees';
    protected $fillable = [
        'transaction_id',
        'payable_id',
        'payable_type'
    ];

    protected $appends = ['entity'];

    public function payable(){

        return $this->morphTo();
    }

    public function getEntityAttribute(){

        return $this->payable_type::where('id', $this->payable_id)->with(['address.province', 'address.city', 'address.brgy'])->first();
    }
}
