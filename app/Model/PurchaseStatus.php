<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class PurchaseStatus extends Model
{
    use Globals, Optimuss;

    protected $table = 'purchase_status';
    protected $fillable = ['name'];

    protected $appends = ['optimus_id', 'label', 'value'];

    public function getLabelAttribute(){
        return $this->name;
    }
}
