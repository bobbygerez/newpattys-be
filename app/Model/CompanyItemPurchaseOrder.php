<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
class CompanyItemPurchaseOrder extends Model
{

    use Optimuss;

    protected $table = 'company_item_purchase_order';
    
    protected $fillable = [
        'company_item_id', 
        'purchase_order_id', 
        'qty', 'price',
        'freight', 'tax', 
        'vendorable_id', 
        'vendorable_type',
        'orderable_id', 
        'orderable_type',
        'token'
    ];

     protected $appends = ['optimus_id', 'total', 'label', 'value'];

    public function companyItem(){
        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }

    public function vendorable(){
        return $this->morphTo();
    }

    public function getTotalAttribute(){
        return $this->qty * $this->price;
    }

    public function orderable(){
        return $this->orderable_type::find($this->orderable_id);
    }

    public function purchaseOrderNo(){
        return $this->hasOne('App\Model\PurchaseOrderNo', 'id', 'purchase_order_no_id');
    }

    public function getFreightAttribute($val){
        return (float)$val;
    }

     public function getLabelAttribute(){
        return $this->purchaseOrderNo->po_no;
    }
}
