<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PurchaseRequestScope;
use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;
class PurchaseRequest extends Model
{
    use Globals, Optimuss, SoftDeletes;
    protected $table = 'purchase_requests';
    protected $fillable = [
        'name', 'purchase_request_code', 'purchase_request_no', 'purchasable_type', 'purchasable_id', 'prepared_by',
        'noted_date', 'checked_by', 'approved_by', 'remarks'
    ];
    protected $appends = ['optimus_id', 'has_comment'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new PurchaseRequestScope);
    }


    public function purchaseRequestCompanyItems(){
        return $this->hasMany('App\Model\PurchaseRequestCompanyItem', 'purchase_request_id', 'id');
    }

    public function purchasable(){

        return $this->morphTo();
    }

    public function preparedBy(){
        return $this->hasOne('App\Model\User', 'id', 'prepared_by');
    }

    public function checkedBy(){
        return $this->hasOne('App\Model\User', 'id', 'checked_by');
    }

    public function approvedBy(){
        return $this->hasOne('App\Model\User', 'id', 'approved_by');
    }

    public function scopeForPurchaseOrder($q){
        $q->where('checked_by', '!=', null)->where('approved_by', '!=', null);
        
    }

    public function scopeRelTable($q){
        return $q->with(['preparedBy', 'notedBy', 'approvedBy','purchasable']);
    }

    public function getIdAttribute($val){

        return $val;
    }
    public function getOrderDateAttribute($val){

        return Carbon::parse($val)->toDayDateTimeString();
    }

    public function getNotedDateAttribute($val){

        if($val != null){
            return Carbon::parse($val)->toDayDateTimeString();
        }
       
    }

    public function getApprovedDateAttribute($val){

        if($val != null){
            return Carbon::parse($val)->toDayDateTimeString();
        }
        
    }

    public function getHasCommentAttribute(){
        $prci = collect($this->purchaseRequestCompanyItems)->filter(function($v){
            return $v->comment != "";
        })->first();

        if($prci){
            return true;
        }

        return false;
    }
}
