<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\CompanyItemScope;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;
class CompanyItem extends Model
{
    
    use  Optimuss, SoftDeletes;

    protected $table = 'company_items';
    protected $fillable = [
        'category_id',
        'package_id',
        'company_id',
        'name',
        'amount',
        'sku',
        'barcode',
        'min_qty',
        'reorder_qty',
        'max_qty',
        'freight'
    ];
    protected $appends = ['optimus_id', 'float_amount', 'string_price', 'default_image'];

    // protected static function boot(){
    //     parent::boot();
    //     static::addGlobalScope(new CompanyItemScope);
    // }

    public function package(){
        return $this->hasOne('App\Model\Package', 'id', 'package_id');
    }

     public function images()
    {
        return $this->morphMany('App\Model\Image', 'imageable', 'imageable_type', 'imageable_id');
    }

    public function category(){
        return $this->hasOne('App\Model\Category', 'id', 'category_id');
    }

    public function getFloatAmountAttribute(){
        return (float)$this->amount;
    }

    public function getStringPriceAttribute(){
        return number_format($this->amount,2,".",",");
    }

    public function vendorProduct(){
        return $this->hasMany('App\Model\VendorProduct', 'company_item_id', 'id');
    }

    public function branchItem(){
        return $this->hasOne('App\Model\BranchItem', 'company_item_id', 'id');
    }

    public function branchItemNoGlobalScope(){
        return $this->hasOne('App\Model\BranchItem', 'company_item_id', 'id')->withoutGlobalScopes();
    }

    public function companyItemVendorRanks(){
        return $this->hasMany('App\Model\companyItemVendorRank', 'company_item_id', 'id');
    }

     public function getDefaultImageAttribute(){

        return collect($this->images)->filter(function($v){
            return $v->is_primary == 1;
        })->first();
    }
    
    // public function purchaseRequest(){
    //     return $this->belongsToMany('App\Model\PurchaseRequest', 'company_item_purchase_request', 'company_item_id', 'purchase_request_id')
    //                 ->withPivot('qty')
    //                 ->withTimestamps();
    // }



}
