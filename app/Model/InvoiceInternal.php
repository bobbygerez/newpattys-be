<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceInternal extends Model
{
    protected $table = 'invoice_internal';
    protected $fillable = [
        'invoiceable_id',
        'invoiceable_type',
        'invoice_no'	
    ];

    public function products(){
        return $this->belongsToMany('App\Model\CompanyItem', 'ii_products', 'invoice_internal_id', 'company_item_id')->withPivot(['qty', 'price'])->withTimestamps();
    }
}
