<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GlAccount extends Model
{
    
    protected $table = 'gl_accounts';
    protected $fillable = [
        'branch_chart_account_id',
        'amount',
        'accountable_id',
        'accountable_type'
    ];

    public function branchChartAccount(){
        return $this->hasOne('App\Model\BranchChartAccount', 'id', 'branch_chart_account_id');
    }
}
