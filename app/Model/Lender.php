<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lender extends Model
{
    
    protected $table='lenders';
    protected $fillable = [
        'code', 'name', 'branch_id', 'orig_code', 'visible'
    ];
}
