<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductTransaction extends Model
{
    protected $table = 'product_transaction';
    protected $fillable = [
        'transaction_id',
        'product_id', 'desc', 'discount_amt',
        'qty', 'price', 'amount', 'chart_account_id',
        'tax_type_id', 'job_id', 'debit_amount'

    ];

    public function getDiscountAmtAttribute($val){
        return (float)$val;
    }

    public function getPriceAttribute($val){
        return (float)$val;
    }

    public function getAmountAttribute($val){
        return (float)$val;
    }

    public function getDebitAmountAttribute($val){
        return (float)$val;
    }

    public function taxType(){
        return $this->hasOne('App\Model\TaxType', 'id', 'tax_type_id');
    }

    public function chartAccount(){
        return $this->hasOne('App\Model\ChartAccount', 'id', 'chart_account_id');
    }

    public function product(){
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
}
