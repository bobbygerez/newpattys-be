<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorProduct extends Model
{
    
    use Optimuss, SoftDeletes;
    protected $table = 'vendor_products';
    protected $fillable = ['company_item_id', 'price', 'vendor_id'];
    protected $appends = ['optimus_id', 'string_price'];
    public function vendor(){
        return $this->belongsTo('App\Model\Vendor');
    }

    public function getPriceAttribute($val){
        return (float)$val;
    }

    public function getStringPriceAttribute(){
        return number_format($this->price,2,".",",");
    }

    public function companyItem(){
        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }

}
