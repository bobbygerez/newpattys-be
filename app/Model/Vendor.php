<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Vendor extends Model
{
    
    use Optimuss, SoftDeletes;
    protected $table = 'vendors';
    protected $fillable = [
        'name', 'token', 'vendorable_id', 'vendorable_type'
    ];
    protected $appends = ['optimus_id', 'entity', 'value', 'label'];

    public static function boot(){
        parent::boot();
        // $request = app()->make('request');
        // static::addGlobalScope('entityVendor', function(Builder $builder) use ($request){
        //     return $builder->where('vendorable_type', $request->accessable_type)
        //             ->where('vendorable_id',  self::remove($request->accessable_id));	
        // });
    }

    public function address()
    {
        return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function vendorable(){

        return $this->morphTo();
   }

    public function getCreatedAtAttribute($val){
        return Carbon::parse($val)->toDayDateTimeString();
    }

    public function vendorProducts(){
        return $this->hasMany('App\Model\VendorProduct', 'vendor_id', 'id');
    }

    public function companyItems(){
        return $this->belongsToMany('App\Model\CompanyItem', 'vendor_products', 'vendor_id', 'company_item_id')
            ->withPivot('price')
            ->withTimestamps();
    }

    public function getEntityAttribute()
    {
        return 'App\Model\Vendor';
    }

    public function getLabelAttribute(){
        return $this->name;
    }



}
