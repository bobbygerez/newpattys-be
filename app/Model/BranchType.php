<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
class BranchType extends Model
{
    use Optimuss;

    protected $table = 'branch_types';
    protected $fillable = ['name'];

    protected $appends = ['optimus_id'];
}
