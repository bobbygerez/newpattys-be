<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\AreaScope;
use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
class Area extends Model
{
    use Globals, Optimuss;
    
    protected $table = 'areas';
    protected $fillable = [
        'name',
        'areable_id',
        'areable_type'
    ];

    protected $appends = ['optimus_id', 'entity',  'value', 'label'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new AreaScope);

        static::deleting(function($area) { 
             foreach ( $area->areaBranches as $ab){
                $ab->delete();
             }
        });
    }

    public function areaBranches(){
        return $this->hasMany('App\Model\AreaBranch', 'area_id', 'id');
    }

    public function getEntityAttribute()
    {
        return 'App\Model\Area';
    }

    public function getLabelAttribute(){
        return $this->name;
    }
}
