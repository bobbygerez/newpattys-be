<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
class ShippingTerm extends Model
{
    use Optimuss;
    protected $table = 'shipping_terms';
    protected $fillable = ['name'];
    protected $appends = ['label', 'value'];

    public function getLabelAttribute(){
        return $this->name;
    }
}
