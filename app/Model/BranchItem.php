<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\BranchItemScope;
use App\Traits\Obfuscate\Optimuss;

class BranchItem extends Model
{

    use Optimuss;

    protected $table = 'branch_items';
    protected $fillable = ['branch_id', 'company_item_id', 'rack_id', 'rack_row_id', 'rack_column_id'];

    protected $appends = ['total_qty', 'optimus_id', 'label', 'value'];

    protected static function boot(){
        parent::boot();
        //static::addGlobalScope(new BranchItemScope);
        static::deleting(function($branchItem) {
            $branchItem->qty()->delete();
        });
    }

    public function scopeOnlyBranch($q){

        $request = app()->make('request');
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);

        if($accessables['accessable_type'] == 'App\Model\Branch'){
            return $q->where( 'branch_id', $this->removeStringEncode($accessables['accessable_id']) );
        }

    }
    public function scopeProducts($q){
        return $q->whereHas('companyItem.category', function($qq){
            $qq->where('name', 'Product');
        })->with(['companyItem.images', 'companyItem.category.children']);

    }
    public function branch(){
        return $this->hasOne('App\Model\Branch', 'id', 'branch_id');
    }
    public function companyItem(){
        return $this->hasOne('App\Model\CompanyItem', 'id', 'company_item_id');
    }
    public function qty(){
        return $this->hasMany('App\Model\BranchItemQuantity', 'branch_item_id', 'id');
    }

    public function getTotalQtyAttribute(){
        return collect( $this->qty )->sum('quantity');
    }

    public function getLabelAttribute(){
        // return $this->companyItem->name;
    }
    
}
