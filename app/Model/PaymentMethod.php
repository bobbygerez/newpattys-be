<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;

class PaymentMethod extends Model
{
    use Optimuss;
    protected $table = 'payment_methods';
    protected $fillable = ['name'];
    
    protected $appends = ['optimus_id'];
}
