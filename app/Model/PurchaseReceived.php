<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
use App\Model\GlAccount;

class PurchaseReceived extends Model
{
    use Optimuss, Globals;
    protected $table = 'purchase_received';
    						
    protected $fillable = [
       'purchase_received_no', 'purchase_order_no_id', 'invoice_no', 'delivery_receipt_no', 'vendorable_id', 'vendorable_type', 'purchasable_id', 'purchasable_type', 'received_by', 'token'
    ];

    protected $appends = ['optimus_id', 'pr_total'];

    public static function boot() {
        parent::boot();
        static::deleting(function($pr) {
            foreach($pr->purchaseReceivedItems as $item){
                $item->delete();
            }

            foreach($pr->purchaseReceivedPurchasesItems as $item){
                $item->delete();
            }
        });
    }
    
    public function purchaseOrderNo(){
        return $this->hasOne('App\Model\PurchaseOrderNo', 'id', 'purchase_order_no_id');
    }

    public function scopeGlAccount(){

        return GlAccount::where('accountable_id', $this->id)
            ->where('accountable_type', 'App\Model\PurchaseReceived')
            ->with(['branchChartAccount'])
            ->first();
    }

    public function purchasable(){

        return $this->morphTo();
    }

    public function vendorable(){

        return $this->morphTo();
    }

    public function purchaseReceivedItems(){
        return $this->hasMany('App\Model\PurchaseReceivedItem', 'purchase_received_id', 'id');
    }

     public function purchaseReceivedPurchasesItems(){
        return $this->hasMany('App\Model\PurchaseReceivedPurchaseItem', 'purchase_received_id', 'id');
    }

    public function getPrTotalAttribute(){

        $array = collect( $this->purchaseReceivedItems)->map(function($v){
            return $v->amount * $v->received_qty;
        })->values()->all();

        return array_sum($array);
    }

    
}
