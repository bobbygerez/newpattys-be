<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;

class Assignment extends Model
{
    use Optimuss;
    protected $table = 'assignments';
    protected $fillable = [
        'user_id', 'assignmentable_type', 'assignmentable_id', 'is_default'
    ];

    protected $appends = ['optimus_id'];

    public function assignmentable()
    {
        return $this->morphTo();
    }

}
