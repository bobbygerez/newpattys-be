<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Model\Globals;
use App\Scopes\BranchScope;

class Branch extends Model
{
    use Globals, Optimuss, SoftDeletes;

    protected $table = 'branches';
    protected $fillable = [
        'name', 'branch_type_id', 'company_id', 'code', 'acronym', 'branch_address', 'initial', 'tel', 'bir', 'desc'
    ];

    protected $appends = ['entity', 'optimus_id', 'letter_optimus', 'entity_model', 'value', 'label'];

    protected static function boot(){
        parent::boot();
        //Disable this global scope... I changed it to local scope
       // static::addGlobalScope(new BranchScope); 
    }

    public function scopeAccess($q){
        $request = app()->make('request');
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        if($accessables['accessable_type'] == 'App\Model\Company'){
            return $q->where('company_id', $this->removeStringEncode($accessables['accessable_id']));
        }
        if($accessables['accessable_type'] == 'App\Model\Branch'){
            return $q->where('id', $this->removeStringEncode($accessables['accessable_id']));
        }
    }

    public function areaBranch(){
        return $this->hasOne('App\Model\AreaBranch', 'branch_id', 'id');
    }

    public function jobs()
    {
        return $this->morphmany('App\Model\Job', 'jobable');
    }

    public function accessables(){

        return $this->belongsToMany('App\Model\Accessable')->withTimestamps();
    }
    public function informations(){

        return $this->morphMany('App\Model\Information', 'informationable');
    }

    public function users(){

        return $this->morphMany('App\Model\User', 'userable');
    }
    public function roles(){

        return $this->morphMany('App\Model\Role', 'roleable');
    }

    public function employees(){

        return $this->morphMany('App\Model\Employee', 'employable');
    }

    public function transactions()
    {

        return $this->morphMany('App\Model\Transaction', 'transactable');
    }

    public function purchases()
    {

        return $this->morphMany('App\Model\Purchase', 'purchasable');
    }

    public function purchaseReceived()
    {

    	return $this->morphMany('App\Model\PurchaseReceived', 'purchasable');
    }

    public function saleInvoices()
    {

    	return $this->morphMany('App\Model\SaleInvoice', 'salable');
    }

    public function items()
    {

        return $this->morphMany('App\Model\Item', 'itemable');
    }

    public function products()
    {

        return $this->morphMany('App\Model\Product', 'productable');
    }

    public function address()
    {

        return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function franchisee()
    {

        return $this->morphOne('App\Model\Franchisee', 'franchisable');
    }

    public function category()
    {

        return $this->morphOne('App\Model\Category', 'categorable');
    }

    public function company()
    {
        return $this->belongsTo('App\Model\Company');
    }

    public function businessInfo()
    {

        return $this->morphOne('App\Model\BusinessInfo', 'businessable');
    }

    public function scopeRelTable($query)
    {

        return $query->with(['purchaseReceived.products', 'address.region', 'address.province', 'address.city', 'address.brgy', 'company', 'businessInfo', 'products.taxType', 'transactions.chartAccount', 'transactions.transactionType', 'transactions.payee']);
    }

    public function accessRights()
    {
        return $this->morphToMany('App\Model\AccessRight', 'accessable');
    }

    public function getLabelAttribute(){
        return $this->name;
    }
    public function branchType(){

        return $this->hasOne('App\Model\BranchType', 'id', 'branch_type_id');
    }
    public function getEntityAttribute()
    {

        return 'App\Model\Branch';
    }

    public function getEntityModelAttribute()
    {

        return 'Branch';
    }

    public function getCodeAttribute($val){
        return str_pad($val, 3, '0', STR_PAD_LEFT);
    }

    public function getLetterOptimusAttribute(){
        return substr($this->name, 0, 5)  . $this->optimus()->decode($this->id);
    }

   
    
}
