<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyItemVendorRank extends Model
{
    
    protected $table = 'company_item_vendor_rank';
    protected $fillable = [
        'company_item_id',
        'vendorable_id',
        'vendorable_type',
        'rank',
        'percentage'
    ];
    

    public function branchItem(){
        return $this->hasOne('App\Model\BranchItem', 'company_item_id', 'company_item_id');
    }

    public function vendorProduct(){

        return $this->hasOne('App\Model\VendorProduct', 'company_item_id', 'company_item_id');
    }

    public function vendorable(){
       return $this->morphTo();
    }

}
