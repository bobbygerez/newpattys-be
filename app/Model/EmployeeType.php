<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    
    protected $table = 'employee_types';
    protected $fillable = ['name', 'desc'];
}
