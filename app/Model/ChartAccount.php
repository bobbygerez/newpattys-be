<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Scopes\ChartAccountScope;

class ChartAccount extends Model
{
    use Optimuss;
    protected $table = 'chart_accounts';
    protected $fillable = [
        'company_id',
        'posting_code',
        'series_no',
        'parent_id',
        'parent_code',
        'taccount_id',
        'name',
        'account_code',
        'account_display',
        'postable',
        'trial_balance'
    ];

    protected $appends = ['optimus_id', 'name_post', 'label', 'value', 'trial_balance_word', 'postable_word'];

    public static function boot() {
        parent::boot();

        static::deleting(function($chartAccount) {
            $chartAccount->children()->delete();
        });

        static::addGlobalScope(new ChartAccountScope);
    }

    public function parent(){
        return $this->hasOne('App\Model\ChartAccount', 'account_code', 'parent_code');
    }
    public function children() {

        return $this->hasMany('App\Model\ChartAccount', 'parent_code', 'account_code');
    }

    public function allChildren()
    {
        return $this->children()->with(['allChildren', 'tAccount']);
    } 

    public function company(){
        return $this->belongsTo('App\Model\Company');
    }

    public function tAccount(){
        return $this->hasOne('App\Model\TAccount', 'id', 'taccount_id');
    }

    public function transactions(){
        return $this->hasMany('App\Model\Transaction', 'id', 'chart_account_id');
    }
    public function scopeRelTable($query){

        return $query->with(['allChildren', 'parent', 'company', 'tAccount']);
    }

    public function getNamePostAttribute(){
        if($this->posting_code != null){
            return '(' . $this->posting_code . ') ' . $this->name;
        }
        return $this->name;
    }

    public function getLabelAttribute(){
        return  $this->name . '-' . $this->account_code;
    }

    public function getTrialBalanceAttribute($val){
        if($val == 1){
            return true;
        }
        return false;
    }

    public function getPostableAttribute($val){
        if($val == 1){
            return true;
        }
        return false;
    }

    public function getTrialBalanceWordAttribute(){
        if($this->trial_balance == 1){
            return "true";
        }
        return "false";
    }

    public function getPostableWordAttribute($val){
        if($this->postable == 1){
            return "true";
        }
        return "false";
    }

    
}
