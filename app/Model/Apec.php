<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Apec extends Model
{
    
    protected $table='apecs';
    protected $fillable = ['name'];
}
