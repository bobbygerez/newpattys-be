<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShiftingDenomination extends Model
{
    
    protected $table = 'shifting_denomination';
    protected $fillable = ['shifting_id', 'denomination_id', 'qty'];

    public function denomination(){

        return $this->hasOne('App\Model\Denomination', 'id', 'denomination_id');
    }
}
