<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PosInvoice extends Model
{
    protected $table = 'pos_invoices';
    protected $fillable = [
        'user_id', 'branch_item_id', 'price', 'qty'
    ];

}
