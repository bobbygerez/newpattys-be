<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\TransactionSetupScope;
use App\Traits\Obfuscate\Optimuss;
class TransactionSetup extends Model
{
    use Optimuss;
    protected $table = 'transaction_setup';
    protected $fillable = [
        'model_listing_id',
        'model',
        'transactable_id',
        'transactable_type',
        'dr_chart_account_id',
        'cr_chart_account_id',
        'defaultt'
    ];

    protected $appends = [
        'optimus_id',
    ];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new TransactionSetupScope);
    }

    public function modelListing(){
        return $this->hasOne('App\Model\ModelListing', 'id', 'model_listing_id');
    }

    public function drBranchChartAccount(){
        return $this->hasOne('App\Model\BranchChartAccount', 'id', 'dr_chart_account_id');
    }

     public function crBranchChartAccount(){
        return $this->hasOne('App\Model\BranchChartAccount', 'id', 'cr_chart_account_id');
    }

    public function getDefaulttAttribute($val){
        if($val === 1){
            return true;
        }
        return false;
    }
}
