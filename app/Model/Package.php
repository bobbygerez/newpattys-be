<?php

namespace App\Model;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;

class Package extends Model
{
    use Optimuss;
    protected $table = 'packages';
    protected $fillable = [
        'name', 'desc'
    ];
    protected $appends = ['label', 'value', 'optimus_id'];

    public function getCreatedAtAttribute($val){

        return Carbon::parse($val)->toDayDateTimeString();
    }

    public function getLabelAttribute(){
        return $this->name;
    }
}
