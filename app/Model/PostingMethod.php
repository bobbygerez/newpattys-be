<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class PostingMethod extends Model
{

    use Optimuss, Globals;

    protected $table = 'posting_methods';
    protected $fillable = ['name'];
    protected $appends = ['label', 'value'];

    public function getLabelAttribute(){
        return $this->name;
    }
}
