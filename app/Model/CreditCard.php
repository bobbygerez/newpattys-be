<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    
    protected $table = 'credit_cards';
    protected $fillable = ['name', 'card_no'];

    public function setCardNoAttribute($val){
        $this->attributes['card_no'] = encrypt($val);
    }

}
