<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\Globals;
use App\Model\Branch;
use App\Scopes\FranchiseeScope;
use App\Traits\Obfuscate\Optimuss;
class Franchisee extends Model
{
    use Globals,  Optimuss;
    
    protected $table = 'franchisees';
    protected $fillable = [
        'name', 'desc', 'franchisable_id', 'franchisable_type', 'trademark_id'
    ];

    protected $appends = ['optimus_id'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new FranchiseeScope);
    }

    public function accessRights()
    {
        return $this->morphToMany('App\Model\AccessRight', 'accessable');
    }

    public function trademarks(){
        return $this->hasOne('App\Model\Trademark', 'id', 'trademark_id');
    }

    public function franchisable(){

        return $this->morphTo();
    }

    public function branch(){
        return $this->belongsTo(Branch::class);
    }

    public function scopeRelTable($query){

        return $query->with(['trademarks.company', 'accessRights.roles.users', 'franchisable']);
    }
}
