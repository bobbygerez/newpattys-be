<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
class Shifting extends Model
{
    use Globals, Optimuss;
    protected $table = 'shifting';
    protected $fillable = ['shiftable_id', 'shiftable_type', 'from', 'to', 'cash_on_hand', 'is_accepted', 'open', 'close'];
    protected $appends = ['optimus_id'];
    protected $casts = [
        'is_accepted' => 'boolean'
    ];

    public function fromUser(){
        return $this->hasOne('App\Model\User', 'id', 'from');
    }

    public function toUser(){
        return $this->hasOne('App\Model\User', 'id', 'to');
    }

    public function shiftingDenomination(){
        return $this->hasMany('App\Model\ShiftingDenomination', 'shifting_id', 'id');
    }
}
