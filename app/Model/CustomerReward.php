<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerReward extends Model
{
    protected $table = 'customer_rewards';
    protected $fillable = ['name', 'no'];
    protected $appends = ['balance'];

    public function getBalanceAttribute(){
        return 1000;
    }

}
