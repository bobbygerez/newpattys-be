<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\TransactionNo\ModelScope;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
use Carbon\Carbon;

class TransactionNo extends Model
{
    
    use Optimuss, Globals;
    protected $table = 'transaction_nos';
    protected $fillable = [ 
        'no', 
        'is_completed',
        'is_void', 
        'save_by',
        'transact_by',
        'grand_total',
        'enter_amount',
        'credit_card_id',
        'transactable_id', 
        'transactable_type'
    ];
    protected $appends = ['ago', 'optimus_id'];
    protected $casts = [
        'is_completed' => 'boolean',
        'is_void' => 'boolean'
    ];
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new ModelScope);
    }

    
    public function transactable()
    {
        return $this->morphTo();
    }

    public function transactBy(){
        
        return $this->hasOne('App\Model\User', 'id', 'transact_by');
    }

    public function saveBy(){
        
        return $this->hasOne('App\Model\User', 'id', 'save_by');
    }

    public function branchItemTrans(){
        
        return $this->hasMany('App\Model\BranchItemTrans', 'transaction_no_id', 'id');
    }

    public function getAgoAttribute(){
        return Carbon::parse($this->created_at)->diffForHumans(); 
    }
}
