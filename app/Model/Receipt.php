<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    
    protected $table = 'receipts';
    protected $fillable = [
        'receiptable_id',
        'receiptable_type',
        'trans_date',
        'deposit_date',
        'chart_account_id',
        'total_amount',
        'total_discount',
        'remarks',
        'checknumber',
        'refnum',
        'receipt_num',
        'status',
        'vat_amount',
        'created_by',
    ];
}
