<?php 

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\Obfuscate\Optimuss;

class PurchaseRequestScope implements Scope
{

    use Optimuss;
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $request = app()->make('request');
        if($request->header('accessables') != null){
            $accessables = array();
            parse_str($request->header('accessables'), $accessables);
            if($accessables['accessable_type'] == 'App\Model\Branch'){
                $builder->where('purchasable_type', $accessables['accessable_type']);
                $builder->where('purchasable_id', $this->removeStringEncode($accessables['accessable_id']));
            }
        }

        
    }
}

?>