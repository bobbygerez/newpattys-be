<?php 

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\Obfuscate\Optimuss;

class BranchItemScope implements Scope
{

    use Optimuss;
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $request = app()->make('request');
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        if($request->header('accessables') != null){
            parse_str($request->header('accessables'), $accessables);
            if($accessables['accessable_type'] == 'App\Model\Company'){
                $branchIds = $accessables['accessable_type']::where('id', $this->removeStringEncode($accessables['accessable_id']) )
                ->with('branches')
                ->first()
                ->branches
                ->pluck('id')
                ->values()
                ->all();

                $builder->whereIn('branch_id', $branchIds);
            }

            if($accessables['accessable_type'] == 'App\Model\Branch'){
                $builder->where( 'branch_id', $this->removeStringEncode($accessables['accessable_id']) );
            }
            
        }
        
    }
}

?>