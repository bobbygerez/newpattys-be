<?php 

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\Obfuscate\Optimuss;

class RackScope implements Scope
{

    use Optimuss;
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $request = app()->make('request');
        $accessables = array();
        
        if($request->header('accessables') != null){
            parse_str($request->header('accessables'), $accessables);
            $builder->where('rackable_type', $accessables['accessable_type']);
            $builder->where('rackable_id', $this->removeStringEncode($accessables['accessable_id']));
        }
        
    }
}

?>