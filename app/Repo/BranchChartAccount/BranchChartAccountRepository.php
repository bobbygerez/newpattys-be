<?php 

namespace App\Repo\BranchChartAccount;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\BranchChartAccount;
use App\Model\ChartAccount;

class BranchChartAccountRepository extends BaseRepository implements BranchChartAccountInterface{


    public function __construct(){

        $this->modelName = new BranchChartAccount();
    
    }
    public function index($request){

   $chartAccounts = $this->modelName->when(request('filter') !== null, function ($q) use ($request) {
                        return $q->where('name', 'LIKE', '%' . $request->filter . '%');
                    })
                    ->with(['allChildren', 'tAccount'])
                    ->orderBy('parent_id', 'desc')
                    ->get();

    
    
    $childIds = [];
    foreach($chartAccounts as $chartAccount){
        $childIds[] = $this->getIdRecursive($chartAccount->allChildren);
    }
    $childIds = collect($childIds)->flatten(1)->values()->all();

    $filtered = $chartAccounts->filter(function($v) use ($childIds) {
        return !in_array($v->id, $childIds);
    })->values()->all();
    
        return $this->paginate( collect($filtered) );
    }

    public function store($request){

        $branchChartAccount = $this->modelName->find($request->chart_account_id);
        if($branchChartAccount != null){
            return response()->json([
                'message' => 'Chart Account already exist.'
            ], 404);
        }
        $chartAccount = ChartAccount::where('id', $request->chart_account_id)->with(['allChildren'])->first();
        $newRequest = $request->all();
        								
        $newRequest['chartable_id'] = $this->accessable_id();
        $newRequest['chartable_type'] = $this->accessable_type();
        $newRequest['parent_id'] = $chartAccount->parent_id;
        $newRequest['accounting_standard_id'] = $chartAccount->accounting_standard_id;
        $newRequest['taccount_id'] = $chartAccount->taccount_id;
        $newRequest['account_code'] = $chartAccount->account_code;
        $newRequest['parent_code'] = $chartAccount->parent_code;
        $newRequest['name'] = $chartAccount->name;
        $newRequest['posting_code'] = $chartAccount->posting_code;
        $newRequest['account_display'] = $chartAccount->account_display;
        $newRequest['series_no'] = $chartAccount->series_no;
        $newRequest['postable'] = $chartAccount->postable;
        $newRequest['trial_balance'] = $chartAccount->trial_balance;
        $this->create($newRequest);

        $this->recursive($chartAccount->allChildren);
        
    }

    public function recursive($array) {
        foreach ($array as $c) {
            $chartA = $this->modelName->find($c->id);
            if($chartA == null){
                $newRequest['chartable_id'] = $this->accessable_id();
                $newRequest['chartable_type'] = $this->accessable_type();
                $newRequest['parent_id'] = $c->parent_id;
                $newRequest['accounting_standard_id'] = $c->accounting_standard_id;
                $newRequest['taccount_id'] = $c->taccount_id;
                $newRequest['account_code'] = $c->account_code;
                $newRequest['parent_code'] = $c->parent_code;
                $newRequest['name'] = $c->name;
                $newRequest['posting_code'] = $c->posting_code;
                $newRequest['account_display'] = $c->account_display;
                $newRequest['series_no'] = $c->series_no;
                $newRequest['postable'] = $c->postable;
                $newRequest['trial_balance'] = $c->trial_balance;
                $this->create($newRequest);
            }
            $this->recursive($c['allChildren']);
        }
    }

    public function remove($request){
        $chartAccount = $this->modelName->where('id', $request->id)->with(['allChildren'])->first();
        $this->delRecursive($chartAccount->allChildren);
        $chartAccount->delete();
    }

    public function delRecursive($array){
        foreach ($array as $c) {
            $this->modelName->where('id', $c->id)
            ->first()
            ->delete();
            $this->delRecursive($c['allChildren']);
        }
    }

    public function getIdRecursive($array) {
        $result = [];
        foreach ($array as $item) {
             $result[] = $item['id'];
            $result = array_merge($result, $this->getIdRecursive($item['allChildren']));
        }
        return array_filter($result);
    }


}
