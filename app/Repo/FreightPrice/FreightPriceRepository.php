<?php 

namespace App\Repo\FreightPrice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\FreightPrice;

class FreightPriceRepository extends BaseRepository implements FreightPriceInterface{


    public function __construct(){

        $this->modelName = new FreightPrice();
    
    }

    public function index($request){

        $freightPrices =  $this->modelName->orderBy('created_at', 'desc')->get();

        $filtered = collect($freightPrices)->when($request->filter != '', function ($freightPrices) use ($request) {
            return $freightPrices->filter(function ($val) use ($request) {
                return false !== stristr($val->from['name'], $request->filter);
            })->values()->all();

        });
        return $this->paginate( collect($filtered) );

    }

    public function store($request){
        
        $newRequest = $request->all();
        $newRequest['freightable_type'] = $this->accessable_type();
        $newRequest['freightable_id'] = $this->accessable_id();
        $newRequest['from_id'] = $this->removeStringEncode(  $request->from_id );
        $newRequest['to_id'] = $this->removeStringEncode(  $request->to_id );
        $newRequest['shipping_method_id'] = $this->removeStringEncode(  $request->shipping_method_id );
        $newRequest['shipping_term_id'] = $this->removeStringEncode(  $request->shipping_term_id );
        $newRequest['payment_term_id'] = $this->removeStringEncode(  $request->payment_term_id );
        $this->create( $newRequest );

    }

    public function edit($request){

        return $this->where('id', $request->id)->with(['shippingTerm', 'shippingMethod', 'paymentTerm'])->first();
    }

    public function update($request){

        $newRequest = $request->all();
        $newRequest['from_id'] = $this->removeStringEncode(  $request->from_id );
        $newRequest['to_id'] = $this->removeStringEncode(  $request->to_id );
        $newRequest['shipping_method_id'] = $this->removeStringEncode(  $request->shipping_method_id );
        $newRequest['shipping_term_id'] = $this->removeStringEncode(  $request->shipping_term_id );
        $newRequest['payment_term_id'] = $this->removeStringEncode(  $request->payment_term_id );
        $freightPrice = $this->find($request->id);
        $freightPrice->update($newRequest);
    }

}