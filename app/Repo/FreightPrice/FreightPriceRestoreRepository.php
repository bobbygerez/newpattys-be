<?php 

namespace App\Repo\FreightPrice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\FreightPrice;

class FreightPriceRestoreRepository extends BaseRepository implements FreightPriceInterface{


    public function __construct(){

        $this->modelName = new FreightPrice();
    
    }

    public function index($request){

        $freightPrices =  $this->modelName->withTrashed()->get();

        $filtered = collect($freightPrices)->filter(function($v){
            return $v->deleted_at != null;
        })->when($request->filter != '', function ($freightPrices) use ($request) {
            return $freightPrices->filter(function ($val) use ($request) {
                return false !== stristr($val->from['name'], $request->filter);
            })->values()->all();

        });
        return $this->paginate( collect($filtered) );

    }

    public function store($request){
        
        $newRequest = $request->all();
        $newRequest['freightable_type'] = $this->accessableHeader()['accessable_type'];
        $newRequest['freightable_id'] = $this->removeStringEncode( $this->accessableHeader()['accessable_id'] );
        $newRequest['from_id'] = $this->removeStringEncode(  $request->from_id );
        $newRequest['to_id'] = $this->removeStringEncode(  $request->to_id );
        $this->create( $newRequest );

    }

    public function edit($request){

        return $this->find($request->id);
    }

    public function update($request){

        $newRequest = $request->all();
        $newRequest['from_id'] = $this->removeStringEncode(  $request->from_id );
        $newRequest['to_id'] = $this->removeStringEncode(  $request->to_id );
        $freightPrice = $this->find($request->id);
        $freightPrice->update($newRequest);
    }

}