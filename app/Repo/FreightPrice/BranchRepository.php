<?php 

namespace App\Repo\FreightPrice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Branch;

class BranchRepository extends BaseRepository implements FreightPriceInterface{


    public function __construct(){

        $this->modelName = new Branch();
    
    }

    public function index(){
        return $this->modelName->all()->map(function($val){
            return [
                'label' => $val->name,
                'value' => $val->name . $val->optimus_id,
                'entity' => 'App\Model\Branch'
            ];
        })->values()->all();

    }


}