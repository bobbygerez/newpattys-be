<?php 

namespace App\Repo\FreightPrice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Branch;
use App\Model\Franchisee;
class BranchFranchiseRepository extends BaseRepository implements FreightPriceInterface{


    public function index(){
        $branches = Branch::all()->map(function($val){
            return [
                'label' => $val->name,
                'value' => $val->name . $val->optimus_id,
                'entity' => 'App\Model\Branch'
            ];
        })->values()->all();
        $franchisees = Franchisee::all()->map(function($val){
            return [
                'label' => $val->name,
                'value' => $val->name . $val->optimus_id,
                'entity' => 'App\Model\Franchisee'
            ];
        })->values()->all();

        return array_merge($branches, $franchisees);

    }
}