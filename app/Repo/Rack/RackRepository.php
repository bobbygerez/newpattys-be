<?php 

namespace App\Repo\Rack;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Rack;
use App\Model\RackRow;
use App\Model\RackColumn;
class RackRepository extends BaseRepository implements RackInterface{


    public function __construct(){

        $this->modelName = new Rack();
    
    }

    public function index($request){
        return $this->paginate(
            $this->modelName->when($request->filter != '', function($q) use ($request){
                $q->where('name', 'like', '%'. $request->filter . '%');
            })
            ->orderBy('created_at', 'desc')
            ->get()
        );
    }

    public function store($request){
        
        $newRequest = $request->all();
        $newRequest['rackable_id'] = $this->accessable_id();
        $newRequest['rackable_type'] = $this->accessable_type();
        $rack = $this->create($newRequest);

        for($i=1; $request->rows >= $i; $i++){
            $rackRow = RackRow::create([
                'rack_id' => $rack->id,
                'no' => $i,
                'rackable_id' => $this->accessable_id(),
                'rackable_type' => $this->accessable_type()
            ]);
        }

        for($i=1; $request->columns >= $i; $i++){
            $rackRow = RackColumn::create([
                'rack_id' => $rack->id,
                'no' => $i,
                'rackable_id' => $this->accessable_id(),
                'rackable_type' => $this->accessable_type()
            ]);
        }
    }

    public function updatee($request){
        $rack = $this->where('id', $request->optimus_id)->first();
        $rc = RackColumn::where('rack_id', $rack->id)->get();
            foreach($rc as $c){
                $c->delete();
            }
        for($i=1; $request->columns >= $i; $i++){
            $rackRow = RackColumn::create([
                'rack_id' => $rack->id,
                'no' => $i,
                'rackable_id' => $this->accessable_id(),
                'rackable_type' => $this->accessable_type()
            ]);
        }

        $rr = RackRow::where('rack_id', $rack->id)->get();
        foreach($rr as $r){
                $r->delete();
            }
        for($i=1; $request->rows >= $i; $i++){
            $rackRow = RackRow::create([
                'rack_id' => $rack->id,
                'no' => $i,
                'rackable_id' => $this->accessable_id(),
                'rackable_type' => $this->accessable_type()
            ]);
        }
        
    }
    
}
