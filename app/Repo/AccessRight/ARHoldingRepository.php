<?php 

namespace App\Repo\AccessRight;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\AccessRight;
use App\Model\Role;
use App\Traits\Obfuscate\Optimuss;
class ARHoldingRepository extends BaseRepository implements AccessRightInterface{

    use Optimuss;
    
    public function __construct(){

        $this->modelName = new AccessRight();
    
    }
    public function index($request){
       $roles = $this->modelName->with('roles.accessRights')->get()->flatMap(function($value){
            return $value->roles;
        });
       $roles = collect($roles)->filter(function($value) use ($request){
            return $value->pivot['access_type'] == 'App\Model\Holding' && $value->pivot['access_id'] == $this->optimus()->encode($request->accessId);
        });
       return collect($roles)->unique('id');
    }
    public function arUpdate($request){

       $arIds = [];
       foreach($request->accessRightIds as $id){
           //add associative array to $arIds
            $arIds += [$id => [
                'access_id' => $this->optimus()->encode($request->accessible_id),
                'access_type' => $request->accessible_type
            ] ];
       }

       $roles =  Role::whereIn('id', $request->roles)->get();

       foreach($roles as $role){
           $role->accessRights()->sync($arIds, true);
       }
    }

}