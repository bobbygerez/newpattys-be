<?php 

namespace App\Repo\CompanyItemPurchaseOrder;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\CompanyItemPurchaseOrder;
use App\Model\PurchaseOrder;
use App\Model\FreightPrice;
use App\Model\Area;
class CompanyItemPurchaseOrderRepository extends BaseRepository implements CompanyItemPurchaseOrderInterface{


    public function __construct(){

        $this->modelName = new CompanyItemPurchaseOrder();
    
    }

    public function edit($request){

        $cipo = $this->where('vendorable_id', $request->id)
            ->where('vendorable_type', $request->vendorType)
            ->where('purchase_order_id', $this->removeStringEncode($request->poId) )
            ->with(['companyItem', 'purchaseOrderNo'])
            ->get();

            
        $gTotal = $cipo->map(function($v){
            return $v->price * $v->qty;
        })->sum();

        $vendor = $request->vendorType::where('id', $this->removeStringEncode( $request->id) )
                ->with(['address.region', 'address.province', 'address.city', 'address.brgy'])
                ->first();
        $po = PurchaseOrder::where('id', $this->removeStringEncode($request->poId) )->with(['purchaseRequest.approvedBy'])->first();



        $areaBranch = Area::whereHas('areaBranches', function($q) use ($request){
                        $q->where('branch_id', $request->requestId);
                    })->first();
        $freight = 0;           
        

        foreach($cipo as $companyItemPurchaseOrder){
            $dbFreight = FreightPrice::withoutGlobalScope('App\Scopes\FreightPriceScope')
            ->where('from_type', $companyItemPurchaseOrder->vendorable_type)
            ->where('from_id', $companyItemPurchaseOrder->vendorable_id)
             ->where('to_id', $areaBranch->id)
             ->where('to_type', $areaBranch->entity)
            ->first();
            if($dbFreight){
                $freight = $dbFreight->price_float;
            }
        }

        $frm = $request->requestType::where('id', $request->requestId)
            ->with(['address.region', 'address.province', 'address.city', 'address.brgy', 'company.address.region', 'company.address.province', 'company.address.city', 'company.address.brgy', 'company.businessInfo'])
            ->first();

            return response()->json([
                'freight' => $freight,
                'gTotal' => $gTotal,
                'vendor' => $vendor,
                'purchaseOrder' => $po,
                'companyItemPurchaseOrder' => $cipo,
                'branch' => $frm,
                'company' => $frm->company
            ]);
        
    }

}
