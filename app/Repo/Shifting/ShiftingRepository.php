<?php 

namespace App\Repo\Shifting;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Shifting;
use App\Model\ShiftingInventory;
use App\Model\ShiftingDenomination;
use Auth;

class ShiftingRepository extends BaseRepository implements ShiftingInterface{


    public function __construct(){

        $this->modelName = new Shifting();
    
    }

    public function index(){

        
       $shiftings = $this->modelName->with(['fromUser', 'toUser', 
                'shiftingDenomination' => function($q){
                    $q->where('qty', '!=', 0);
                }
                 ,'shiftingDenomination.denomination'])
            ->orderBy('created_at', 'desc')->get()->map(function($v){
            return [
                'optimus_id' => $v->optimus_id,
                'from' => $v->fromUser['name'],
                'to' => $v->toUser['name'],
                'cash_on_hand' => $v->cash_on_hand,
                'is_accepted' => $v->is_accepted,
                'created_at' => $v->created_at,
                'denominations' => $v->shiftingDenomination
            ];
        });
        return $this->paginate($shiftings);

    }

    public function store($request){
        $newRequest = $request->all();
        $newRequest['shiftable_id'] = $this->accessable_id();
        $newRequest['shiftable_type'] = $this->accessable_type();
        $newRequest['from'] = Auth::User()->id;
        $newRequest['to'] = $this->removeStringEncode($request->cashier);
        $shift = $this->modelName->create($newRequest);

            foreach($request->inventories as $i){
                ShiftingInventory::create([
                    'shifting_id' => $shift->id,
                    'company_item_id' => $i['id'],
                    'qty' => $i['qty'], 
                    'amount' => $i['price']
                ]);
            }

            foreach($request->denominations as $d){
                ShiftingDenomination::create([
                    'shifting_id' => $shift->id,
                    'denomination_id' => $d['id'],
                    'qty' => $d['qty']
                ]);
            }
       
    }

    public function accept( $request ){

        $shifting = $this->where('id', $request->id)->first();
        $shifting->update([
            'is_accepted' => true
        ]);
    }

    public function decline( $request ){

        $shifting = $this->where('id', $request->id)->first();
        $shifting->update([
            'is_accepted' => false
        ]);
    }

    public function coa( $request ){

         return ShiftingDenomination::where('shifting_id', $this->removeStringEncode( $request->id) )
            ->with(['denomination'])
            ->get();
    }

}
