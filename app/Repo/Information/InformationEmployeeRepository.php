<?php 

namespace App\Repo\Information;

use App\Repo\BaseRepository;
use App\Model\Information;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class InformationEmployeeRepository extends InformationRepository implements InformationInterface{


    public function __construct(){

        $this->modelName = new Information();
    
    }

  

    public function index($request){
        
        if($request->accessable_type === 'App\Model\Company'){
            $informations = $this->modelName->where('informationable_id', $this->removeStringEncode($request->accessable_id))
            ->where('informationable_type', $request->accessable_type )
            ->with(['informationable.branches.informations'])
            ->get();

            $companyInformations = $informations->map(function($val){
                return $val->only(['optimus_id', 'name']);
            })->values()->all();

            $branchesInformations =  $informations->map(function($val){
                return $val->informationable;
            })
            ->first()
            ->branches
            ->flatMap(function($val){
                return $val->informations;
            })
            ->map(function($val){
                return $val->only(['optimus_id', 'name']);
            })
            ->values()->all();

           $merge = array_merge($companyInformations, $branchesInformations);

           return $filter = $this->filterName($merge);

        }

            return $this->modelName->where('informationable_id', $this->removeStringEncode($request->accessable_id))
            ->where('informationable_type', $request->accessable_type )
            ->get()
            ->map(function($val){
                return $val->only(['optimus_id', 'name']);
            })
            ->values()->all();


    }

    public function store($request){
        $newRequest = $request->all();
        $newRequest['informationable_id'] = $this->optimus()->encode($request->informationable_id);
        $information = $this->create($newRequest);

        $result = $this->modelName->find($information->id);
        $result->address()->create($request->address);

    }

    public function update($request){
        $newRequest = $request->all();
        $newRequest['informationable_id'] = $this->optimus()->encode($request->informationable_id);
        $information = $this->find($request->optimus_id);
        $information->address()->update($request->address);
        $information->update($newRequest);
    }
}
