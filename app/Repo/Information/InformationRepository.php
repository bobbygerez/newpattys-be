<?php 

namespace App\Repo\Information;

use App\Repo\BaseRepository;
use App\Model\Information;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class InformationRepository extends BaseRepository implements InformationInterface{


    public function __construct(){

        $this->modelName = new Information();
    
    }

    public function index($request){

        if($request->accessable_type === 'App\Model\Company'){
            $information = $this->modelName->where('informationable_id', $this->removeStringEncode($request->accessable_id))
            ->where('informationable_type', $request->accessable_type )
            ->with(['informationable.branches.informations'])
            ->get();

            $companyInformations = $information->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })->values()->all();

            $branchesInformations =  $information->map(function($val){
                return $val->informationable;
            })
            ->first()
            ->branches
            ->flatMap(function($val){
                return $val->informations;
            })
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })
            ->values()->all();

           $merge = array_merge($companyInformations, $branchesInformations);

            $filter = $this->filterName($merge);

            return $this->paginate( collect($filter) );
        }

            $information = $this->modelName->where('informationable_id', $this->removeStringEncode($request->accessable_id))
            ->where('informationable_type', $request->accessable_type )
            ->get();

            $informations = $information->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })
            ->values()->all();

            $filter = $this->filterName($informations);

            return $this->paginate( collect($filter) );


    }

    public function store($request){
        $newRequest = $request->all();
        $newRequest['informationable_id'] = $this->removeStringEncode($request->informationable_id);
        $information = $this->create($newRequest);

        $result = $this->modelName->find($information->id);
        $result->address()->create($request->address);

    }

    public function update($request){
        $newRequest = $request->all();
        $newRequest['informationable_id'] = $this->removeStringEncode($request->informationable_id);
        $information = $this->find($request->optimus_id);
        $information->address()->update($request->address);
        $information->update($newRequest);
    }

    
}
