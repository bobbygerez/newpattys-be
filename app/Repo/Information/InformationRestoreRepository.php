<?php 

namespace App\Repo\Information;

use App\Repo\BaseRepository;
use App\Model\Information;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;
class InformationRestoreRepository extends BaseRepository implements InformationInterface{


    public function __construct(){

        $this->modelName = new Information();
    
    }

    public function index($request){

        if($request->accessable_type === 'App\Model\Company'){
            $information = $this->modelName->where('informationable_id', $this->removeStringEncode($request->accessable_id))
            ->where('informationable_type', $request->accessable_type )
            ->with(['informationable.branches.informations' => function($q){
                $q->withTrashed();
                $q->where('deleted_at', '!=', null);
            }])
            ->withTrashed()
            ->get();

            $companyInformations = $information->filter(function($val){
                return $val['deleted_at'] != null;
            })->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })->values()->all();

            $branchesInformations =  $information->map(function($val){
                return $val->informationable;
            })
            ->first()
            ->branches
            ->flatMap(function($val){
                return $val->informations;
            })
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })
            ->values()->all();

           $merge = array_merge($companyInformations, $branchesInformations);

            $filter = $this->filterName($merge);

            return $this->paginate( collect($filter) );
        }

            $information = $this->modelName->where('informationable_id', $this->removeStringEncode($request->accessable_id))
            ->where('informationable_type', $request->accessable_type )
            ->where('deleted_at', '!=', null)
            ->get();

            $informations = $information->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })
            ->values()->all();

            $filter = $this->filterName($informations);

            return $this->paginate( collect($filter) );


    }

    public function restore($request){

        $this->modelName->withTrashed()->find($this->removeStringEncode($request->id))->restore();
    }

    public function destroy($request){
        $info = $this->withTrashed($request)->first();
        $info->forceDelete();
    }

    
}
