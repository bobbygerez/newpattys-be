<?php 

namespace App\Repo\ChartAccount;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\ChartAccount;
use App\Traits\Obfuscate\Optimuss;
class ChartAccountRepository extends BaseRepository implements ChartAccountInterface{


    use Optimuss;

    public function __construct(){

        $this->modelName = new ChartAccount();
    
    }

    public function modelId($request){
        return $this->optimus()->encode($request->id);
    }
    public function index($request){

      $chartAccounts = $this->modelName->when(request('filter') !== null, function ($q) use ($request) {
                        return $q->where('name', 'LIKE', '%' . $request->filter . '%');
                    })
                    ->when(request('filter') === null, function ($q) {
                        return $q->where('parent_code', '=', 0);
                    })
                    ->with(['allChildren', 'tAccount'])
                    ->orderBy('created_at', 'desc')
                    ->get();
        return $this->paginate($chartAccounts);
    }

    public function create($request){
       
        $newRequest = $request->all();
        $parent = $this->modelName->find($request->parent_id);
        if($parent != null){
            $newRequest['parent_code'] = $parent->account_code;
        }
       $chartAccount = $this->modelName->create($newRequest);

       return $parent;
       
    }

    public function update($request){

        $newRequest = $request->all();
        $newRequest['company_id'] = $this->accessable_id();
        $this->whereNoObfuscate('id', request('id'))->first()
            ->update($newRequest);
    }
}