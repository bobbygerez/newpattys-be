<?php 

namespace App\Repo\BranchItem;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\BranchItem;

class BranchItemRepository extends BaseRepository implements BranchItemInterface{


    public function __construct(){

        $this->modelName = new BranchItem();
    
    }

    public function index($request){

        $branchItems = $this->modelName->onlyBranch()->when($request->filter!=null, function($q) use ($request){
            $q->where('name', 'like', '%'.$request->filter.'%');
          })->with(['branch', 'companyItem.category', 'companyItem.package', 'qty'])
          ->get();
    
           return $this->paginate( $branchItems );

    }

    public function store($request){

        $newRequest = $request->all();
        $newRequest['company_item_id'] = $this->removeStringEncode( $request->item_id );
        $bi = $this->create($newRequest);
        $b =  $this->modelName->find($bi->id);
        $bi->qty()->create([
            'branch_item_id' => $bi->id,
            'quantity' => $request->qty
        ]);
    }

}
