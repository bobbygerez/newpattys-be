<?php 

namespace App\Repo\BranchItem;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\BranchItem;
use App\Model\Category;
use App\Scopes\BranchScope;
class ProductRepository extends BaseRepository implements BranchItemInterface{


    public function __construct(){

        $this->modelName = new BranchItem();
    
    }

    public function index($request){

           
        $category = Category::when($request->selectedCategory != "null", function($q) use ($request) {
                            $q->where('id', $request->selectedCategory);
                        })
                        ->when($request->selectedCategory == "null", function($q){
                            $q->where('name', 'For Sale');
                        })
                        
                        ->with('children')
                        ->first();

            $childIds = $this->recursiveCategory($category->children);
            $childIds[] = $category->id;

            $childProducts =  $this->modelName->whereHas('companyItem.category', function($qq) use ($childIds) {
                $qq->whereIn('id', $childIds);
            })
            ->when($request->selectedRack != "null", function($q) use ($request) {
                            $q->where('rack_id', $request->selectedRack);
                        })
            ->with('companyItem.images')->get();


            return $this->paginate( collect($childProducts->toArray()) );

        

    }

     public function recursiveCategory($array) {
        
        $result = [];
        foreach ($array as $item) {
            if($item['id'] != null){
                $result[] = $item['id'];
            }
            $result = array_merge($result, $this->recursiveCategory($item['children']));
        }
        return array_filter($result);
    }

}
