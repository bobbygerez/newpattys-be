<?php 

namespace App\Repo\BranchItem;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\BranchItem;

class RestoreRepository extends BaseRepository implements BranchItemInterface{

   

    public function __construct(){

        $this->modelName = new BranchItem();
    
    }

    
    public function index($request){

        $branchItems = $this->modelName
        ->where('deleted_at', '!=', null)
         ->when($request->filter!=null, function($q) use ($request){
            $q->where('name', 'like', '%'.$request->filter.'%');
          })->with(['companyItem.category', 'companyItem.package', 'qty'])
          ->withTrashed()
          ->get();
          
           return $this->paginate( $branchItems );

    }
}
