<?php 

namespace App\Repo\BranchItem;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\BranchItem;

class BranchItemRestoreRepository extends BaseRepository implements BranchItemInterface{


    public function __construct(){

        $this->modelName = new BranchItem();
    
    }


}
