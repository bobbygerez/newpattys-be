<?php 

namespace App\Repo\TransactionSetup;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\TransactionSetup;

class TransactionSetupRepository extends BaseRepository implements TransactionSetupInterface{


    public function __construct(){

        $this->modelName = new TransactionSetup();
    
    }

    public function index($request){

          $transSetup = $this->modelName->when($request->filter!=null, function($q) use ($request){
            $q->whereHas('modelListing', function($qq) use ($request) {
                $qq->where('name', 'like', '%'.$request->filter.'%');
            });
          })->with(['modelListing', 'drBranchChartAccount', 'crBranchChartAccount'])->get();
    
           return $this->paginate( $transSetup );
        
    }

    public function store($request){

        $newRequest = $request->all();
        if($request->dr_chart_of_account != null){
             $newRequest['dr_chart_account_id'] = $this->removeStringEncode($request->dr_chart_of_account);
        }
        if($request->cr_chart_of_account != null){
             $newRequest['cr_chart_account_id'] = $this->removeStringEncode($request->cr_chart_of_account);
        }
        $newRequest['transactable_id'] = $this->accessable_id();
        $newRequest['transactable_type'] = $this->accessable_type();
        
        if($request->defaultt){
            $models = $this->modelName->where('model', $request->model)->get();
            foreach($models as $model){
                $model->update([
                    'defaultt' => false
                ]);
            }
        }
        $this->modelName->create( $newRequest );
    }

    public function update($request){

        $transactionSetup = $this->find($request->id);

        $newRequest = $request->all();
        if($request->dr_chart_of_account != null){
             $newRequest['dr_chart_account_id'] = $this->removeStringEncode($request->dr_chart_of_account);
        }
        if($request->cr_chart_of_account != null){
             $newRequest['cr_chart_account_id'] = $this->removeStringEncode($request->cr_chart_of_account);
        }

        if($request->defaultt){
            $models = $this->modelName->where('model', $request->model)->get();
            foreach($models as $model){
                $model->update([
                    'defaultt' => false
                ]);
            }
        }

        $transactionSetup->update( $newRequest );

    }

}
