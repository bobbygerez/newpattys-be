<?php 

namespace App\Repo\RackNo;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\RackNo;
class RackNoRepository extends BaseRepository implements RackNoInterface{


    public function __construct(){

        $this->modelName = new RackNo();
    
    }

    public function index($request){
        return $this->paginate(
            $this->modelName->when($request->filter != '', function($q) use ($request){
                $q->where('no', 'like', '%'. $request->filter . '%');
            })
            ->with(['rack'])
            ->orderBy('created_at', 'desc')
            ->get()
        );
    }

    
    
}
