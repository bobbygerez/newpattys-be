<?php 

namespace App\Repo\Category;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Category;

class CategoryRestoreRepository extends BaseRepository implements CategoryInterface{


    public function __construct(){

        $this->modelName = new Category();
    
    }

    public function index($request){
        $c = $this->modelName
                ->withTrashed()
                ->get()
                ->where('deleted_at', '!=', null)
                ->values()
                ->all();
        return collect($this->filterName($c))->values()->all();
    }

}