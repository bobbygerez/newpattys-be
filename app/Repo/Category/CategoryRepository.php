<?php 

namespace App\Repo\Category;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Category;

class CategoryRepository extends BaseRepository implements CategoryInterface{


    public function __construct(){

        $this->modelName = new Category();
    
    }

    public function index($request){
        return $this->modelName->when($request->filter == '', function($q){
            $q->where('parent_id', 0);
            
        })
        ->when($request->filter != '', function($q) use ($request){
            $q->where('name', 'like', '%'. $request->filter . '%');
        })
        ->with('children')
        ->get();
    }

    public function store($request){
        $this->create($this->data());
    }

    public function update($request){
        $c = $this->find($request->id);
        $c->update($this->data());
        if($request->chart_account_id !== null){
            foreach($c->children as $child){
                $child->update([
                    'chart_account_id' => $this->removeStringEncode( $request->chart_account_id['value'])
                ]);
            }
        }
        
    }


    public function data(){

        $request = app()->make('request');
        $newRequest = $request->all();
        if($request->chart_account_id !== null){
            $newRequest['chart_account_id'] = $this->removeStringEncode( $request->chart_account_id['value']);
        }
        
        if($request->parent_id != null){
            $newRequest['parent_id'] = $this->removeStringEncode($request->parent_id['value']);
        }else{
            $newRequest['parent_id'] = 0;
        }

        return $newRequest;
    }

}