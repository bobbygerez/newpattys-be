<?php 

namespace App\Repo\PaymentMethod;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\PaymentMethod;

class PaymentMethodRepository extends BaseRepository implements PaymentMethodInterface{


    public function __construct(){

        $this->modelName = new PaymentMethod();
    
    }

}