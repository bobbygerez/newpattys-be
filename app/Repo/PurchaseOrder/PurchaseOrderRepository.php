<?php 

namespace App\Repo\PurchaseOrder;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\PurchaseOrder;

class PurchaseOrderRepository extends BaseRepository implements PurchaseOrderInterface{


    public function __construct(){

        $this->modelName = new PurchaseOrder();
    
    }
    
    public function index($request){

        $po = $this->modelName->when($request->filter!=null, function($q) use ($request){
          $q->whereHas('purchaseRequest', function($qq) use ($request) {
             return $qq->where('purchase_request_no', 'like', '%'.$request->filter.'%');
          });
        })->with(['purchaseRequest.preparedBy', 'purchaseRequest.checkedBy', 'purchaseRequest.approvedBy'])
        ->orderBy('created_at', 'desc')
        ->get();
  
         return $this->paginate( $po );
      }
    
      public function edit($request){

        $po = $this->where('id', $request->id)
                ->with(['purchaseRequest.purchaseRequestCompanyItems.companyItem.package', 'companyItemPurchaseOrder.companyItem.companyItemVendorRanks.vendorable', 'companyItemPurchaseOrder.companyItem.package', 'companyItemPurchaseOrder.vendorable',
                'companyItemPurchaseOrder.purchaseOrderNo'])
                ->first();
                
        return response()->json([
          'purchaseOrder' => $po,
          'companyItemPurchaseOrder' => array_values( collect($po->companyItemPurchaseOrder)->groupBy('purchase_order_no_id')->values()->all() )
        ]); 

      }
      
}
