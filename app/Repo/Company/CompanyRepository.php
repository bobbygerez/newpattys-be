<?php

namespace App\Repo\Company;

use App\Model\Company;
use App\Model\User;
use App\Model\Accessable;
use App\Model\AccessRight;
use App\Model\Menu;
use App\Repo\BaseRepository;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class CompanyRepository extends BaseRepository implements CompanyInterface
{

    use Optimuss;
    public function __construct()
    {

        $this->modelName = new Company();
    }

    public function index($request)
    {
        $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable'])->first();

        $accessables = collect($user->roles)->flatMap(function ($val) {
            return $val->accessables;
        })->unique('id')->values()->all();

        $companies = collect($accessables)->filter(function ($val) {
            return $val->accessable_type === 'App\Model\Company';
        })->unique('id')->values()->all();

        $filtered = collect($companies)->when($request->filter != '', function ($companies) use ($request) {
            return $companies->filter(function ($val) use ($request) {
                return false !== stristr($val->accessable->name, $request->filter);
            })->values()->all();

        });

        $companies = collect($filtered)->map(function ($val) {
            return $val->accessable;
        })->filter(function($val){
            return $val !== null;
        })->unique('id')->sortByDesc('id')->values()->all();

        return $this->paginate( collect($companies) );
    }

    public function store($request){
        $newRequest = $request->all();
        $newRequest['holding_id'] = $request->holding_id != null ? $this->optimus()->encode($request->holding_id) : null;
        $company = $this->modelName->create($newRequest);
        $newCompany = $this->modelName->find($company->id);
        $businessInfo = $request->businessInfo;
        $businessInfo['posting_method_id'] = $this->removeStringEncode($request->businessInfo['posting_method_id']);
        $newCompany->businessInfo()->create($businessInfo);
        $newCompany->address()->create($request->address);
        
        $accessRights = AccessRight::all();

        $menuId = Menu::where('name', 'companies')->first();

        foreach($accessRights as $ar){
            $accessable =  Accessable::create([
                    'access_right_id' => $ar->id,
                    'accessable_id' => $company->id,
                    'accessable_type' => 'App\Model\Company',
                    'menu_id' => $menuId->id
                ]);

            foreach(Auth::User()->roles as $role){
                $role->accessables()->attach($role->id,[
                    'accessable_id' => $accessable->id,
                    'role_id' => $role->id
                ]);
            }
        }
        

    }

    public function edit($request){
        return $this->where('id', $request->id)
            ->with(['address.country', 'address.region', 'address.province', 'address.city', 'address.brgy', 'holding', 'businessInfo.accountingMethod', 'businessInfo.businessType', 'businessInfo.vatType', 'businessInfo.postingMethod'])
            ->first();
    }

    public function update($request){

        $newRequest = $request->all();
        $newRequest['holding_id'] = $request->holding_id != null ? $this->removeStringEncode($request->holding_id) : null;
        $company = $this->modelName->find($this->removeStringEncode($request->id));
        $businessInfo = $request->businessInfo;
        $businessInfo['posting_method_id'] = $this->removeStringEncode($request->businessInfo['posting_method_id']);
        $company->businessInfo()->update($businessInfo);
        $company->address()->update($request->address);
        $company->update($newRequest);
    }
}
