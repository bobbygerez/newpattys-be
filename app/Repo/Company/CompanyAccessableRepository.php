<?php

namespace App\Repo\Company;

use App\Model\Company;
use App\Model\User;
use App\Repo\BaseRepository;
use Auth;

class CompanyAccessableRepository extends BaseRepository implements CompanyInterface
{

    public function __construct()
    {
        $this->modelName = new Company();
    }

    public function index($request){
        
            $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable'])->first();

            $accessables =  collect($user->roles)->flatMap(function($val){
                return $val->accessables;
            })->unique('id')->values()->all();

            $branches = collect($accessables)->filter(function($val) use ($request) {
                return $val->accessable_type == $request->accessable_type && $this->removeStringEncode($val->accessable_id == $request->accessable_id);
            })->map(function($val){
                return $val->accessable;
            })->filter(function($val){
                return $val != null;
            })->unique('id');

        if($request->accessable_type === 'App\Model\Company'){
            return $branches->values()->all();
        }

        $branchIds = $branches->pluck('id');

        return Company::whereIn('id', $branchIds)->get();

    }
}
