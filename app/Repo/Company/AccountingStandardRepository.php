<?php

namespace App\Repo\Company;

use App\Repo\BaseRepository;


class AccountingStandardRepository extends BaseRepository implements CompanyInterface
{

    public function __construct()
    {

        $this->modelName = new AccountingStandard();
    }

    public function index($request)
    {
        
        return $this->modelName->all();
    }

}
