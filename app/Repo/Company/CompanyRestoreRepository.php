<?php

namespace App\Repo\Company;

use App\Model\Company;
use App\Repo\BaseRepository;
use App\Model\User;
use Auth;

class CompanyRestoreRepository extends BaseRepository implements CompanyInterface
{

    public function __construct()
    {

        $this->modelName = new Company();
    }

    public function index($request)
    {
        $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable' => function($q){
            $q->withTrashed();
        }])->first();

        $accessables = collect($user->roles)->flatMap(function ($val) {
            return $val->accessables;
        })->unique('id')->values()->all();

        $companies = collect($accessables)->filter(function ($val) {
            return $val->accessable_type === 'App\Model\Company';
        })->unique('id')->values()->all();

        $filtered = collect($companies)->when($request->filter != '', function ($companies) use ($request) {
            return $companies->filter(function ($val) use ($request) {
                return false !== stristr($val->accessable->name, $request->filter);
            })->values()->all();

        });

        $companies = collect($filtered)->map(function ($val) {
            return $val->accessable;
        })->filter(function($val){
            return $val !== null && $val['deleted_at'] != null;
        })->unique('id')->sortByDesc('id')->values()->all();

        return $this->paginate( collect($companies) );
    }
}
