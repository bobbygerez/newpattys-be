<?php 

namespace App\Repo\Role;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Role;
use App\Traits\Obfuscate\Optimuss;
use App\Model\Accessable;
use Auth;

class RoleAccessableModelRepository extends BaseRepository implements RoleInterface{

    use Optimuss;
    public function __construct(){

        $this->modelName = new Role();
    
    }

    public function accessableModels($request){

        if (Auth::User()->isSuperAdmin()) {
            return Accessable::all()->unique('accessable_type')->values()->all();
        }

         return collect(
                $this->modelName
                    ->where('id', $this->optimus()->encode($request->roleId))
                    ->with('accessables')->first()->accessables
                )->unique('accessable_type')->values()->all();;
    }

}