<?php 

namespace App\Repo\Role;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Role;
use App\Model\Accessable;
class RoleAccessableRepository extends BaseRepository implements RoleInterface{


    public function __construct(){

        $this->modelName = new Role();
    
    }

    public function store($request){
        
        $accessables = Accessable::whereHas('roles', function($q) use ($request){
           $q->where('roles.id', $this->removeStringEncode($request->availableRole) );
        })
        ->where('accessable_id', $this->removeStringEncode($request->accessable_id))
        ->where('accessable_type', $request->accessable_type)
        ->where('menu_id', $request->menu_id)
        ->get();

        foreach($accessables as $accessable){
            $accessable->roles()->detach();
            $accessable->delete();
        }

        

        foreach($request->access_rights as $ar){
            $accessable = Accessable::create([
                'access_right_id' => $ar['value'],
                'menu_id' => $request->menu_id,
                'accessable_type' => $request->accessable_type,
                'accessable_id' => $this->removeStringEncode($request->accessable_id) 
            ]);

            Accessable::find($accessable->id)->roles()->attach($accessable->id, [
                'accessable_id' => $accessable->id,
                'role_id' => $this->removeStringEncode($request->availableRole)
            ]);
        }

    }

    public function menuAccessRights($request){

        $role = $this->modelName->where('id', $this->removeStringEncode($request->roleId) )->with(['accessables.menu', 'accessables.accessRight'])->first();

        $accessables = collect($role->accessables)->filter(function($val) use ($request){
            return $val->accessable_id == $this->removeStringEncode($request->modelId) && $val->accessable_type == $request->modelType;
        });

       return collect($accessables)->map(function($val){

            return [
                'access_right_name' => $val->accessRight['name'],
                'access_right_id' => $val->access_right_id,
                'menu_id' => $val->menu['id'],
                'menu_name' => $val->menu['name']
            ];
        })->values()->all();
       
    }
}