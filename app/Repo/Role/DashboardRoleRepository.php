<?php 

namespace App\Repo\Role;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Role;
use App\Model\User;
use Auth;
class DashboardRoleRepository extends BaseRepository implements RoleInterface{


    public function __construct(){

        $this->modelName = new Role();
    
    }

    public function index($request){

            $roles = $this->modelName->where('roleable_id', $this->removeStringEncode($request->accessable_id))
            ->where('roleable_type', $request->accessable_type )
            ->with(['allChildren'])
            ->get();
            $parentId = $roles->min('parent_id');
           $roles = $roles->filter(function($val) use ($parentId){
                return $val->id > $parentId;
            });
            
            $roles = $this->flatten($roles->toArray());

            $filter = collect( $this->filterName($roles) )->unique('id')->values()->all();

            return  $this->paginate (
                collect( collect($filter ) )
            );
        
    }

    public function create($request){
            if(Auth::User()->isSuperAdmin()){
                return $this->modelName->all()->unique('id')->values()->all();
            }

            $user = User::where('id', Auth::User()->id)->with(['roles.allChildren'])->first();

            $roles  = collect( $this->flatten($user->roles->toArray()) )->filter(function($val) use ($request){
                    return $val['roleable_type'] == $request->accessable_type && $val['roleable_id'] == $this->removeStringEncode($request->accessable_id);
                })->values()->all();

            return array_merge($user->roles->toArray(),$roles);
    }

    

    public function store($request){
        $this->modelName->create( $this->newRequest($request) );
    }

    public function update($request){
        if( $this->removeStringEncode($request->parent_id) >= $this->removeStringEncode($request->id)  ){
            return response()->json(['errors' => ['name' => 'Supervisor must have a greater role...']], 422);
        }else{
            $role = $this->modelName->find( $this->removeStringEncode($request->id) );
            $role->update( $this->newRequest($request)  );
        }
        
        return response()->json([
            'success' => true
        ]);
    }

    public function newRequest($request){
        $newRequest = $request->all();
        $newRequest['parent_id'] = $this->removeStringEncode($request->parent_id);
        $newRequest['roleable_id'] = $this->removeStringEncode($request->roleable_id);
        return $newRequest;
    }

}