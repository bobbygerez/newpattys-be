<?php 

namespace App\Repo\Role;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Role;
use App\Model\User;
use Auth;
class RoleRestoreRepository extends BaseRepository implements RoleInterface{


    public function __construct(){

        $this->modelName = new Role();
    
    }

    public function index($request){
        $roles = $this->modelName->where('roleable_type', $request->accessable_type)
        ->where('roleable_id', $this->removeStringEncode($request->accessable_id))
        ->where('deleted_at', '!=', null)
        ->withTrashed()
        ->get();
        $filter = $this->filterName($roles);
        return $this->paginate( 
            collect($filter)
        );
    }

   

    public function destroy($request){
        $role = $this->withTrashed($request)->first();
        $role->accessables()->detach();
        $role->forceDelete();
    }

   
}