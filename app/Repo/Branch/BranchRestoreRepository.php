<?php 

namespace App\Repo\Branch;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Branch;
use App\Model\User;
use App\Model\AccessRight;
use App\Model\Menu;
use App\Model\Accessable;
use Auth;

class BranchRestoreRepository extends BaseRepository implements BranchInterface{


    public function __construct(){

        $this->modelName = new Branch();
    
    }

    public function index($request)
    {

        
        $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable'=> function($q){
            $q->withTrashed();
        }])->first();

        $accessables = collect($user->roles)->flatMap(function ($val) {
            return $val->accessables;
        })->unique('id')->values()->all();

        $branches = collect($accessables)->filter(function ($val) {
            return $val->accessable_type === 'App\Model\Branch';
        })->unique('id')->values()->all();

        $filtered = collect($branches)->when($request->filter != '', function ($branches) use ($request) {
            return $branches->filter(function ($val) use ($request) {
                return false !== stristr($val->accessable->name, $request->filter);
            })->values()->all();

        });

        $branches = collect($filtered)->map(function ($val) {
            return $val->accessable;
        })->filter(function($val){
            return $val !== null && $val['deleted_at'] != null;
        })->unique('id')->sortByDesc('id')->values()->all();

        return $this->paginate( collect($branches) );
    }


}
