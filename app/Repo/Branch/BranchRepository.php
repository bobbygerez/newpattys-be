<?php 

namespace App\Repo\Branch;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Branch;
use App\Model\User;
use App\Model\AccessRight;
use App\Model\Menu;
use App\Model\Accessable;
use Auth;

class BranchRepository extends BaseRepository implements BranchInterface{


    public function __construct(){

        $this->modelName = new Branch();
    
    }


    public function index($request)
    {

        $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable'])->first();
        $accessables = collect($user->roles)->flatMap(function ($val) {
            return $val->accessables;
        })->unique('id')->values()->all();

        $branches = collect($accessables)->filter(function ($val) {
            return $val->accessable_type === 'App\Model\Branch';
        })->unique('id')->values()->all();

        $filtered = collect($branches)->when($request->filter != '', function ($branches) use ($request) {
            return $branches->filter(function ($val) use ($request) {
                return false !== stristr($val->accessable->name, $request->filter);
            })->values()->all();

        });

        $branches = collect($filtered)->map(function ($val) {
            return $val->accessable;
        })->filter(function($val){
            return $val !== null;
        })->unique('id')->sortByDesc('id')->values()->all();

        return $this->paginate( collect($branches) );
    }


    public function store($request){

        $newRequest = $request->all();
        $newRequest['branch_type_id'] = $this->removeStringEncode( $request->branch_type_id );
        $newRequest['company_id'] = $this->removeStringEncode( $request->company_id );
        $branch = $this->create($newRequest);
        $branch = $this->findNoObfuscate($branch->id);
        $branch->address()->create($request->address);

        $accessRights = AccessRight::all();

        $menuId = Menu::where('name', 'branches')->first();

        foreach($accessRights as $ar){
            $accessable =  Accessable::create([
                    'access_right_id' => $ar->id,
                    'accessable_id' => $branch->id,
                    'accessable_type' => 'App\Model\Branch',
                    'menu_id' => $menuId->id
                ]);

            foreach(Auth::User()->roles as $role){
                $role->accessables()->attach($role->id,[
                    'accessable_id' => $accessable->id,
                    'role_id' => $role->id
                ]);
            }
        }
    }

    public function update($request){
        $newRequest = $request->all();
        $newRequest['branch_type_id'] = $this->removeStringEncode( $request->branch_type_id );
        $newRequest['company_id'] = $this->removeStringEncode( $request->company_id );
        $branch = $this->modelName->find( $this->removeStringEncode( $request->company_id ) );
        $branch->address()->update($request->address);
        $branch->update($newRequest);
    }

    public function getBranches($companyId){

        return $this->where('id', $companyId);
    }

    public function getBranchMenus($branchId){
        
        $user = Auth::User()->with('roles.accessRights.branches')->first();
        return $user;
        // return $this->where('id',$branchId)->with('accessRights.roles.accessRights.menus')->first();
    }
}
