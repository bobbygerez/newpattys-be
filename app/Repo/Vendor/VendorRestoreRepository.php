<?php 

namespace App\Repo\Vendor;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Vendor;

class VendorRestoreRepository extends BaseRepository implements VendorInterface{


    public function __construct(){

        $this->modelName = new Vendor();
    
    }

    public function index($request){

        if($request->accessable_type === 'App\Model\Company'){
            $vendors = $this->modelName
            ->where('vendorable_id', $this->removeStringEncode($request->accessable_id))
            ->where('vendorable_type', $request->accessable_type )
            ->withTrashed()
            ->get()
            ->filter(function($val){
                return $val->deleted_at != null;
            })
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'token', 'created_at']);
            })
            ->values()->all();

            $filter = $this->filterName($vendors);

            return $this->paginate( collect($filter) );
        }
    }

}
