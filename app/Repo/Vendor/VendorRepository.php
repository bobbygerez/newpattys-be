<?php 

namespace App\Repo\Vendor;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Vendor;

class VendorRepository extends BaseRepository implements VendorInterface{


    public function __construct(){

        $this->modelName = new Vendor();
    
    }

    public function index($request){

        if($request->accessable_type === 'App\Model\Company'){
            $vendors = $this->modelName
            ->where('vendorable_id', $this->removeStringEncode($request->accessable_id))
            ->where('vendorable_type', $request->accessable_type )
            ->get()
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'token', 'created_at']);
            })
            ->values()->all();

            $filter = $this->filterName($vendors);

            return $this->paginate( collect($filter) );
        }
    }

    public function store($request){
        $newRequest = $request->all();
        $newRequest['vendorable_id'] =  $this->removeStringEncode($request->accessable_id);
        $newRequest['vendorable_type'] = $request->accessable_type;
        $newRequest['token'] = str_random(32);
        $vendor = $this->modelName->create($newRequest);
        $vendor = $this->modelName->find($vendor->id);
        $vendor->address()->create($request->address);
    }

    public function update($request){
        $newRequest = $request->all();
        $newRequest['vendorable_id'] =  $this->removeStringEncode($request->accessable_id);
        $newRequest['vendorable_type'] = $request->accessable_type;
        $vendor = $this->find($request->id);
        $vendor->address()->update($request->address);
        $vendor->update($newRequest);
    }

    public function destroy($request){

        $vendor = $this->find($request->id);
        $vendor->delete();
    }

}
