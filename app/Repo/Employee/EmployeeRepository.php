<?php 

namespace App\Repo\Employee;

use App\Repo\BaseRepository;
use App\Model\Employee;
use App\Model\Information;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class EmployeeRepository extends BaseRepository implements EmployeeInterface{


    public function __construct(){

        $this->modelName = new Employee();
    
    }

    public function index($request){
    
        if($request->accessable_type === 'App\Model\Company'){
            //Company Employee should created first before branch employee to be able to work this code.
            $employees = $this->modelName->where('employable_id', $this->removeStringEncode($request->accessable_id))
            ->where('employable_type', $request->accessable_type )
            ->with(['employable.branches.employees'])
            ->get();

            $companyEmployees = $employees->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })->values()->all();

            $branchesEmployees =  $employees->map(function($val){
                return $val->employable;
            })
            ->first()
            ->branches
            ->flatMap(function($val){
                return $val->employees;
            })
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })
            ->values()->all();

           $merge = array_merge($companyEmployees, $branchesEmployees);

            $filter = $this->filterName($merge);

            return $this->paginate( collect($filter) );
        }

        $employees = $this->modelName->where('employable_id', $this->removeStringEncode($request->accessable_id))
            ->where('employable_type', $request->accessable_type )
            ->get()
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'string_birth', 'created_at']);
            })->values()->all();
            $filter = $this->filterName($employees);
        return $this->paginate( collect($filter) );
   
    }

    public function store($request){

        $newRequest = $request->all();
        $newRequest['information_id'] = $this->removeStringEncode($request->information_id);
        $newRequest['employable_id'] = $this->removeStringEncode($request->employable_id);
        $this->create($newRequest);
    }

    public function update($request){

        $requestEmployee = $request->employee;
        $requestEmployee['employable_id'] = $this->removeStringEncode($request->employee['employable_id']);
        $requestEmployee['information_id'] = $this->removeStringEncode($request->employee['information_id']);
        $employee = $this->find($request->employee['optimus_id']);
        $employee->update($requestEmployee);


        $requestInfo = $request->info;
        $requestInfo['informationable_id'] = $this->removeStringEncode($request->info['informationable_id']);
        $info = Information::find($this->removeStringEncode($request->info['optimus_id']));
        $info->address()->update($request->info['address']);
        $info->update($requestInfo);
        
    }

    
}
