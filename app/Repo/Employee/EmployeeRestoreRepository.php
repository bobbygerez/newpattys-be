<?php 

namespace App\Repo\Employee;

use App\Repo\BaseRepository;
use App\Model\Employee;
use App\Traits\Obfuscate\Optimuss;
use App\Model\User;
use Auth;
class EmployeeRestoreRepository extends BaseRepository implements EmployeeInterface{


    public function __construct(){

        $this->modelName = new Employee();
    
    }
    public function index($request){
            $user = User::where('id', Auth::User()->id)
            ->with(['roles.allChildren.accessables.accessable.employees'=> function ($query) {
                $query->withTrashed();
            }, 'roles.accessables.accessable.employees'=> function ($query) {
                $query->withTrashed();
            }])->first();

            $minId = collect($user->roles)->min('id');
            $roles = collect( $user->roles )->filter(function($val) use ($minId){
                return $val->id == $minId;
            });
            $employees = collect($this->mapRecursive($roles))->flatten(1)
            ->map(function($val){
                return $val->accessable['employees'];
            })
            ->flatten(1)
            ->unique('id')
            ->filter(function($val) use ($request) {
                return $val['employable_type'] == $request->accessable_type && $val['employable_id'] == $this->removeStringEncode($request->accessable_id) && $val['deleted_at'] !== null;
            });

            return $this->paginate( 
                collect($employees)
        );
    }
    public function restore($request){

        $this->modelName->withTrashed()->find($this->removeStringEncode($request->id))->restore();
    }
}
