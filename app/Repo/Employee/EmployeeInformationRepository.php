<?php 

namespace App\Repo\Employee;

use App\Repo\BaseRepository;
use App\Model\Employee;
use App\Model\Information;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class EmployeeInformationRepository extends BaseRepository implements EmployeeInterface{


    public function __construct(){

        $this->modelName = new Employee();
    
    }

    public function store($request){
        $requestInfo = $request->info;
        $requestInfo['informationable_id'] = $this->removeStringEncode($request->info['informationable_id']);
        $info = Information::create($requestInfo);
        $newInfo = Information::find($info->id);
        $newInfo->address()->create($request->info['address']);

        $newRequest = $request->employee;
        $newRequest['information_id'] = $info->id;
        $newRequest['employable_id'] = $this->removeStringEncode($request->employee['employable_id']);
        $this->create($newRequest);
    }

}
