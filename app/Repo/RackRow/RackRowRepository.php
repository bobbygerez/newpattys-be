<?php 

namespace App\Repo\RackRow;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\RackRow;
class RackRowRepository extends BaseRepository implements RackRowInterface{


    public function __construct(){

        $this->modelName = new RackRow();
    
    }

    public function index($request){
        return $this->paginate(
            $this->modelName->when($request->filter != '', function($q) use ($request){
                $q->where('no', 'like', '%'. $request->filter . '%');
            })
            ->with(['rack'])
            ->orderBy('created_at', 'desc')
            ->get()
        );
    }

    
    
}
