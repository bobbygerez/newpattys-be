<?php 

namespace App\Repo\Menu;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Menu;
use Auth;

class AccessableMenuRepository extends MenuRepository implements MenuInterface{


    public function __construct(){

        $this->modelName = new Menu();
    
    }

    public function accessableMenus(){

        $user = Auth::User()->with('roles.accessables')->first();
        return $this->getMenus($user);
    }

    
}