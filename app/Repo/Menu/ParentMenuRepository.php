<?php 

namespace App\Repo\Menu;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Menu;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;
class ParentMenuRepository extends MenuRepository implements MenuInterface{

    use Optimuss;
    public function __construct(){

        $this->modelName = new Menu();
    
    }

    public function parentMenus(){

        $user = User::where('id', Auth::User()->id)->with('roles.accessables.menus')->first();
        return $this->getAccessRights($user);
    }

    public function userParentMenus($request){
        // $user = User::where('id', $this->optimus()->encode($request->id))->with('roles.accessables.accessRights')->first();
        // $accessables = collect($user->roles)->flatMap(function($val){
        //     return $val->accessables;
        // });

        // $filtered =  collect($accessables)->filter(function($val){
        //     return $val->accessable_type === 'App\Model\Menu';
        // });

        // return $unique = collect($filtered)->unique('id')->values()->all();
    }

    public function getAccessRights($user){

        return $accessables = collect($user->roles)->flatMap(function($val){
            return $val->accessables;
        });

    //     $filtered =  collect($accessables)->filter(function($val){
    //         return $val->accessable_type == Auth::User()->assignment->assignmentable_type && $val->accessable_id == Auth::User()->assignment->assignmentable_id && $val->access_right_id == 1;
    //     });

    //     $menus = collect($filtered)->flatMap(function($val){
    //         return $val->menus;
    //     });

    //    return collect($menus)->unique('id')->values()->all();
    }

}