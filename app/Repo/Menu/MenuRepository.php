<?php 

namespace App\Repo\Menu;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Menu;
use Auth;

class MenuRepository extends BaseRepository implements MenuInterface{


    public function __construct(){

        $this->modelName = new Menu();
    
    }

    public function getMenus($user){

        if (Auth::User()->isSuperAdmin()) {
            return $this->modelName->all();
         }
        $accessables = collect($user->roles)->flatMap(function($val){
            return $val->accessables;
        });

        $filtered =  collect($accessables)->filter(function($val){
            return $val->accessable_type == Auth::User()->assignment->assignmentable_type && $val->accessable_id == Auth::User()->assignment->assignmentable_id && $val->access_right_id == 1;
        });

        $menus = collect($filtered)->flatMap(function($val){
            return $val->menus;
        });

       return collect($menus)->unique('id')->values()->all();
    }

}