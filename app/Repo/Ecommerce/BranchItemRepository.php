<?php

namespace App\Repo\Ecommerce;

use App\Model\BranchItem;
use App\Repo\BaseRepository;

class BranchItemRepository extends BaseRepository implements EcommerceInterface
{

    public function __construct()
    {

        $this->modelName = new BranchItem();

    }

    public function branchItems($request)
    {

        $branchItems = $this->where('branch_id', $request->id)
            ->when($request->filter != null, function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->filter . '%');
            })->with(['branch', 'companyItem.category', 'companyItem.package', 'qty'])
            ->get()
            ->map(function ($v) {
                return [
                    'image' => $v->companyItem['default_image']['path_url'],
                    'label' => $v->companyItem['name'],
                    'value' => $v->value,
                    'price' => $v->companyItem['amount'],
                    'amount' => 0,
                ];
            });

        return $this->paginate($branchItems);

    }

}
