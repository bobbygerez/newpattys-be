<?php 

namespace App\Repo\Pos;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\TransactionNo;
use App\Model\CreditCard;
use App\Model\CustomerReward;

class TransactionNoRepository extends BaseRepository implements PosInterface{

  public function __construct(){

        $this->modelName = new TransactionNo();
    
  }

  public function index( $request ){
      $t = $this->modelName
        ->where('no', 'like', '%'.$request->filter.'%')
        ->with(['branchItemTrans.companyItem', 'saveBy', 'transactBy'])
        ->orderBy('no', 'desc')
        ->get();
      
      $t = $t->map(function($v) use ($t) {
        $gt =  $v->branchItemTrans->map(function($x){
          return $x->qty_out * $x->amount;
        })->sum();
        return [
          'no' => $v->no,
          'save_by' => $v->saveBy['name'],
          'is_completed' => $v->is_completed,
          'is_void' => $v->is_void,
          'time' => $v->ago,
          'optimus_id' => $v->optimus_id,
          'grand_total' => $gt
        ];
      });
      
      return $this->paginate($t);
  }

  public function edit( $request ){

      $t = $this->where('id', $request->id)
        ->with([
            'branchItemTrans.companyItem.images', 
            'transactable.address.province',
            'transactable.address.city',
            'transactable.address.brgy',
          ])
        ->first();

      return $t->branchItemTrans->map(function($v) use ($t){
        return [
          'name' => $v->companyItem['name'],
          'qty_out' => $v->qty_out,
          'amount' => $v->amount,
          'img' => $v->companyItem['default_image']['path_url'],
          'subtotal' => $v->qty_out * $v->amount
        ];
      });
  }

  public function getBranchInfo( $request ){

    $t = $this->where('id', $request->id)
        ->with([
            'transactable.address.province',
            'transactable.address.city',
            'transactable.address.brgy',
          ])
        ->first();
      
      return [
        'name' => $t->transactable->name,
        'contact' => $t->transactable->tel,
        'address1' => $t->transactable->address['address1'],
        'address2' => $t->transactable->address['address2'],
        'created_at' => $t->created_at
      ];
  }

  public function creditCard($request){
     
    $allCreditCard = CreditCard::all();
    // return $allCreditCard->map(function($v){
    //   return decrypt($v->card_no);
    // })->values()->all();
    $result = $allCreditCard->filter(function($v) use ($request) {
      return decrypt($v->card_no) === $request->card_no;
    })->first();
    
    if($result){
      return $result;
    }else{
      return CreditCard::create( $request->all() );
    }
  }

  public function customerReward($request){

     return CustomerReward::where('no', $request->card_no)->first();

     
  }
  
}
