<?php 

namespace App\Repo\Pos;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\BranchItemTrans;
use App\Model\TransactionNo;
use App\Events\MyEvent;
use App\Model\Branch;
use Carbon\Carbon;
use Auth;

class BranchItemTransRepository extends BaseRepository implements PosInterface{

  public function __construct(){

        $this->modelName = new BranchItemTrans();
    
  }
  public function store( $request ){

    $aI = $this->accessable_id();
    $aT = $this->accessable_type();
    $branch = Branch::find($this->accessable_id());
    
   $poOnThisDate = TransactionNo::where('created_at', '>=', Carbon::today())
            ->get();
        $no = 1;
        if(count($poOnThisDate) > 0){
            $no = count($poOnThisDate) + 1;
        }else{
            $no = 1;
        }
    $trNo = $branch->acronym.'-'. date('Ymd') . '-' . str_pad($no, 4, '0', STR_PAD_LEFT);

    $tr = TransactionNo::create([
      'no' => $trNo,
      'save_by' => Auth::User()->id,
      'transactable_id' => $aI,
      'transactable_type' => $aT, 
    ]);
    
    foreach($request->cart as $cart){
      $this->modelName->create([
        'transaction_no_id' => $tr->id,
        'transactable_id' => $aI,
        'transactable_type' => $aT, 
        'company_item_id' => $cart['id'], 
        'qty_out' => $cart['qty'], 
        'amount' => $cart['price']
      ]);
    }
    event(new MyEvent('hello world'));
    return TransactionNo::where('id', $tr->id)->first();

  }
}
