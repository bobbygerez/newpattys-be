<?php

namespace App\Repo\Holding;

use App\Model\Holding;
use App\Model\User;
use App\Repo\BaseRepository;
use Auth;

class HoldingAccessableRepository extends HoldingRepository implements HoldingInterface
{

    public function __construct()
    {
        $this->modelName = new Holding();
    }

    
    public function index($request){

        // if (Auth::User()->isSuperAdmin()) {
        //     return $this->modelName->all();
        // }

        $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable'])->first();

        $accessables =  collect($user->roles)->flatMap(function($val){
              return $val->accessables;
         })->unique('id')->values()->all();

         return collect($accessables)->filter(function($val) use ($request) {
            return $val->accessable_type == 'App\Model\Holding';
        })->values()->all();
    }
}
