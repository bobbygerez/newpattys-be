<?php

namespace App\Repo\Holding;

use App\Model\Holding;
use App\Repo\BaseRepository;

class HoldingRepository extends BaseRepository implements HoldingInterface
{

    public function __construct()
    {
        $this->modelName = new Holding();
    }

    public function index($request){

        // if (Auth::User()->isSuperAdmin()) {
        //     return $this->modelName->all();
        // }

        $user = User::where('id', Auth::User()->id)->with(['roles.accessables.accessable'])->first();

        $accessables =  collect($user->roles)->flatMap(function($val){
              return $val->accessables;
         })->unique('id')->values()->all();

         return collect($accessables)->filter(function($val) use ($request) {
            return $val->accessable_type == 'App\Model\Holding';
        })->values()->all();
    }

    public function update($request){

        $holding = $this->find($request->optimus_id);
        $newRequest = [];
        $newRequest['province_id'] = $request->address['province_id'];
        $newRequest['city_id'] = $request->address['city_id'];
        $newRequest['brgy_id'] = $request->address['brgy_id'];
        $holding->address()->update($newRequest);
        $holding->update($request->all());


    }
       
}
