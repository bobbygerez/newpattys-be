<?php 

namespace App\Repo\PurchaseRequest;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\CompanyItem;
use App\Model\Branch;
class CompanyItemRepository extends BaseRepository implements PurchaseRequestInterface{


    public function __construct(){

        $this->modelName = new CompanyItem();
    
    }

    
}
