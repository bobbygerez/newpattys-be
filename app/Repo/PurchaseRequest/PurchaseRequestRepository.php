<?php 

namespace App\Repo\PurchaseRequest;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\PurchaseRequest;
use Auth;
use App\Model\Branch;
use Carbon\Carbon;
class PurchaseRequestRepository extends BaseRepository implements PurchaseRequestInterface{


    public function __construct(){

        $this->modelName = new PurchaseRequest();
    
    }

    public function index($request){

      $purchaseRequests = $this->modelName->when($request->filter!=null, function($q) use ($request){
        $q->where('purchase_request_no', 'like', '%'.$request->filter.'%');
      })->with(['preparedBy', 'checkedBy', 'approvedBy'])->orderBy('updated_at', 'desc')->get();

       return $this->paginate( $purchaseRequests );
    }

    public function store($request){

        $dateString = date('Ymd'); 
        $newRequest = $request->all();

        $branch = Branch::find($this->accessable_id());

        $poOnThisDate = $this->modelName->where('created_at', '>', Carbon::now()->subDays(1) )
            ->get();
        
        $no = 1;
        if(count($poOnThisDate) > 0){
            $no = count($poOnThisDate) + 1;
        }else{
            $no = 1;
        }
        $prCode = $branch->acronym.'-'. date('Ymd') . '-' . str_pad($no, 4, '0', STR_PAD_LEFT);


        $newRequest['purchasable_id'] = $this->accessable_id();
        $newRequest['purchasable_type'] = $this->accessable_type();
        $newRequest['purchase_request_no'] = $prCode;
        $newRequest['purchase_request_code'] = str_random(32);
        $newRequest['prepared_by'] = Auth::User()->id;
        $pr = $this->create( $newRequest );
        $newPr =  $this->modelName->find($pr->id);

        foreach($request->items as $item){

            $newPr->purchaseRequestCompanyItems()->create([
                'company_item_id' => $this->removeStringEncode($item['item']['value']),
                'purchase_request_id' => $pr->id,
                'qty_left' => $item['totalQty'],
                'qty_order' => $item['orderQty'],
                'qty_max' => $item['generatedQty'],
                'comment' => $item['comment']
            ]);
        }

    }

    public function update( $request){

        $pr = $this->find($request->id);
        foreach($pr->purchaseRequestCompanyItems as $ci) {
            $ci->delete();
        };

        foreach($request->items as $item){
            $pr->purchaseRequestCompanyItems()->create([
                'company_item_id' => $this->removeStringEncode($item['item']['value']),
                'purchase_request_id' => $pr->id,
                'qty_left' => $item['totalQty'],
                'qty_order' => $item['orderQty'],
                'qty_max' => $item['generatedQty'],
                'comment' => $item['comment']
            ]);
        }

        $pr->update($request->all());

    }
}
