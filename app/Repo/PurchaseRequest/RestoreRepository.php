<?php 

namespace App\Repo\PurchaseRequest;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\PurchaseRequest;

class RestoreRepository extends BaseRepository implements PurchaseRequestInterface{

    
    public function __construct(){

        $this->modelName = new PurchaseRequest();
    
    }

    public function index($request){

        $purchaseRequests = $this->modelName->withTrashed()->when($request->filter!=null, function($q) use ($request){
          $q->where('purchase_request_no', 'like', '%'.$request->filter.'%');
        })->with(['preparedBy', 'checkedBy', 'approvedBy'])->get()->filter(function($v){
            return $v->deleted_at != null;
        })->values()->all();
  
         return $this->paginate( collect($purchaseRequests) );
      }
      


}
