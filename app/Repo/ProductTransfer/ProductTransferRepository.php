<?php

namespace App\Repo\ProductTransfer;

use App\Model\ProductTransfer;
use App\Model\PTProducts;
use App\Repo\BaseRepository;
use Auth;

class ProductTransferRepository extends BaseRepository implements ProductTransferInterface
{

    public function __construct()
    {

        $this->modelName = new ProductTransfer();

    }

    public function index($request)
    {

        $pt = $this->modelName
            ->when($request->myRequest == 'true', function ($q) {
                $q->where('from', $this->accessable_id());
            })
            ->when($request->myRequest == 'false', function ($q) {
                $q->where('to', $this->accessable_id());
            })
            ->with(['fromBranch', 'toBranch', 'approvedFrom', 'approvedTo', 'preparedBy', 'deliveryReceipt'])
            ->orderBy('created_at', 'desc')
            ->get();
        $pt = $pt->map(function ($v) {
            $approvedFrom = null;
            if ($v->approvedFrom != null) {
                $approvedFrom = $v->approvedFrom['name'];
            }
            $approvedTo = null;
            if ($v->approvedTo != null) {
                $approvedTo = $v->approvedTo['name'];
            }
            $preparedBy = null;
            if ($v->preparedBy != null) {
                $preparedBy = $v->preparedBy['name'];
            }
            $deliveryReceipt = null;
            if ($v->deliveryReceipt != null) {
                $deliveryReceipt = $v->deliveryReceipt['no'];
            }

            return [
                'workingBranch' => $this->accessable_id(),
                'optimus_id' => $v->optimus_id,
                'from' => $v->fromBranch['label'],
                'fromId' => $v->fromBranch['id'],
                'to' => $v->toBranch['label'],
                'toId' => $v->toBranch['id'],
                'approvedFrom' => $approvedFrom,
                'approvedTo' => $approvedTo,
                'preparedBy' => $preparedBy,
                'deliveryReceipt' => $deliveryReceipt,
                'created_at' => $v->created_at,
            ];
        });

        return $this->paginate($pt);
    }

    public function store($request)
    {

        $newRequest = [];
        $newRequest['from'] = $this->accessable_id();
        $newRequest['to'] = $this->removeStringEncode($request->to['value']);
        $newRequest['request_by'] = Auth::User()->id;

        $purchaseTransfer = $this->modelName->create($newRequest);

        foreach ($request->inventories as $i) {
            PTProducts::create([
                'product_transfer_id' => $purchaseTransfer->id,
                'qty' => $i['qty'],
                'company_item_id' => $i['id'],
                'price' => $i['price'],
            ]);
        }
    }

    public function approveFrom($request)
    {

        $this->where('id', $request->id)->update([
            'approved_from' => Auth::User()->id,
        ]);
    }

    public function approveTo($request)
    {

        $this->where('id', $request->id)->update([
            'approved_to' => Auth::User()->id,
        ]);
    }
}
