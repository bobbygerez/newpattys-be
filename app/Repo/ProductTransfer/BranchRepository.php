<?php

namespace App\Repo\ProductTransfer;

use App\Model\Branch;
use App\Model\ProductTransfer;
use App\Repo\BaseRepository;

class BranchRepository extends BaseRepository implements ProductTransferInterface
{

    public function index()
    {

        $b = Branch::where('id', $this->accessable_id())->first();
        return Branch::where('company_id', $b->company_id)
            ->where('id', '!=', $b->id)
            ->get()
            ->map(function ($v) {
                return [
                    'label' => $v->name,
                    'value' => $v->optimus_id,
                ];
            })
            ->values()
            ->all();
    }

    public function branch($request)
    {

        $pt = ProductTransfer::where('id', $this->removeStringEncode($request->id))
            ->with(['fromBranch.address'])
            ->first();

        return [
            'name' => $pt->fromBranch['name'],
            'contact' => $pt->fromBranch['tel'],
            'address1' => $pt->fromBranch['address']['address1'],
            'address2' => $pt->fromBranch['address']['address2'],
            'street' => $pt->fromBranch['address']['street_lot_blk'],
        ];
    }

    public function transferee($request)
    {

        $pt = ProductTransfer::where('id', $this->removeStringEncode($request->id))
            ->with(['toBranch.address'])
            ->first();

        return [
            'name' => $pt->toBranch['name'],
            'toTel' => $pt->toBranch['tel'],
            'toAddress1' => $pt->toBranch['address']['address1'],
            'toAddress2' => $pt->toBranch['address']['address2'],
            'toStreet' => $pt->toBranch['address']['street_lot_blk'],
        ];
    }

    public function approvedTo($request)
    {

        $pt = ProductTransfer::where('id', $this->removeStringEncode($request->id))
            ->with(['approvedTo'])
            ->first();
        if ($pt->approvedTo) {
            return $pt->approvedTo['name'];
        }
        return '';

    }

    public function preparedBy($request)
    {

        $pt = ProductTransfer::where('id', $this->removeStringEncode($request->id))
            ->with(['preparedBy'])
            ->first();
        if ($pt->preparedBy) {
            return $pt->preparedBy['name'];
        }
        return '';

    }
}
