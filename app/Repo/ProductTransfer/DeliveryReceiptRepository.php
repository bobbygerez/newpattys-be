<?php

namespace App\Repo\ProductTransfer;

use App\Model\DeliveryReceipt;
use App\Model\InvoiceInternal;
use App\Model\ProductTransfer;
use App\Repo\BaseRepository;
use Auth;

class DeliveryReceiptRepository extends BaseRepository implements ProductTransferInterface
{

    public function __construct()
    {

        $this->modelName = new DeliveryReceipt();

    }

    public function index($request)
    {

        $dr = $this->modelName
            ->where('product_transfer_id', $this->removeStringEncode($request->id))
            ->with(['products.package', 'invoiceInternal'])
            ->first();

        $invoiceNo = '';
        if ($dr) {
            $invoiceNo = $dr->invoiceInternal['invoice_no'];
        }

        $products = collect($dr->products)->map(function ($v) {
            return [
                'id' => $v->id,
                'label' => $v->name,
                'image' => $v->default_image['path_url'],
                'price' => $v->amount,
                'qty' => $v->pivot->qty,
                'ui' => $v->package['name'],
                'sku' => $v->sku,
                'total' => $v->amount * $v->pivot->qty,
            ];
        })->values()->all();

        return response()->json([
            'products' => $products,
            'invoiceNo' => $invoiceNo,
            'deliveryReceiptNo' => $dr->no,
        ]);

    }

    public function store($request)
    {

        ProductTransfer::where('id', $this->removeStringEncode($request->productTransferId)
        )->update([
            'prepared_by' => Auth::User()->id,
        ]);

        $ii = InvoiceInternal::create([
            'invoice_no' => $request->invoiceNo,
            'invoiceable_id' => $this->accessable_id(),
            'invoiceable_type' => $this->accessable_type(),
        ]);

        $newRequest = $request->all();
        $newRequest['receiptable_id'] = $this->accessable_id();
        $newRequest['receiptable_type'] = $this->accessable_type();
        $newRequest['invoice_internal_id'] = $ii->id;
        $newRequest['product_transfer_id'] = $this->removeStringEncode($request->productTransferId);
        $this->modelName->create($newRequest);

        $ii = InvoiceInternal::where('id', $ii->id)->first();

        foreach ($request->cart as $c) {

            $ii->products()->attach([
                $c['id'] => [
                    'qty' => $c['qty'],
                    'price' => $c['price'],
                ],
            ]);
        }

    }

    public function updatee($request)
    {
        $pt = ProductTransfer::where('id', $this->removeStringEncode($request->productTransferId)
        )->first();

        $pt->update([
            'prepared_by' => Auth::User()->id,
        ]);

        $newRequest = $request->all();
        $newRequest['receiptable_id'] = $this->accessable_id();
        $newRequest['receiptable_type'] = $this->accessable_type();
        $newRequest['invoice_internal_id'] = $request->invoiceId;
        $newRequest['product_transfer_id'] = $this->removeStringEncode($request->productTransferId);
        $dr = $this->modelName->create($newRequest);

        $dr = $this->modelName->where('id', $dr->id)->first();

        foreach ($request->cart as $c) {

            $dr->products()->attach($dr->id, [
                'company_item_id' => $c['id'],
                'delivery_receipt_id' => $dr->id,
                'qty' => $c['qty'],
                'price' => $c['price'],
            ]);
        }

    }

}
