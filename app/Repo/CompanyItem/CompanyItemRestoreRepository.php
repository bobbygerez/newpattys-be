<?php 

namespace App\Repo\CompanyItem;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\CompanyItem;

class CompanyItemRestoreRepository extends BaseRepository implements CompanyItemInterface{


    public function __construct(){

        $this->modelName = new CompanyItem();
    
    }

    public function index($request){

        $companyProducts = $this->modelName
        ->withTrashed()
        ->when($request->filter !='', function($q) use ($request){
                $q->where('name', 'like', '%'.$request->filter.'%');
                $q->orWhere('sku', 'like', '%'.$request->filter.'%');
                $q->orWhere('barcode', 'like', '%'.$request->filter.'%');
        })
       ->orderBy('created_at', 'desc')
       ->get()
       ->filter(function($v){
           return $v->deleted_at != null;
       })->values()->all();

       return $this->paginate( collect($companyProducts) );
    }

}
