<?php 

namespace App\Repo\CompanyItem;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\CompanyItem;
use App\Model\CompanyItemVendorRank;
use App\Model\Image;

class CompanyItemRepository extends BaseRepository implements CompanyItemInterface{


    public function __construct(){

        $this->modelName = new CompanyItem();
    
    }

    public function index($request){

        $companyProducts = $this->modelName
       ->when($request->filter !='', function($q) use ($request){
            $q->where('name', 'like', '%'.$request->filter.'%');
            $q->orWhere('sku', 'like', '%'.$request->filter.'%');
            $q->orWhere('barcode', 'like', '%'.$request->filter.'%');
       })
       ->with(['category', 'package', 'images'])
       ->orderBy('created_at', 'desc')
       ->get();

       return $this->paginate( $companyProducts );
    }

    public function edit($request){
        
        return $this->ci()->only('name', 'category', 'package', 'float_amount', 'sku', 'barcode', 'min_qty', 'reorder_qty', 'max_qty', 'images');
        

    }

    public function vendors(){

        return $vendors = $this->ci()->companyItemVendorRanks;

        $newVendors = [];
        foreach($vendors as $vendor){
            if($vendor->vendorable_type === 'App\Model\Vendor'){
                if($vendor->vendorProduct != null){
                    $newVendors[] = [
                        'id' => $vendor->id,
                        'amount' => $vendor->vendorProduct['string_price'],
                        'label' => $vendor->vendorable['name'],
                        'value' => $vendor->vendorable['optimus_id'],
                        'rank' => $vendor->rank,
                        'percentage' => $vendor->percentage,
                        'entity' => $vendor->vendorProduct['vendor']['entity']
                    ];
                }
                
            }
            
            if($vendor->vendorable_type === 'App\Model\Branch'){
                if($vendor->branchItem != null){
                    $newVendors[] = [
                        'id' => $vendor->id,
                        'amount' => $vendor->branchItem['companyItem']['float_amount'],
                        'label' => $vendor->vendorable['name'],
                        'value' => $vendor->vendorable['optimus_id'],
                        'rank' => $vendor->rank,
                        'percentage' => $vendor->percentage,
                        'entity' => $vendor->branchItem['branch']['entity']
                    ];
                }
                
            }
        }

        return $newVendors;
    }

    public function store($request){
        
        $ci  = $this->create($this->data());
        $ci = $this->modelName->find($ci->id);
        $this->addImages($ci, $request);

    }

    public function update($request){
        
        $ci = $this->find($request->id);
        $vendors = json_decode($request->vendors);
        
        $rank = 1;
        $vendorIds = collect($vendors)->map(function($vendor){
            return $this->removeStringEncode($vendor->vendor->value);
        })->values()->all();

        foreach($ci->companyItemVendorRanks as $v){
            if(!in_array($v->id, $vendorIds)){
                $v->delete();
            }
            
        }

       

        if($vendors != ''){

            foreach($vendors as $vendor){
                    CompanyItemVendorRank::create([
                        'company_item_id' => $this->removeStringEncode($request->id) ,
                        'vendorable_id' => $this->removeStringEncode($vendor->vendor->value),
                        'vendorable_type' => $vendor->vendor->entity,
                        'rank' => $rank,
                        'percentage' => $vendor->percentage
                    ]);

                    $rank++;
            }

        }
        
        
        $ciImages = $ci->images;
       
       foreach($ciImages as $img){
            if(!in_array($img->id, $request->ids)){
                $img->delete();
            }
       }
       if($request->ids != null){
            $images = Image::whereIn('id', $request->ids)->get();

            foreach($images as $image){
                if($request->is_primary === $image->name){
                    Image::find($image->id)->update([
                        'is_primary' => true
                    ]);
                }else{
                    Image::find($image->id)->update([
                        'is_primary' => false
                    ]);
                }
            }
       }

       $this->addImages($ci, $request);

        $ci->update($this->data());


    }

    public function data(){
        //CHECK IF YOU ARE IN WORKING COMPANY
        $request = app()->make('request');
        $accessables = array();
        parse_str($request->header('accessables'), $accessables);
        $newRequest = $request->all();
        $newRequest['company_id'] = $this->removeStringEncode($accessables['accessable_id']);
        $newRequest['category_id'] = $this->removeStringEncode($request->category_id);
        $newRequest['package_id'] = $this->removeStringEncode($request->package_id);
        return $newRequest;
    }
    
    public function ci(){
        $request = app()->make('request');
        return $ci = $this->where('id', $request->id)
        ->with(['category', 'package', 'companyItemVendorRanks.vendorProduct.vendor', 'companyItemVendorRanks.branchItem.companyItem', 'companyItemVendorRanks.vendorable', 'images'])->first();
    }

    public function addImages($model, $request){
        if(isset($_FILES["files"]["name"])){
            foreach($_FILES["files"]["name"] as $key=>$tmp_name){
                $name =  $_FILES["files"]["name"][$key];
                $file_name= str_random(5) . '-'. $name;
                $file_tmp=$_FILES["files"]["tmp_name"][$key];
                $uploadfile = file_get_contents($file_tmp);

                \File::put(public_path() . '/images/uploads/'.$file_name, $uploadfile);

                if ($_FILES["files"]["name"][$key] === $request->is_primary) {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $model->id,
                        'imageable_type' => 'App\Model\CompanyItem',
                        'is_primary' => true,
                        'name' => $name,
                        'desc' => $name,
                    ]);
                    
                }else{
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $model->id,
                        'imageable_type' => 'App\Model\CompanyItem',
                        'is_primary' => false,
                        'name' => $name,
                        'desc' => $name,
                    ]);
                }
            }
        }

    }
      
}
