<?php 

namespace App\Repo\Receipt;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Receipt;

class ReceiptRepository extends BaseRepository implements ReceiptInterface{


    public function __construct(){

        $this->modelName = new Receipt();
    
    }

}