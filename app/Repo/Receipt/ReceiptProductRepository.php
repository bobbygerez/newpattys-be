<?php 

namespace App\Repo\Receipt;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;

class ReceiptProductRepository extends BaseRepository implements ReceiptInterface{

    public function index($request){

        $entity = $request->accessable_type::where('id', $this->removeStringEncode( $request->accessable_id) )
            ->with(['products.images', 'products.taxType', 'products.chartAccount'])
            ->first();
        return $entity->products;
    }
}