<?php 

namespace App\Repo\Receipt;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;

class ChartAccountRepository extends BaseRepository implements ReceiptInterface{


    public function index($request){

        if($request->accessable_type === 'App\Model\Company'){
            $chartAccounts = $request->accessable_type::where('id', $this->removeStringEncode($request->accessable_id))
                                ->with(['chartAccounts.children'])
                                ->first()->chartAccounts;
        
            return $chartAccounts->filter(function($val){
                return count($val->children) === 0;
            })->values()->all();
        }else{
            $chartAccounts = $request->accessable_type::where('id', $this->removeStringEncode($request->accessable_id))
                                ->with(['company.chartAccounts.children'])
                                ->first()->company->chartAccounts;
        
                return $chartAccounts->filter(function($val){
                    return count($val->children) === 0;
                })->values()->all();
        }
        
        
    }

}