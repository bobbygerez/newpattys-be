<?php 

namespace App\Repo\Lender;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Lender;

class LenderRepository extends BaseRepository implements LenderInterface{


    public function __construct(){

        $this->modelName = new Lender();
    
    }

}