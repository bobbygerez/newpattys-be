<?php 

namespace App\Repo\PurchaseReceived;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\PurchaseReceived;
use App\Model\PurchaseReceivedItem;
use Auth;
use App\Model\Branch;
use Carbon\Carbon;
use App\Model\CompanyItem;
use App\Model\GlAccount;
use App\Model\TransactionSetup;

class PurchasesRepository extends BaseRepository implements PurchaseReceivedInterface{


    public function __construct(){

        $this->modelName = new PurchaseReceived();
    
    }

    public function store($request){
         $branch = Branch::find($this->accessable_id());

        $poOnThisDate = $this->modelName->where('created_at', '>', Carbon::now()->subDays(1) )
            ->get();
        
        $no = 1;
        if(count($poOnThisDate) > 0){
            $no = count($poOnThisDate) + 1;
        }else{
            $no = 1;
        }
        $prCode = $branch->acronym.'-'. date('Ymd') . '-' . str_pad($no, 4, '0', STR_PAD_LEFT);

        $newRequest = $request->all();
        $newRequest['purchase_received_no'] = $prCode;
        $newRequest['received_by'] = Auth::User()->id;
        $newRequest['token'] = str_random(32);
        $newRequest['purchasable_id'] = $this->accessable_id();
        $newRequest['purchasable_type'] = $this->accessable_type();
        $pr = $this->create($newRequest);

        foreach($request->items as $item){
            $dbItem = CompanyItem::where('id', $item['item']['company_item']['id'])->with(['category'])->first();
            PurchaseReceivedItem::create([
                'purchase_received_id' => $pr->id, 
                'company_item' => $item['item']['company_item']['id'], 
                'amount' => $item['item']['company_item']['amount'], 
                'order_qty' => $item['qty'], 
                'received_qty' => $item['received'],
                'branch_chart_of_account_id' => $dbItem->category['chart_account_id']
            ]);
        }

       $pr = $this->modelName->where('id', $pr->id)->with('purchaseReceivedItems')->first();

       $trans = TransactionSetup::where('model', 'App\Model\PurchaseReceived')->where('defaultt', 1)->first();

        GlAccount::create([
            'branch_chart_account_id' => $trans->dr_chart_account_id,
            'amount'=> $pr->pr_total,
            'accountable_id' => $pr->id,
            'accountable_type' => 'App\Model\PurchaseReceived'
        ]);
        

    }

    public function updatee($request){
        $pr = $this->where('id', $request->id)->first();
        $newRequest = $request->all();
        $newRequest['vendorable_id'] = $this->removeStringEncode($request->vendorable_id);
        foreach($pr->purchaseReceivedItems as $i){
            $i->delete();
        }
        foreach($request->items as $item){
            $dbItem = CompanyItem::where('id', $item['item']['company_item']['id'])->with(['category'])->first();
            PurchaseReceivedItem::create([
                'purchase_received_id' => $pr->id, 
                'company_item' => $item['item']['company_item']['id'], 
                'amount' => $item['item']['company_item']['amount'],
                'order_qty' => $item['qty'], 
                'received_qty' => $item['received'],
                'branch_chart_of_account_id' => $dbItem->category['chart_account_id']
            ]);
        }

        $gl = GlAccount::where('accountable_id', $pr->id)
            ->where('accountable_type', 'App\Model\PurchaseReceived')
            ->first();
        if($gl){
            $gl->delete();
        }

        GlAccount::create([
            'branch_chart_account_id' => $this->removeStringEncode( $request->selectedTransactionSetup['value'] ),
            'amount'=> $pr->pr_total,
            'accountable_id' => $pr->id,
            'accountable_type' => 'App\Model\PurchaseReceived'
        ]);
        
        $pr->update($request->all());
    }

    
}
