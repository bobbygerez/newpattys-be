<?php 

namespace App\Repo\Area;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Area;
use App\Model\AreaBranch;
class AreaRepository extends BaseRepository implements AreaInterface{


    public function __construct(){

        $this->modelName = new Area();
    
    }

    public function index($request){
        return $this->paginate(
            $this->modelName->when($request->filter != '', function($q) use ($request){
                $q->where('name', 'like', '%'. $request->filter . '%');
            })
            ->orderBy('created_at', 'desc')
            ->get()
        );
    }

    public function  store($request){
        $newRequest =  $request->all();
        $newRequest['areable_id'] = $this->accessable_id();
        $newRequest['areable_type'] = $this->accessable_type();
        $a = $this->modelName->create($newRequest);
        foreach($request->branch_ids as $b){
            AreaBranch::create([
                'area_id' => $a->id,
                'branch_id' => $this->removeStringEncode($b['value'])
            ]);
        }
    }

    public function  update($request){
        $newRequest =  $request->all();
        $newRequest['areable_id'] = $this->accessable_id();
        $newRequest['areable_type'] = $this->accessable_type();
        $a = $this->find($request->id);

        $abs = AreaBranch::where('area_id', $this->removeStringEncode($request->id))->get();
        foreach($abs as $ab){
            $ab->delete();
        }

        foreach($request->branch_ids as $b){
            AreaBranch::create([
                'area_id' => $a->id,
                'branch_id' => $this->removeStringEncode($b['value'])
            ]);
        }
        $a->update($newRequest);
    }
    
}
