<?php 

namespace App\Repo\User;
use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\User;
use App\Model\Role;
use Auth;
use App\Traits\Obfuscate\Optimuss;
class UserRoleMenuRepository extends BaseRepository implements UserInterface{


    use optimuss;

    public function __construct(){

        $this->modelName = new User();
    
    }


    public function index($request){

        $roleIds = collect($request->roleIds)->map(function($val){
            return $this->removeStringEncode($val);
        });
        
        return Role::whereIn('id', $roleIds)->with(['accessables.menu', 'accessables.accessRight'])->get()
                ->map(function($val){
                    return $val->accessables;
                })->flatten(1)
                ->filter(function($val) use ($request){
                    return $val->accessable_type === $request->modelType && $val->accessable_id == $this->removeStringEncode($request->modelId);
                })->unique('id')->values()->all();
    }
}