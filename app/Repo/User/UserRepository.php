<?php 

namespace App\Repo\User;
use App\Notifications\UserRegisteredSuccessfully;
use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\User;
use App\Model\Assignment;
use Auth;
use Hash;
class UserRepository extends BaseRepository implements UserInterface{


    public function __construct(){

        $this->modelName = new User();
    
    }


    public function index($request){
        if($this->accessable_type() === 'App\Model\Company'){
               $users = $this->modelName
               ->where('userable_id', $this->accessable_id())
                ->where('userable_type', $this->accessable_type() )
                ->with(['userable.branches.users'])
                ->get();

                 $companyUsers = $users->map(function($val){
                    return $val->only(['optimus_id', 'name', 'email', 'mobile', 'status', 'status_text', 'mobile', 'created_at']);
                })->values()->all();

                $branchUsers =  $users->map(function($val){
                    return $val->userable;
                })
                ->first()
                ->branches
                ->flatMap(function($val){
                    return $val->users;
                })
                ->map(function($val){
                    return $val->only(['optimus_id', 'name', 'email', 'mobile', 'status', 'status_text', 'mobile', 'created_at']);
                })
                ->values()->all();
                
                 $merge = collect( array_merge($companyUsers, $branchUsers) )
                            ->filter(function($item) use ($request) {
                                return $item['status'] == $request->status;
                            });

                $filter = $this->filterName($merge);

                return $this->paginate( collect($filter) );
            }

            $users = $this->modelName->where('userable_id', $this->accessable_id())
                    ->where('userable_type', $this->accessable_type() )
                    ->where('status', $request->status)
                    ->get()
                    ->map(function($val){
                        return $val->only(['optimus_id', 'name', 'email', 'mobile', 'status', 'status_text', 'mobile', 'created_at']);
                    })
                    ->values()->all();
            $filter = $this->filterName($users);

            return $this->paginate( collect(
                $this->filterName($filter)
            ) );
    }

    public function edit($request){

       return $this->where('id', $request->id)
                    ->with([
                        'roles', 
                        'information.address.country', 
                        'information.address.region',
                        'information.address.province',
                        'information.address.city',
                        'information.address.brgy',
                        'information.civilStatus',
                        'information.gender',
                        'assignments.assignmentable'
                    ])
                    ->first();
    }

    public function store($request){

        $newRequest = $request->user;
        $newRequest['information_id'] = $this->removeStringEncode($request->user['information_id']);
        $newRequest['password'] = Hash::make($request->user['password']);
        $newRequest['activation_code'] = str_random(30).time();
        $newRequest['userable_id'] = $this->removeStringEncode( $request->userable_id );
        $newRequest['userable_type'] = $request->userable_type;
        $newUser = $this->modelName->create($newRequest);
        $user = $this->modelName->find($newUser->id);

        foreach($request->roles as $role){
            $user->roles()->attach($user->id, [
                'role_id' => $this->removeStringEncode($role),
                'user_id' => $user->id
            ]);
        }

        $assignments = $request->assignments;
        foreach($assignments as $key => $assignment){
            reset($assignments);

            if ($key === key($assignments)){
                $user->assignments()->create([
                    'is_default' => 1,
                    'user_id' => $user->id,
                    'assignmentable_type' => $assignment['entity'],
                    'assignmentable_id' => $this->removeStringEncode($assignment['value'])
                ]);
            }else{
                if ($key === key($assignments)){
                    $user->assignments()->create([
                        'is_default' => 0,
                        'user_id' => $user->id,
                        'assignmentable_type' => $assignment['entity'],
                        'assignmentable_id' => $this->removeStringEncode($assignment['value'])
                    ]);
                }
            }
            
            
        }

        // $user->notify(new UserRegisteredSuccessfully($user));
    }   
    public function update($request){

        $newRequest = $request->user;
        $newRequest['information_id'] = $this->removeStringEncode($request->user['information_id']);
        
        $user = $this->find($request->id);
        
        $user->roles()->detach();
        foreach($request->roles as $role){
            $user->roles()->attach($user->id, [
                'role_id' => $this->removeStringEncode($role),
                'user_id' => $user->id
            ]);
        }

        $assignments = $request->assignments;

        $user->assignments()->delete();
        
        foreach($assignments as $key => $assignment){
            reset($assignments);

            if ($key === key($assignments)){
                $user->assignments()->create([
                    'is_default' => 1,
                    'user_id' => $user->id,
                    'assignmentable_type' => $assignment['entity'],
                    'assignmentable_id' => $this->removeStringEncode($assignment['value'])
                ]);
            }else{
                if ($key === key($assignments)){
                    $user->assignments()->create([
                        'is_default' => 0,
                        'user_id' => $user->id,
                        'assignmentable_type' => $assignment['entity'],
                        'assignmentable_id' => $this->removeStringEncode($assignment['value'])
                    ]);
                }
            }
            
        }

        $user->update($newRequest);

    }

    public function destroy($request){

        if( $this->removeStringEncode($request->id) == Auth::User()->id){
            return response()->json(['message' => 'You cannot delete your own account.'], 403);
        }else{
            $this->modelName->find( $this->removeStringEncode($request->id) )->delete();
            return response()->json([
                'success' => true
            ]);
        }
    }

    


    public function accessableUsers($user, $request){
            
            $min = $user->roles->min('parent_id');
            
            $role = $user->roles->filter(function($val) use ($min) {
                return $val['parent_id'] === $min;
            })->first();

            $merge = array_merge(collect($role->accessables)->toArray(), collect( $this->mapRecursive($role->allChildren) )->flatten(1)->toArray());

            return collect($merge)->filter(function($val) use ($request) {
                return $val['accessable_type'] == $request->accessable_type && $val['accessable_id'] == $this->removeStringEncode($request->accessable_id) && isset($val['accessable']);
            })->map(function($val){
                return $val['accessable']['users'];
            })->flatten(1);
            
    }


}