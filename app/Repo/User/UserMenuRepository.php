<?php 

namespace App\Repo\User;
use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\User;
use Auth;

class UserMenuRepository extends BaseRepository implements UserInterface{


    public function __construct(){

        $this->modelName = new User();
    
    }

    public function index($request){
        
        $user = $this->modelName->where('id', Auth::User()->id)
        ->with(['roles.allChildren.accessables.menu.allChildren', 'roles.accessables.menu.allChildren'])
        ->first();

       if(count($user->roles) > 1){
            $min = $user->roles->min('parent_id');
            $roles = collect($user->roles)->filter(function($val) use ($min) {
                    return $val->parent_id === $min;
            })->values()->all();
            $accessables = $this->flatten($roles);

            return $flatten = collect($accessables)->flatten(1)->map(function($val){
                return $val->menu;
            })->unique('id')->sortBy('id')->values()->all();
       }else{
           return $accessables = collect($user->roles)->map(function($val){
                return $val->accessables;
           })->flatten(1)->map(function($val){
                return $val->menu;
            })->unique('id')->sortBy('id')->values()->all();
       }

       
    }


     //Flatten Recursive
     public function flatten($array) {
        $result = [];
        foreach ($array as $item) {
            if($item['accessables'] != null){
                $result[] = $item['accessables'];
            }
            $result = array_merge($result, $this->flatten($item['allChildren']));
        }
        return array_filter($result);
    }

    
}