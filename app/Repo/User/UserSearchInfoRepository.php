<?php 

namespace App\Repo\User;
use App\Notifications\UserRegisteredSuccessfully;
use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\User;
use App\Model\Information;
use Hash;
class UserSearchInfoRepository extends BaseRepository implements UserInterface{


    public function __construct(){

        $this->modelName = new User();
    
    }

    public function store($request){

        $requestInfo =  $request->info;
        $requestInfo['informationable_id'] = $this->removeStringEncode( $request->info['informationable_id'] );
       
        $info = Information::create($requestInfo);
        Information::find($info->id)->address()->create($request->info['address']);

        $requestUser = $request->user;
        $requestUser['password'] = Hash::make($request->user['password']);
        $requestUser['information_id'] = $info->id;
        $requestUser['userable_id'] = $this->removeStringEncode( $request->accessable_id );
        $requestUser['userable_type'] = $request->accessable_type;
        $user = $this->modelName->create($requestUser);
        $newUser = $this->modelName->find($user->id);

        foreach($request->roles as $role){
            $newUser->roles()->attach($user->id, [
                'role_id' => $this->removeStringEncode( $role['value'] ),
                'user_id' => $user->id
            ]);
        }

        $assignments =  $request->assignments;
        foreach($assignments as $key => $assignment){

            reset($assignments);

            if ($key === key($assignments)){
                $newUser->assignments()->create([
                    'is_default' => true,
                    'user_id' => $user->id,
                    'assignmentable_id' => $this->removeStringEncode( $assignment['value'] ),
                    'assignmentable_type' => $assignment['entity']
                ]);
            }else{
                $newUser->assignments()->create([
                    'is_default' => false,
                    'user_id' => $user->id,
                    'assignmentable_id' => $this->removeStringEncode( $assignment['value'] ),
                    'assignmentable_type' => $assignment['entity']
                ]);
            }
        }
       
        // $user->notify(new UserRegisteredSuccessfully($user));
    }
    

}