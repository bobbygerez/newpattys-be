<?php 

namespace App\Repo\User;
use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\User;
use Auth;

class UserRoleRepository extends BaseRepository implements UserInterface{


    public function __construct(){

        $this->modelName = new User();
    
    }

    public function index($request){

      $user = $this->modelName->where('id', Auth::User()->id)->with(['roles.allChildren'])->first();

      $roles = collect( $this->flatten($user->roles->toArray()) )->unique('id');
      
      $min = $roles->min('parent_id');

      return $roles->filter(function($val) use($min){
        return $val['parent_id'] > $min ;
      })->values()->all();
       
    }

}