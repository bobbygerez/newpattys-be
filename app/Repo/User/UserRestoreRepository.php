<?php 

namespace App\Repo\User;
use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\User;
use Auth;
class UserRestoreRepository extends UserRepository implements UserInterface{

    public function __construct(){

        $this->modelName = new User();
    
    }

    public function index($request){

        $users = $this->modelName->where('userable_id', $this->removeStringEncode($request->accessable_id))
            ->where('userable_type', $request->accessable_type)
            ->where('deleted_at', '!=', null)
            ->withTrashed()
            ->get()
            ->map(function($val){
                return $val->only(['optimus_id', 'name', 'mobile', 'created_at']);
            })
            ->values()->all();
            $filter = $this->filterName($users);
        return $this->paginate( 
            collect($filter)
            );
    }

    public function edit($request){

        return $this->modelName->withTrashed()->where('id', $this->removeStringEncode($request->id))
                     ->with([
                         'roles', 
                         'information.address.country', 
                         'information.address.region',
                         'information.address.province',
                         'information.address.city',
                         'information.address.brgy',
                         'information.civilStatus',
                         'information.gender',
                         'assignments.assignmentable'
                     ])
                     ->first();
     }

     public function destroy($request){
        $user = $this->modelName->withTrashed()->find($this->removeStringEncode($request->id));
        $user->assignments()->delete();
        $user->roles()->detach();
        $user->forceDelete();
     }
    public function restore($request){

        $this->modelName->withTrashed()->find($this->removeStringEncode($request->id))->restore();
    }

}