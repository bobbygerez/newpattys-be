<?php

namespace App\Repo\Transaction;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Transaction;
use App\Model\GeneralLedger;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class GeneralJournalRepository extends BaseRepository implements TransactionInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Transaction();
        $this->glModel = new GeneralLedger();

    }

    public function create($request){

        $newRequest = $this->customRequest($request);
        $trans = $this->modelName->create($newRequest);
        $trans = $this->modelName->find($trans->id);
        $this->createUpdate($request, $trans);

    }

    public function update($request){

        $newRequest = $this->customRequest($request);
        $trans = $this->find($request->transaction['optimus_id']);
        $this->createUpdate($request, $trans);
        $trans->update($newRequest);

    }

    public function customRequest($request){
        $newRequest = $request->transaction;
        $newRequest['total_debit'] = $request->transaction['total_debit'];
        $newRequest['transaction_type_id'] = 3;
        $newRequest['transactable_id'] =  $this->accessable_id();
        $newRequest['transactable_type'] =  $this->accessable_type();
        $newRequest['refnum'] =  str_replace('0.', '', microtime() . uniqid(true));
        return $newRequest;
    }

    public function createUpdate($request, $trans){

        $trans->productTransaction()->delete();
        foreach($request->additionalProducts as $product){
            $trans->productTransaction()->create([
                'transaction_id' => $trans->id,
                'desc' => $product['desc'],
                'amount' => $product['amount'],
                'debit_amount' => $product['debit'],
                'chart_account_id' => $product['chart_account_id']['value'],
            ]);
        }

    }

    public function glentry($request)
    {
        $request['ledgerable_id'] = $this->accessable_id();
        $request['ledgerable_type'] = $this->accessable_type();
        $trans = $this->glModel->create($request);
    }
}
