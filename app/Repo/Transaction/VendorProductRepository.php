<?php 

namespace App\Repo\Transaction;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Transaction;
class VendorProductRepository extends BaseRepository implements TransactionInterface{

    public function __construct(){

        $this->modelName = new Transaction();
    }


    public function index($request){
        return $request->accessable_type::where('id', $this->removeStringEncode($request->accessable_id))
                ->with(['products.taxType', 'products.chartAccount'])
                ->first()
                ->products;
        return $request->all();
    }
}