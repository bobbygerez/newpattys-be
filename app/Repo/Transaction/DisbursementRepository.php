<?php 

namespace App\Repo\Transaction;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Transaction;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class DisbursementRepository extends TransactionRepository implements TransactionInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Transaction();
    
    }

    public function edit($id){

        return $this->modelName->where('id', $this->optimus()->encode($id))
            ->with(['purchaseReceived', 'productTransaction', 'payee', 'transactable'])
            ->first();
    }

}