<?php 

namespace App\Repo\Transaction;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Transaction;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class SalesJournalRepository extends TransactionRepository implements TransactionInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Transaction();
    
    }


    public function getSalesPurchaseReceived($request){
        $model = $request->modelType::where('id', $this->optimus()->encode($request->modelId))
                ->with(['purchaseReceived.saleInvoice', 'purchaseReceived.products'])
                ->first();
        
        if($model != null){

           return  collect($model->purchaseReceived)->filter(function($value) use ($request){
                return $value->saleInvoice['salable_id'] === $this->optimus()->encode($request->salableId) && $value->saleInvoice['salable_type'] === $request->salableType;
            });
        }
        return $model;
        
    }
    
    public function create($request){

        $newRequest = $this->customRequest($request);
        $trans = $this->modelName->create($newRequest);
        $trans = $this->modelName->find($trans->id);
        $this->createUpdate($request, $trans);
       
        
    }

    public function update($request){

        $newRequest = $this->customRequest($request);
        $trans = $this->find($request->transaction['optimus_id']);
        $this->createUpdate($request, $trans);
        $trans->update($newRequest);
        
        
    }

    public function customRequest($request){
        $newRequest = $request->transaction;
        $newRequest['chart_account_id'] = $request->transaction['chart_account_id']['value'];
        $newRequest['transaction_type_id'] = 2;
        $newRequest['transactable_id'] =  $this->optimus()->encode($request->transaction['transactable_id']) ;
        $newRequest['payment_method_id'] = $this->optimus()->encode($request->transaction['payment_method_id']['value']);

        return $newRequest;
    }

    public function createUpdate($request, $trans){

       $trans->saleInvoices()->detach();
        foreach($request->invoices as $pr){
            $trans->saleInvoices()->attach($trans->id, [
                'transaction_id' => $trans->id,
                'sale_invoice_id' => $pr['purchase_received_id']['sale_invoice']['id'],
                'amount_due' => $pr['amount_due'],
                'amount_paid' =>  $pr['amount_paid'],
                'vat_amount' => $pr['vat_amount'],
                'date_due' =>  $pr['date_due'],
                'description' =>  $pr['description'],
                'discount' =>  $pr['discount'],
                'vat_exempt_sales' => $pr['vat_exempt_sales'],
                'vatable_sales' => $pr['vatable_sales'],
                'zero_rated_sales' => $pr['zero_rated_sales'],
                'pay' =>  true
                ]);
        }
        $trans->productTransaction()->delete();
        foreach($request->additionalProducts as $product){
            $trans->productTransaction()->create([
                'transaction_id' => $trans->id,
                'product_id' => $product['product_id']['value'],
                'job_id' => $product['job_id'],
                'desc' => $product['desc'],
                'discount_amt' => $product['discount_amt'],
                'qty' => $product['qty'],
                'price' => $product['price'],
                'amount' => $product['amount'],
                'chart_account_id' => $product['chart_account_id']['value'],
                'tax_type_id' => $product['tax_type_id']['value']
            ]);
        }

        $trans->payor()->updateOrCreate([
            'transaction_id' => $trans->id,
            'payable_id' =>  $this->optimus()->encode($request->payor['payable_id']),
            'payable_type' => $request->payor['payable_type']
        ]);
    }

    
}