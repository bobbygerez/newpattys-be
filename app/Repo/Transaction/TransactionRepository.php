<?php 

namespace App\Repo\Transaction;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Transaction;
use App\Model\TransactionType;
use App\Model\ChartAccount;
use App\Model\Purchase;
use App\Model\PurchaseReceived;
use App\Traits\Obfuscate\Optimuss;
use App\Model\User;
use Auth;
use Carbon\Carbon;
class TransactionRepository extends BaseRepository implements TransactionInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Transaction();
    }

    public function index($request){

        $transactions = $this->modelName->where('transactable_id', $this->removeStringEncode ($request->accessable_id))
            ->when($request->startDate != '', function($q) use ($request) {
                $q->where('created_at', '>=', $request->startDate);
            })
            ->when($request->startDate != '', function($q) use ($request) {
                $q->where('created_at', '<=', Carbon::parse($request->endDate)->addDay());
            })
            ->when($request->filter !='', function($q) use ($request) {
                $q->whereHas('chartAccount', function($q) use ($request) {
                    return $q->where('name', 'like', '%'.$request->filter.'%');
                });
            })
            ->where('transactable_type', $request->accessable_type)
            ->with(['transactionType', 'chartAccount', 'createdBy'])
            ->get();

        return $this->paginate($transactions);
    }

    // public function updateDisbursement($request){

    //     $trans = $this->find($request->id);
    //     $newRequest = $request->transaction;
    //     $newRequest['chart_account_id'] = $request->transaction['chart_account_id']['value'];
    //     $newRequest['transaction_type_id'] = 1;
    //     $newRequest['transactable_id'] =  $this->optimus()->encode($request->transaction['transactable_id']) ;
    //     $trans->purchaseReceived()->detach();
    //     $trans->productTransaction()->delete();

    //     $trans->update($newRequest);
       

    //     $trans = $this->find($request->id);
    //     foreach($request->purchaseReceived as $pr){
    //         $trans->purchaseReceived()->attach($trans->id, [
    //             'transaction_id' => $trans->id,
    //             'purchase_received_id' => $pr['purchase_received_id']['value'],
    //             'amount_due' => $pr['amount_due'],
    //             'amount_paid' =>  $pr['amount_paid'],
    //             'vat_amount' => $pr['vat_amount'],
    //             'date_due' =>  $pr['date_due'],
    //             'description' =>  $pr['description'],
    //             'discount' =>  $pr['discount'],
    //             'vat_exempt_sales' => $pr['vat_exempt_sales'],
    //             'vatable_sales' => $pr['vatable_sales'],
    //             'zero_rated_sales' => $pr['zero_rated_sales'],
    //             'pay' =>  true
    //             ]);
    //     }

    //     foreach($request->additionalProducts as $product){
    //         $trans->productTransaction()->create([
    //             'transaction_id' => $trans->id,
    //             'product_id' => $product['product_id']['value'],
    //             'job_id' => $product['job_id'],
    //             'desc' => $product['desc'],
    //             'discount_amt' => $product['discount_amt'],
    //             'qty' => $product['qty'],
    //             'price' => $product['price'],
    //             'amount' => $product['amount'],
    //             'chart_account_id' => $product['chart_account_id']['value'],
    //             'tax_type_id' => $product['tax_type_id']['value']
    //         ]);
    //     }

    //     $trans->payee()->updateOrCreate([
    //         'transaction_id' => $trans->id,
    //         'payable_id' =>  $this->optimus()->encode($request->payor['payable_id']),
    //         'payable_type' => $request->payor['payable_type']
    //     ]);
    // }
    
    // public function disbursement($request){

    //     $newRequest = $request->transaction;
    //     $newRequest['chart_account_id'] = $request->transaction['chart_account_id']['value'];
    //     $newRequest['transaction_type_id'] = 1;
    //     $newRequest['transactable_id'] =  $this->optimus()->encode($request->transaction['transactable_id']) ;
    //     $newRequest['refnum'] =  str_replace('0.', '', microtime() . uniqid(true));

    //     $trans = $this->modelName->create($newRequest);
    //     $trans = $this->modelName->find($trans->id);

    //     foreach($request->purchaseReceived as $pr){
    //         $trans->purchaseReceived()->attach($trans->id, [
    //             'transaction_id' => $trans->id,
    //             'purchase_received_id' => $pr['purchase_received_id']['value'],
    //             'amount_due' => $pr['amount_due'],
    //             'amount_paid' =>  $pr['amount_paid'],
    //             'vat_amount' => $pr['vat_amount'],
    //             'date_due' =>  $pr['date_due'],
    //             'description' =>  $pr['description'],
    //             'discount' =>  $pr['discount'],
    //             'vat_exempt_sales' => $pr['vat_exempt_sales'],
    //             'vatable_sales' => $pr['vatable_sales'],
    //             'zero_rated_sales' => $pr['zero_rated_sales'],
    //             'pay' =>  true
    //             ]);
    //     }

    //     foreach($request->additionalProducts as $product){
    //         $trans->productTransaction()->create([
    //             'transaction_id' => $trans->id,
    //             'product_id' => $product['product_id']['value'],
    //             'job_id' => $product['job_id'],
    //             'desc' => $product['desc'],
    //             'discount_amt' => $product['discount_amt'],
    //             'qty' => $product['qty'],
    //             'price' => $product['price'],
    //             'amount' => $product['amount'],
    //             'chart_account_id' => $product['chart_account_id']['value'],
    //             'tax_type_id' => $product['tax_type_id']['value']
    //         ]);
    //     }

    //     $trans->payee()->create([
    //         'transaction_id' => $trans->id,
    //         'payable_id' =>  $this->optimus()->encode($request->payor['payable_id']),
    //         'payable_type' => $request->payor['payable_type']
    //     ]);
        
    //     return $trans;
    // }


    // public function models($modelType){
    //     return $modelType::with(['address'])->orderBy('id', 'asc');
    // }

    // public function transactable($modelType, $id){
        
    //    return $modelType::where('id',  $this->optimus()->encode($id))
    //         // ->whereHas('accessRights.roles.users', function($q){
    //         //     $q->where('users.id', Auth::User()->id);
    //         // })
    //         ->relTable()
    //         ->first()
    //         ->transactions;
            

    // }

    // public function items($modelType, $id){
    //     return $modelType::where('id', $id)
    //             ->whereHas('accessRights.roles.users', function($q){
    //                 // $q->where('users.id', Auth::User()->id);
    //             })
    //             ->relTable()
    //             ->first()
    //             ->items;
    // }

    // public function purchase($id){

    //     return Purchase::where('id', $id)
    //             // ->whereHas('accessRights.roles.users', function($q){
    //             //     $q->where('users.id', Auth::User()->id);
    //             // })
    //             ->relTable()
    //             ->first();
               
    // }

    // public function purchaseReceived($modelType, $id){
    //     return $modelType::where('id', $this->optimus()->encode($id))
    //             ->whereHas('accessRights.roles.users', function($q){
    //                 // $q->where('users.id', Auth::User()->id);
    //             })
    //             ->with(['purchaseReceived.saleInvoice', 'purchaseReceived.products'])
    //             ->first()
    //             ->purchaseReceived;
    // }

    // public function getAssignedInvoices ($modelType, $id){
    //     return $modelType::where('id', $this->optimus()->encode($id))
    //     ->whereHas('accessRights.roles.users', function($q){
    //         // $q->where('users.id', Auth::User()->id);
    //     })
    //     ->with(['saleInvoices'])
    //     ->first();
        
    // }

    // public function getAssignedProducts(){
    //     return Auth::User();
    // }


    // public function entityProducts($modelType, $id){
    //     return $modelType::where('id', $this->optimus()->encode($id))
    //             ->whereHas('accessRights.roles.users', function($q){
    //                 // $q->where('users.id', Auth::User()->id);
    //             })
    //             ->with(['products.taxType', 'products.chartAccount'])
    //             ->first()
    //             ->products;
    // }

    // public function userEntities($modelType){

    //    return  $modelType::whereHas('accessRights.roles.users', function($q){
    //         // $q->where('users.id', Auth::User()->id);
    //     })
    //     ->get();
        
    // }

    // public function entity($modelType, $id){
      
    //     return $modelType::where('id', $id)->with(['address.brgy', 'address.city', 'address.province', 'address.country'])->first();
        
    // }

    // public function chartAccounts($modelType, $id){

    //     return $modelType::where('id', $this->optimus()->encode($id))
    //                         // ->whereHas('company.chartAccounts.allChildren', function($q){
    //                         //     $q->where('parent_id', 0);
    //                         // })
    //                         ->with('company.chartAccounts')->first()->company->chartAccounts;
    // }

    // public function transactionTypes($modelType, $modelId){

    //     return $modelType::where('id', $modelId)->first()->businessInfo->accountingMethod->transactionTypes;
    // }

    // public function updateGeneralLedgers($request){

    //     $transaction = $this->find($request->id);
    //     foreach($request->generalLedgers as $gl){
            
    //         if( is_null($gl['id'])){
    //             $transaction->generalLedgers()->create([
    //                 'ledgerable_id' => $request->transaction['transactable_id'],
    //                 'ledgerable_type' => $request->transaction['transactable_type'],
    //                 'particulars' => $gl['particulars'],
    //                 'chart_account_id' => $gl['chart_account_id'],
    //                 'credit_amount' => $gl['credit_amount'],
    //                 'debit_amount' => $gl['debit_amount'],
    //                 'tax' => $gl['tax']
    //             ]);
    //         }else{
    //             $transaction->generalLedgers()->where('id', $gl['id'])->update([
    //                 'particulars' => $gl['particulars'],
    //                 'chart_account_id' => $gl['chart_account_id'],
    //                 'credit_amount' => $gl['credit_amount'],
    //                 'debit_amount' => $gl['debit_amount'],
    //                 'tax' => $gl['tax']
    //             ]);
    //         }
            
    //     }
    // }

    // public function payee($transactionId){
        
    //     $trans = $this->modelName->where('id', $transactionId)->first();
    //     if($trans->payee !== null){
    //         return $trans->payee->payable;
    //     }
        
    //     return null;
    // }

    // public function editPurchaseReceived($purchaseId){

    //     return PurchaseReceived::where('id', $purchaseId)->with('items')->first();
    // }

    // public function getVendorProducts($request){

    //     
    // }

}