<?php 

namespace App\Repo\Transaction;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Transaction;
class TransactionModelRepository extends BaseRepository implements TransactionInterface{

    public function __construct(){

        $this->modelName = new Transaction();
    }

    public function index($request){
        return $request->modelType::with(['address.country', 'address.region', 'address.province', 'address.city', 'address.brgy',])->orderBy('id', 'asc')->get();
    }

}