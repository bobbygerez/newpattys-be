<?php 

namespace App\Repo\VendorProduct;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\VendorProduct;
use App\Model\Vendor;

class VendorProductRepository extends BaseRepository implements VendorProductInterface{


    public function __construct(){

        $this->modelName = new VendorProduct();
    
    }

    public function index($request){

           $vendorProducts = $this->modelName->whereHas('vendor', function($q) use ($request){
                $q->where('token', $request->id);
           })
           ->with('companyItem')
           ->get();

           return $this->paginate( $vendorProducts );
    }

    public function store($request){
        $vendor = Vendor::where('token', $request->id)->first();
        $newRequest = $request->all();
        $newRequest['vendor_id'] = $vendor->id;
        $newRequest['company_item_id'] = $this->removeStringEncode($request->item_id);
        $this->modelName->create($newRequest);
    }

    public function update($request){
        $product = $this->find($request->id);
        $newRequest = $request->all();
        $newRequest['company_item_id'] = $this->removeStringEncode( $request->item_id );
        $product->update($newRequest);
    }   
}
