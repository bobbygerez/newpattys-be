<?php 

namespace App\Repo\VendorProduct;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\VendorProduct;

class VendorProductRestoreRepository extends BaseRepository implements VendorProductInterface{


    public function __construct(){

        $this->modelName = new VendorProduct();
    
    }

    public function index($request){

        $vendorProducts = $this->modelName->whereHas('vendor', function($q) use ($request){
             $q->where('token', $request->id);
        })
        ->withTrashed()
        ->when($request->filter !='', function($q) use ($request){
             $q->where('name', 'like', '%'.$request->filter.'%');
        })
        ->get();

        $vendorProducts = $vendorProducts->filter(function($val){
            return $val->deleted_at != null;
        });

        return $this->paginate( $vendorProducts );
 }

}
