<?php 

namespace App\Repo\Accessable;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Accessable;
use App\Model\User;
use App\Model\Branch;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class AccessableRepository extends BaseRepository implements AccessableInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Accessable();
    
    }

    public function store($request){

        foreach($request->access_right_id as $ar){
            $accessable = $this->modelName->create([
                'access_right_id' => $ar['value'],
                'accessable_id' => $this->optimus()->encode($request->accessable_id),
                'accessable_type' => $request->accessable_type,
                'menu_id' => $request->menu_id
            ]);

            $a = $this->modelName->find($accessable->id);
            $a->roles()->attach($a->id, [
                'role_id' => $request->role_id,
                'accessable_id' => $accessable->id
            ]);
        }
    }

    public function accessableModels(){

        if (Auth::User()->isSuperAdmin()) {
            // return $this->modelName->all()->unique('accessable_type')->values()->all();
            return $this->modelName->all()->unique('accessable_type')->values()->all();
        }
        // return Auth::User()->with('roles.accessRights')->first();
    }

    public function accessRightsModel($request){

        return User::where('id', $this->optimus()->encode($request->userId))
            ->with('roles.accessRights.'. $this->pluralName($request->modelType) )
            ->first();

    }

    public function pluralName($model){
        $assoc = [
            'Menu' => 'menus',
            'Holding' => 'holdings',
            'Company' => 'companies',
            'Branch' => 'branches',
            'Trademark' => 'trademarks',
            'Franchisee' => 'franchisees'
        ];

        return collect($assoc)->filter(function($value, $key) use ($model) {
            return $key === $model;
        })[$model];
    }

    public function getUserModelNames($request){
        
        // if (Auth::User()->isSuperAdmin()) {
        //    return $request->modelType::all();
        // }
        $user = User::find(Auth::User()->id)->with(['roles.accessables.accessable'])->first();
        return $accessables = collect($user->roles)->flatMap(function($val){
            return $val->accessables;
        })->filter(function($val) use ($request){
            return $val->accessable_type === $request->modelType;
        })->map(function($v){
            return $v->accessable;
        })->unique('id')->values()->all();
        
    }

    public function getAccessableModelName($request){

        return User::where('id', $this->optimus()->encode($request->userId))
        ->whereHas('roles.accessables', function($q) use ($request) {
            $q->where('access_id', $this->optimus()->encode($request->modelId));
        })
        ->with(['roles.accessables'])
        ->first();
    }
}