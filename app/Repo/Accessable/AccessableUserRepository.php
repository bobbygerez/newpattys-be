<?php 

namespace App\Repo\Accessable;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Accessable;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class AccessableUserRepository extends BaseRepository implements AccessableInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Accessable();
    
    }

    public function index($request){

        $user = User::where('id', Auth::User()->id)
            ->with('roles.accessables.menu.allChildren', 'roles.accessables.accessRight')
            ->first();
            
        $roles = $user->roles
                ->map(function($val){
                    return $val->accessables;
                })
                ->flatten(1)
                ->filter(function($val){
                    return $val->accessRight->name === 'Index';
                })
                ->map(function($val){
                    return $val->menu;
                })->sortBy('id')
                ->unique('id')
                ->values()->all();

        $min = collect($roles)->min('parent_id');
        $parentIds = collect( $this->flatten(collect($roles)->toArray()) )
                ->filter(function($val) use ($min) {
                    return $val['parent_id'] === $min;
                })->pluck('id');
        

        return collect($roles)->filter(function($val) use ($parentIds){
            return !in_array($val['parent_id'], $parentIds->toArray());
        })->values()->all();

        

        
    }

}