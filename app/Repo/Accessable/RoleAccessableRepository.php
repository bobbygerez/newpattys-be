<?php 

namespace App\Repo\Accessable;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Accessable;
use App\Model\Role;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class RoleAccessableRepository extends BaseRepository implements AccessableInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Accessable();
    
    }

    public function menuAccessRights($request){

        $role = Role::where('id', $request->roleId)->with(['accessables.menu', 'accessables.accessRight'])->first();

        $accessables = collect($role->accessables)->filter(function($val) use ($request){
            return $val->accessable_id == $this->optimus()->encode($request->modelId) && $val->accessable_type == $request->modelType;
        });

        return collect($accessables)->map(function($val){

            return [
                'access_right_name' => $val->accessRight['name'],
                'access_right_id' => $val->access_right_id,
                'menu_id' => $val->menu['id'],
                'menu_name' => $val->menu['name']
            ];
        });
    }

    public function addUpdate($request){

        $accessables = $this->modelName
        ->whereHas('roles', function($q) use ($request){
           $q->where('roles.id', $request->availableRole['value']);
        })
        ->where('accessable_id', $this->optimus()->encode($request->accessable_id))
        ->where('accessable_type', $request->accessable_type)
        ->where('menu_id', $request->menu_id)
        ->get();

        foreach($accessables as $accessable){
            $accessable->roles()->detach();
            $accessable->delete();
        }

        

        foreach($request->access_rights as $ar){
            $accessable = $this->modelName->create([
                'access_right_id' => $ar['value'],
                'menu_id' => $request->menu_id,
                'accessable_type' => $request->accessable_type,
                'accessable_id' => $this->optimus()->encode($request->accessable_id) 
            ]);

            $this->modelName->find($accessable->id)->roles()->attach($accessable->id, [
                'accessable_id' => $accessable->id,
                'role_id' => $request->availableRole['value']
            ]);
        }


        
        
    }


}