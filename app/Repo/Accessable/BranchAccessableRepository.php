<?php 

namespace App\Repo\Accessable;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Accessable;
use App\Model\User;
use App\Traits\Obfuscate\Optimuss;
use Auth;

class BranchAccessableRepository extends BaseRepository implements AccessableInterface{

    use Optimuss;

    public function __construct(){

        $this->modelName = new Accessable();
    
    }


}