<?php

namespace App\Providers;

use App\Http\Controllers\Api\Menu\MenuController;
use App\Http\Controllers\Api\Menu\AccessableMenuController;
use App\Http\Controllers\Api\Menu\DashboardMenuController;
use App\Http\Controllers\Api\Menu\ParentMenuController;
use App\Repo\Menu\MenuInterface;
use App\Repo\Menu\MenuRepository;
use App\Repo\Menu\AccessableMenuRepository;
use App\Repo\Menu\ParentMenuRepository;
use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(MenuController::class)
            ->needs(MenuInterface::class)
            ->give(MenuRepository::class);

        $this->app->when(DashboardMenuController::class)
            ->needs(MenuInterface::class)
            ->give(MenuRepository::class);
        
            $this->app->when(ParentMenuController::class)
            ->needs(MenuInterface::class)
            ->give(ParentMenuRepository::class);

            $this->app->when(AccessableMenuController::class)
            ->needs(MenuInterface::class)
            ->give(AccessableMenuRepository::class);
    }
}
