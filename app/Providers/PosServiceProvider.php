<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Pos\BranchItemTransController;
use App\Http\Controllers\Api\Pos\TransactionNoController;
use App\Repo\Pos\PosInterface;
use App\Repo\Pos\BranchItemTransRepository;
use App\Repo\Pos\TransactionNoRepository;
class PosServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
           $this->app->when(BranchItemTransController::class)
            ->needs(PosInterface::class)
            ->give(BranchItemTransRepository::class);

           $this->app->when(TransactionNoController::class)
            ->needs(PosInterface::class)
            ->give(TransactionNoRepository::class);
    }
}
