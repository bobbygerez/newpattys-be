<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\User\UserRestoreController;
use App\Http\Controllers\Api\User\UserRoleController;
use App\Http\Controllers\Api\User\UserMenuController;
use App\Http\Controllers\Api\User\UserRoleMenuController;
use App\Http\Controllers\Api\User\UserSearchInfoController;
use App\Repo\User\UserInterface;
use App\Repo\User\UserRepository;
use App\Repo\User\UserRestoreRepository;
use App\Repo\User\UserRoleRepository;
use App\Repo\User\UserMenuRepository;
use App\Repo\User\UserRoleMenuRepository;
use App\Repo\User\UserSearchInfoRepository;
class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserController::class)
        ->needs(UserInterface::class)
        ->give(UserRepository::class);

        $this->app->when(UserRestoreController::class)
        ->needs(UserInterface::class)
        ->give(UserRestoreRepository::class);

        $this->app->when(UserRoleController::class)
        ->needs(UserInterface::class)
        ->give(UserRoleRepository::class);

        $this->app->when(UserMenuController::class)
        ->needs(UserInterface::class)
        ->give(UserMenuRepository::class);

        $this->app->when(UserRoleMenuController::class)
        ->needs(UserInterface::class)
        ->give(UserRoleMenuRepository::class);

        $this->app->when(UserSearchInfoController::class)
        ->needs(UserInterface::class)
        ->give(UserSearchInfoRepository::class);
    }
}
