<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Area\AreaController;
use App\Repo\Area\AreaInterface;
use App\Repo\Area\AreaRepository;

class AreaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(AreaController::class)
        ->needs(AreaInterface::class)
        ->give(AreaRepository::class);
    }
}
