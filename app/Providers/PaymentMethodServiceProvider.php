<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\PaymentMethod\PaymentMethodController;
use App\Repo\PaymentMethod\PaymentMethodInterface;
use App\Repo\PaymentMethod\PaymentMethodRepository;

class PaymentMethodServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(PaymentMethodController::class)
        ->needs(PaymentMethodInterface::class)
        ->give(PaymentMethodRepository::class);
    }
}
