<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repo\Transaction\TransactionInterface;
use App\Repo\Transaction\TransactionRepository;
use App\Repo\Transaction\SalesJournalRepository;
use App\Repo\Transaction\GeneralJournalRepository;
use App\Repo\Transaction\DisbursementRepository;
use App\Http\Controllers\Api\Transaction\TransactionController;
use App\Http\Controllers\Api\Transaction\SaleJournalController;
use App\Http\Controllers\Api\Transaction\GeneralJournalController;
use App\Http\Controllers\Api\Transaction\DisbursementController;

use App\Http\Controllers\Api\Transaction\VendorProductController;
use App\Repo\Transaction\VendorProductRepository;

use App\Http\Controllers\Api\Transaction\TransactionModelController;
use App\Repo\Transaction\TransactionModelRepository;
class TransactionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->when(TransactionController::class)
      ->needs(TransactionInterface::class)
      ->give(TransactionRepository::class);
    
    $this->app->when(SaleJournalController::class)
      ->needs(TransactionInterface::class)
      ->give(SalesJournalRepository::class);

      $this->app->when(GeneralJournalController::class)
      ->needs(TransactionInterface::class)
      ->give(GeneralJournalRepository::class);

      $this->app->when(DisbursementController::class)
      ->needs(TransactionInterface::class)
      ->give(DisbursementRepository::class);

      $this->app->when(VendorProductController::class)
      ->needs(TransactionInterface::class)
      ->give(VendorProductRepository::class);

      $this->app->when(TransactionModelController::class)
      ->needs(TransactionInterface::class)
      ->give(TransactionModelRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      
    }
}
