<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Rack\RackController;
use App\Repo\Rack\RackInterface;
use App\Repo\Rack\RackRepository;

class RackServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         $this->app->when(RackController::class)
        ->needs(RackInterface::class)
        ->give(RackRepository::class);
    }
}
