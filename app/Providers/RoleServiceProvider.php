<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Role\RoleController;
use App\Http\Controllers\Api\Role\RoleRestoreController;
use App\Http\Controllers\Api\Role\DashboardRoleController;
use App\Http\Controllers\Api\Role\RoleAccessableController;
use App\Http\Controllers\Api\Role\RoleAccessableModelController;
use App\Repo\Role\RoleInterface;
use App\Repo\Role\RoleRepository;
use App\Repo\Role\RoleRestoreRepository;
use App\Repo\Role\DashboardRoleRepository;
use App\Repo\Role\RoleAccessableRepository;
use App\Repo\Role\RoleAccessableModelRepository;
class RoleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(RoleController::class)
        ->needs(RoleInterface::class)
        ->give(RoleRepository::class);

        $this->app->when(RoleRestoreController::class)
        ->needs(RoleInterface::class)
        ->give(RoleRestoreRepository::class);

        $this->app->when(DashboardRoleController::class)
        ->needs(RoleInterface::class)
        ->give(DashboardRoleRepository::class);
        
        $this->app->when(RoleAccessableController::class)
        ->needs(RoleInterface::class)
        ->give(RoleAccessableRepository::class);

        $this->app->when(RoleAccessableModelController::class)
        ->needs(RoleInterface::class)
        ->give(RoleAccessableModelRepository::class);
    }
}
