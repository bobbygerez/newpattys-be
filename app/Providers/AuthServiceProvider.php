<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model\Transaction' => 'App\Policies\TransactionPolicy',
        'App\Model\Employee' => 'App\Policies\EmployeePolicy',
        'App\Model\Information' => 'App\Policies\InformationPolicy',
        'App\Model\User' => 'App\Policies\UserPolicy',
        'App\Model\Category' => 'App\Policies\CategoryPolicy',
        'App\Model\Branch' => 'App\Policies\BranchPolicy',
        'App\Model\AccessRight' => 'App\Policies\AccessRightPolicy',
        'App\Model\Holding' => 'App\Policies\HoldingPolicy',
        'App\Model\Company' => 'App\Policies\CompanyPolicy',
        'App\Model\Role' => 'App\Policies\RolePolicy',
        'App\Model\Menu' => 'App\Policies\MenuPolicy',
        'App\Model\Trademark' => 'App\Policies\TrademarkPolicy',
        'App\Model\Franchisee' => 'App\Policies\FranchiseePolicy',
        'App\Model\Logistic' => 'App\Policies\LogisticPolicy',
        'App\Model\Commissary' => 'App\Policies\CommissaryPolicy',
        'App\Model\OtherVendor' => 'App\Policies\OtherVendorPolicy',
        'App\Model\Item' => 'App\Policies\ItemPolicy',
        'App\Model\Vendor' => 'App\Policies\VendorPolicy',
        'App\Model\Purchase' => 'App\Policies\PurchasePolicy',
        'App\Model\Ingredient' => 'App\Policies\IngredientPolicy',
        'App\Model\AccountingStandard' => 'App\Policies\AccountingStandardPolicy',
        'App\Model\ChartAccount' => 'App\Policies\ChartAccountPolicy',
        'App\Model\TransactionType' => 'App\Policies\TransactionTypePolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Gate::resource('transactions', 'App\Policies\TransactionPolicy');
        Gate::resource('employees', 'App\Policies\EmployeePolicy');
        Gate::resource('informations', 'App\Policies\InformationPolicy');
        Gate::resource('users', 'App\Policies\UserPolicy');
        Gate::resource('roles', 'App\Policies\RolePolicy');
        Gate::resource('menus', 'App\Policies\MenuPolicy');
        Gate::resource('access_rights', 'App\Policies\AccessRightPolicy');
        Gate::resource('holdings', 'App\Policies\HoldingPolicy');
        Gate::resource('companies', 'App\Policies\CompanyPolicy');
        Gate::resource('branches', 'App\Policies\BranchPolicy');
        Gate::resource('categories', 'App\Policies\CategoryPolicy');
        Gate::resource('trademarks', 'App\Policies\TrademarkPolicy');
        Gate::resource('franchisees', 'App\Policies\FranchiseePolicy');
        Gate::resource('logistics', 'App\Policies\LogisticPolicy');
        Gate::resource('commissaries', 'App\Policies\CommissaryPolicy');
        Gate::resource('other_vendors', 'App\Policies\OtherVendorPolicy');
        Gate::resource('items', 'App\Policies\ItemPolicy');
        Gate::resource('vendors', 'App\Policies\VendorPolicy');
        Gate::resource('purchases', 'App\Policies\PurchasePolicy');
        Gate::resource('ingredients', 'App\Policies\IngredientPolicy');
        Gate::resource('ingredient_items', 'App\Policies\IngredientPolicy');
        Gate::resource('account_standards', 'App\Policies\AccountingStandardPolicy');
        Gate::resource('chart_account', 'App\Policies\ChartAccountPolicy');
        Gate::resource('transaction_types', 'App\Policies\TransactionTypePolicy');
    }
}
