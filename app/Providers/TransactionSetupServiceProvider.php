<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\TransactionSetup\TransactionSetupController;
use App\Repo\TransactionSetup\TransactionSetupInterface;
use App\Repo\TransactionSetup\TransactionSetupRepository;
class TransactionSetupServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(TransactionSetupController::class)
        ->needs(TransactionSetupInterface::class)
        ->give(TransactionSetupRepository::class);
    }
}
