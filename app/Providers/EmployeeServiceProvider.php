<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Employee\EmployeeController;
use App\Http\Controllers\Api\Employee\EmployeeRestoreController;
use App\Http\Controllers\Api\Employee\EmployeeInformationController;
use App\Repo\Employee\EmployeeInterface;
use App\Repo\Employee\EmployeeRepository;
use App\Repo\Employee\EmployeeRestoreRepository;
use App\Repo\Employee\EmployeeInformationRepository;
class EmployeeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(EmployeeController::class)
        ->needs(EmployeeInterface::class)
        ->give(EmployeeRepository::class);
        
        $this->app->when(EmployeeInformationController::class)
        ->needs(EmployeeInterface::class)
        ->give(EmployeeInformationRepository::class);

        $this->app->when(EmployeeRestoreController::class)
        ->needs(EmployeeInterface::class)
        ->give(EmployeeRestoreRepository::class);
    }
}
