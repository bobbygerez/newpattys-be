<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\CompanyItem\CompanyItemController;
use App\Http\Controllers\Api\CompanyItem\CompanyItemRestoreController;
use App\Repo\CompanyItem\CompanyItemInterface;
use App\Repo\CompanyItem\CompanyItemRepository;
use App\Repo\CompanyItem\CompanyItemRestoreRepository;
class CompanyItemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(CompanyItemController::class)
        ->needs(CompanyItemInterface::class)
        ->give(CompanyItemRepository::class);

        $this->app->when(CompanyItemRestoreController::class)
        ->needs(CompanyItemInterface::class)
        ->give(CompanyItemRestoreRepository::class);
    }
}
