<?php

namespace App\Providers;

use App\Http\Controllers\Api\Accessable\AccessableController;
use App\Http\Controllers\Api\Accessable\AccessableUserController;
use App\Http\Controllers\Api\Accessable\BranchAccessableController;
use App\Http\Controllers\Api\Accessable\RoleAccessableController;
use App\Http\Controllers\Api\Accessable\UserAccessableController;
use App\Repo\Accessable\AccessableInterface;
use App\Repo\Accessable\AccessableRepository;
use App\Repo\Accessable\AccessableUserRepository;
use App\Repo\Accessable\BranchAccessableRepository;
use App\Repo\Accessable\RoleAccessableRepository;
use App\Repo\Accessable\UserAccessableRepository;
use Illuminate\Support\ServiceProvider;

class AccessableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(AccessableController::class)
            ->needs(AccessableInterface::class)
            ->give(AccessableRepository::class);

        $this->app->when(UserAccessableController::class)
            ->needs(AccessableInterface::class)
            ->give(UserAccessableRepository::class);

        $this->app->when(BranchAccessableController::class)
            ->needs(AccessableInterface::class)
            ->give(BranchAccessableRepository::class);

        $this->app->when(RoleAccessableController::class)
            ->needs(AccessableInterface::class)
            ->give(RoleAccessableRepository::class);

        $this->app->when(AccessableUserController::class)
            ->needs(AccessableInterface::class)
            ->give(AccessableUserRepository::class);
    }
}
