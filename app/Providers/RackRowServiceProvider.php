<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\RackRow\RackRowController;
use App\Repo\RackRow\RackRowInterface;
use App\Repo\RackRow\RackRowRepository;
class RackRowServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         $this->app->when(RackRowController::class)
        ->needs(RackRowInterface::class)
        ->give(RackRowRepository::class);
    }
}
