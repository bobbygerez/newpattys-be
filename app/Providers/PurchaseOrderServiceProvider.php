<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\PurchaseOrder\PurchaseOrderController;
use App\Repo\PurchaseOrder\PurchaseOrderInterface;
use App\Repo\PurchaseOrder\PurchaseOrderRepository;
class PurchaseOrderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(PurchaseOrderController::class)
        ->needs(PurchaseOrderInterface::class)
        ->give(PurchaseOrderRepository::class);

    }
}
