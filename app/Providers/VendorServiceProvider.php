<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Vendor\VendorController;
use App\Http\Controllers\Api\Vendor\VendorRestoreController;
use App\Repo\Vendor\VendorInterface;
use App\Repo\Vendor\VendorRepository;
use App\Repo\Vendor\VendorRestoreRepository;

class VendorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(VendorController::class)
        ->needs(VendorInterface::class)
        ->give(VendorRepository::class);

        $this->app->when(VendorRestoreController::class)
        ->needs(VendorInterface::class)
        ->give(VendorRestoreRepository::class);
    }
}
