<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Lender\LenderController;
use App\Repo\Lender\LenderInterface;
use App\Repo\Lender\LenderRepository;
class LenderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(LenderController::class)
        ->needs(LenderInterface::class)
        ->give(LenderRepository::class);
    }
}
