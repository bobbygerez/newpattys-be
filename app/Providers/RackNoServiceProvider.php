<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\RackNo\RackNoController;
use App\Repo\RackNo\RackNoInterface;
use App\Repo\RackNo\RackNoRepository;
class RackNoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(RackNoController::class)
        ->needs(RackNoInterface::class)
        ->give(RackNoRepository::class);
    }
}
