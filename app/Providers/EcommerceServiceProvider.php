<?php

namespace App\Providers;

use App\Http\Controllers\Api\Ecommerce\BranchItemController;
use App\Repo\Ecommerce\BranchItemRepository;
use App\Repo\Ecommerce\EcommerceInterface;
use Illuminate\Support\ServiceProvider;

class EcommerceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(BranchItemController::class)
            ->needs(EcommerceInterface::class)
            ->give(BranchItemRepository::class);
    }
}
