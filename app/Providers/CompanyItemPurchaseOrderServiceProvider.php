<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\CompanyItemPurchaseOrder\VendorController;
use App\Repo\CompanyItemPurchaseOrder\CompanyItemPurchaseOrderInterface;
use App\Repo\CompanyItemPurchaseOrder\CompanyItemPurchaseOrderRepository;
class CompanyItemPurchaseOrderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(VendorController::class)
        ->needs(CompanyItemPurchaseOrderInterface::class)
        ->give(CompanyItemPurchaseOrderRepository::class);
    }
}