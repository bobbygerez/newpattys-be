<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Company\CompanyController;
use App\Http\Controllers\Api\Company\AccountingStandardController;
use App\Http\Controllers\Api\Company\CompanyRestoreController;
use App\Http\Controllers\Api\Company\CompanyAccessableController;
use App\Repo\Company\CompanyInterface;
use App\Repo\Company\CompanyRepository;
use App\Repo\Company\AccountingStandardRepository;
use App\Repo\Company\CompanyRestoreRepository;
use App\Repo\Company\CompanyAccessableRepository;
class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(CompanyController::class)
        ->needs(CompanyInterface::class)
        ->give(CompanyRepository::class);

        $this->app->when(CompanyRestoreController::class)
        ->needs(CompanyInterface::class)
        ->give(CompanyRestoreRepository::class);
        
        $this->app->when(CompanyAccessableController::class)
        ->needs(CompanyInterface::class)
        ->give(CompanyAccessableRepository::class);

        $this->app->when(AccountingStandardController::class)
        ->needs(CompanyInterface::class)
        ->give(AccountingStandardRepository::class);
    }
}
