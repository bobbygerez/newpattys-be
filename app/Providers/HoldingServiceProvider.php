<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Holding\HoldingController;
use App\Http\Controllers\Api\Holding\HoldingAccessableController;
use App\Repo\Holding\HoldingInterface;
use App\Repo\Holding\HoldingRepository;
use App\Repo\Holding\HoldingAccessableRepository;
class HoldingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(HoldingController::class)
        ->needs(HoldingInterface::class)
        ->give(HoldingRepository::class);

        $this->app->when(HoldingAccessableController::class)
        ->needs(HoldingInterface::class)
        ->give(HoldingAccessableRepository::class);
    }
}
