<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\FreightPrice\FreightPriceController;
use App\Http\Controllers\Api\FreightPrice\BranchController;
use App\Http\Controllers\Api\FreightPrice\BranchFranchiseController;
use App\Http\Controllers\Api\FreightPrice\FreightPriceRestoreController;
use App\Repo\FreightPrice\FreightPriceInterface;
use App\Repo\FreightPrice\FreightPriceRepository;
use App\Repo\FreightPrice\BranchRepository;
use App\Repo\FreightPrice\BranchFranchiseRepository;
use App\Repo\FreightPrice\FreightPriceRestoreRepository;

class FreightPriceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->when(FreightPriceRestoreController::class)
        ->needs(FreightPriceInterface::class)
        ->give(FreightPriceRestoreRepository::class);

        $this->app->when(FreightPriceController::class)
        ->needs(FreightPriceInterface::class)
        ->give(FreightPriceRepository::class);
        
        $this->app->when(BranchController::class)
        ->needs(FreightPriceInterface::class)
        ->give(BranchRepository::class);

        $this->app->when(BranchFranchiseController::class)
        ->needs(FreightPriceInterface::class)
        ->give(BranchFranchiseRepository::class);
    }
}
