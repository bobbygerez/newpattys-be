<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\PurchaseReceived\PurchaseReceivedController;
use App\Repo\PurchaseReceived\PurchaseReceivedInterface;
use App\Repo\PurchaseReceived\PurchaseReceivedRepository;

use App\Http\Controllers\Api\PurchaseReceived\PurchasesController;
use App\Repo\PurchaseReceived\PurchasesRepository;
class PurchaseReceivedServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         $this->app->when(PurchaseReceivedController::class)
        ->needs(PurchaseReceivedInterface::class)
        ->give(PurchaseReceivedRepository::class);

         $this->app->when(PurchasesController::class)
        ->needs(PurchaseReceivedInterface::class)
        ->give(PurchasesRepository::class);
    }
}
