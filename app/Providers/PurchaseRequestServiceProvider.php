<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\PurchaseRequest\PurchaseRequestController;
use App\Http\Controllers\Api\PurchaseRequest\CompanyItemController;
use App\Http\Controllers\Api\PurchaseRequest\RestoreController;
use App\Repo\PurchaseRequest\PurchaseRequestInterface;
use App\Repo\PurchaseRequest\PurchaseRequestRepository;
use App\Repo\PurchaseRequest\CompanyItemRepository;
use App\Repo\PurchaseRequest\RestoreRepository;
class PurchaseRequestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(PurchaseRequestController::class)
        ->needs(PurchaseRequestInterface::class)
        ->give(PurchaseRequestRepository::class);

        $this->app->when(CompanyItemController::class)
        ->needs(PurchaseRequestInterface::class)
        ->give(CompanyItemRepository::class);

        $this->app->when(RestoreController::class)
        ->needs(PurchaseRequestInterface::class)
        ->give(RestoreRepository::class);
    }
}
