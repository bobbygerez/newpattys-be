<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\BranchChartAccount\BranchChartAccountController;
use App\Repo\BranchChartAccount\BranchChartAccountInterface;
use App\Repo\BranchChartAccount\BranchChartAccountRepository;

class BranchChartAccountServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(BranchChartAccountController::class)
        ->needs(BranchChartAccountInterface::class)
        ->give(BranchChartAccountRepository::class);
    }
}
