<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\VendorProduct\VendorProductController;
use App\Http\Controllers\Api\VendorProduct\VendorProductRestoreController;
use App\Repo\VendorProduct\VendorProductInterface;
use App\Repo\VendorProduct\VendorProductRepository;
use App\Repo\VendorProduct\VendorProductRestoreRepository;

class VendorProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(VendorProductController::class)
        ->needs(VendorProductInterface::class)
        ->give(VendorProductRepository::class);

        $this->app->when(VendorProductRestoreController::class)
        ->needs(VendorProductInterface::class)
        ->give(VendorProductRestoreRepository::class);
    }
}
