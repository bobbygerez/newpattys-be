<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\ProductTransfer\BranchController;
use App\Http\Controllers\Api\ProductTransfer\ProductTransferController;
use App\Http\Controllers\Api\ProductTransfer\DeliveryReceiptController;
use App\Repo\ProductTransfer\ProductTransferInterface;
use App\Repo\ProductTransfer\BranchRepository;
use App\Repo\ProductTransfer\ProductTransferRepository;
use App\Repo\ProductTransfer\DeliveryReceiptRepository;
class ProductTransferServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->when(DeliveryReceiptController::class)
            ->needs(ProductTransferInterface::class)
            ->give(DeliveryReceiptRepository::class);

        $this->app->when(BranchController::class)
            ->needs(ProductTransferInterface::class)
            ->give(BranchRepository::class);
        
        $this->app->when(ProductTransferController::class)
            ->needs(ProductTransferInterface::class)
            ->give(ProductTransferRepository::class);
    }
}
