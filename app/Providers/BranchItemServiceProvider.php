<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Http\Controllers\Api\BranchItem\BranchItemController;
use App\Http\Controllers\Api\BranchItem\RestoreController;
use App\Http\Controllers\Api\BranchItem\ProductController;
use App\Repo\BranchItem\BranchItemInterface;
use App\Repo\BranchItem\BranchItemRepository;
use App\Repo\BranchItem\RestoreRepository;
use App\Repo\BranchItem\ProductRepository;
class BranchItemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(BranchItemController::class)
        ->needs(BranchItemInterface::class)
        ->give(BranchItemRepository::class);

        $this->app->when(RestoreController::class)
        ->needs(BranchItemInterface::class)
        ->give(RestoreRepository::class);
        
        $this->app->when(ProductController::class)
        ->needs(BranchItemInterface::class)
        ->give(ProductRepository::class);
    }
}
