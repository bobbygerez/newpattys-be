<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\AccessRight\AccessRightController;
use App\Http\Controllers\Api\AccessRight\ARHoldingController;
use App\Repo\AccessRight\AccessRightInterface;
use App\Repo\AccessRight\AccessRightRepository;
use App\Repo\AccessRight\ARHoldingRepository;
class AccessRightServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(AccessRightController::class)
        ->needs(AccessRightInterface::class)
        ->give(AccessRightRepository::class);

        
        $this->app->when(ARHoldingController::class)
        ->needs(AccessRightInterface::class)
        ->give(ARHoldingRepository::class);

    }
}
