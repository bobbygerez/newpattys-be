<?php

namespace App\Providers;

use App\Http\Controllers\Api\Information\InformationController;
use App\Http\Controllers\Api\Information\InformationRestoreController;
use App\Http\Controllers\Api\Information\InformationEmployeeController;
use App\Repo\Information\InformationInterface;
use App\Repo\Information\InformationRepository;
use App\Repo\Information\InformationRestoreRepository;
use App\Repo\Information\InformationEmployeeRepository;
use Illuminate\Support\ServiceProvider;

class InformationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        
        $this->app->when(InformationController::class)
            ->needs(InformationInterface::class)
            ->give(InformationRepository::class);
        
        $this->app->when(InformationEmployeeController::class)
            ->needs(InformationInterface::class)
            ->give(InformationEmployeeRepository::class);
        
            $this->app->when(InformationRestoreController::class)
            ->needs(InformationInterface::class)
            ->give(InformationRestoreRepository::class);
    }
}
