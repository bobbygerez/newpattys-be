<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Shifting\ShiftingController;
use App\Repo\Shifting\ShiftingInterface;
use App\Repo\Shifting\ShiftingRepository;

class ShiftingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(ShiftingController::class)
        ->needs(ShiftingInterface::class)
        ->give(ShiftingRepository::class);
    }
}
