<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Receipt\ChartAccountController;
use App\Http\Controllers\Api\Receipt\ReceiptProductController;
use App\Repo\Receipt\ReceiptInterface;
use App\Repo\Receipt\ChartAccountRepository;
use App\Repo\Receipt\ReceiptProductRepository;

class ReceiptServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(ChartAccountController::class)
        ->needs(ReceiptInterface::class)
        ->give(ChartAccountRepository::class);

        $this->app->when(ReceiptProductController::class)
        ->needs(ReceiptInterface::class)
        ->give(ReceiptProductRepository::class);

    }
}
