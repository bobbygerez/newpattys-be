<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Vendor;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\Obfuscate\OptimusPolicy;

class VendorPolicy
{
    use HandlesAuthorization, OptimusPolicy;

    public function index(User $user)
    {

        return $this->accessable('Index', 'Vendors');

    }

    /**
     * Determine whether the user can view the vendor.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Vendor  $vendor
     * @return mixed
     */
    public function view(User $user, Vendor $vendor)
    {
        return $this->accessable('View', 'Vendors');
    }

    /**
     * Determine whether the user can create vendors.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->accessable('Create', 'Vendors');
    }

    /**
     * Determine whether the user can update the vendor.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Vendor  $vendor
     * @return mixed
     */
    public function update(User $user, Vendor $vendor)
    {
        return $this->accessable('Update', 'Vendors');
    }

    /**
     * Determine whether the user can delete the vendor.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Vendor  $vendor
     * @return mixed
     */
    public function delete(User $user, Vendor $vendor)
    {
        return $this->accessable('Delete', 'Vendors');
    }

    /**
     * Determine whether the user can restore the vendor.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Vendor  $vendor
     * @return mixed
     */
    public function restore(User $user, Vendor $vendor)
    {
        return $this->accessable('Restore', 'Vendors');
    }

    /**
     * Determine whether the user can permanently delete the vendor.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Vendor  $vendor
     * @return mixed
     */
    public function forceDelete(User $user, Vendor $vendor)
    {
       return false;
    }
}
