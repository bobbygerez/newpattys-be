<?php

namespace App\Policies;

use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\Obfuscate\OptimusPolicy;
use Auth;
class UserPolicy
{
    use HandlesAuthorization, OptimusPolicy;

    public function index(User $user)
    {

        return $this->accessable('Index', 'Users');

    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $this->accessable('View', 'Users');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        
        return $this->accessable('Update', 'Users');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return $this->accessable('Delete', 'Users');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return $this->accessable('Restore', 'Users');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return $this->accessable('Force Delete', 'Users');
    }

    
}
