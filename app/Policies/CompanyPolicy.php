<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Company;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\Obfuscate\OptimusPolicy;
class CompanyPolicy
{
    use HandlesAuthorization, OptimusPolicy;

    public function index(User $user)
    {

        return $this->accessable('Index', 'Companies');

    }

    /**
     * Determine whether the user can view the company.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function view(User $user, Company $company)
    {
        return $this->accessable('View', 'Companies');
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->accessable('Create', 'Companies');
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function update(User $user, Company $company)
    {
        return $this->accessable('Update', 'Companies');
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function delete(User $user, Company $company)
    {
        return $this->accessable('Delete', 'Companies');
    }

    /**
     * Determine whether the user can restore the company.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function restore(User $user, Company $company)
    {
        return $this->accessable('Restore', 'Companies');
    }

    /**
     * Determine whether the user can permanently delete the company.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function forceDelete(User $user, Company $company)
    {
        return true;
    }
}
