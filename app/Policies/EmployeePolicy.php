<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Employee;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\Obfuscate\OptimusPolicy;
class EmployeePolicy
{
    use HandlesAuthorization, OptimusPolicy;
    
    public function index(User $user)
    {

        return $this->accessable('Index', 'Employees');

    }

    /**
     * Determine whether the user can view any employees.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the employee.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Employee  $employee
     * @return mixed
     */
    public function view(User $user, Employee $employee)
    {
        return $this->accessable('View', 'Employees');
    }

    /**
     * Determine whether the user can create employees.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->accessable('Create', 'Employees');
    }

    /**
     * Determine whether the user can update the employee.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Employee  $employee
     * @return mixed
     */
    public function update(User $user, Employee $employee)
    {
        return $this->accessable('Update', 'Employees');
    }

    /**
     * Determine whether the user can delete the employee.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Employee  $employee
     * @return mixed
     */
    public function delete(User $user, Employee $employee)
    {
        return $this->accessable('Delete', 'Employees');
    }

    /**
     * Determine whether the user can restore the employee.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Employee  $employee
     * @return mixed
     */
    public function restore(User $user, Employee $employee)
    {
        return $this->accessable('Restore', 'Employees');
    }

    /**
     * Determine whether the user can permanently delete the employee.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Employee  $employee
     * @return mixed
     */
    public function forceDelete(User $user, Employee $employee)
    {
        return $this->accessable('Force Delete', 'Employees');
    }
}
