<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Role;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\Obfuscate\OptimusPolicy;
use Auth;
class RolePolicy
{
    use HandlesAuthorization, OptimusPolicy;

    public function index(User $user)
    {

        return $this->accessable('Index', 'Roles');

    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Role  $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
       return $this->accessable('View', 'Roles');
        
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user, Role $role)
    {
        return $this->accessable('Create', 'Roles');
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Role  $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return $this->accessable('Update', 'Roles');
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Role  $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        return $this->accessable('Delete', 'Roles');
    }

    /**
     * Determine whether the user can restore the role.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Role  $role
     * @return mixed
     */
    public function restore(User $user, Role $role)
    {
        return $this->accessable('Restore', 'Roles');
    }

    /**
     * Determine whether the user can permanently delete the role.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Role  $role
     * @return mixed
     */
    public function forceDelete(User $user, Role $role)
    {
        return false;
    }

    
}
