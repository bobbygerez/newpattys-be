<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Information;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\Obfuscate\OptimusPolicy;

class InformationPolicy
{

    
    use HandlesAuthorization, OptimusPolicy;

    public function index(User $user)
    {

        return $this->accessable('Index', 'KYC');

    }
    
    /**
     * Determine whether the user can view any information.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the information.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Information  $information
     * @return mixed
     */
    public function view(User $user, Information $information)
    {
        return $this->accessable('View', 'KYC');
    }

    /**
     * Determine whether the user can create information.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->accessable('Create', 'KYC');
    }

    /**
     * Determine whether the user can update the information.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Information  $information
     * @return mixed
     */
    public function update(User $user, Information $information)
    {
        return $this->accessable('Update', 'KYC');
    }

    /**
     * Determine whether the user can delete the information.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Information  $information
     * @return mixed
     */
    public function delete(User $user, Information $information)
    {
        return $this->accessable('Delete', 'KYC');
    }

    /**
     * Determine whether the user can restore the information.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Information  $information
     * @return mixed
     */
    public function restore(User $user, Information $information)
    {
        return $this->accessable('Restore', 'KYC');
    }

    /**
     * Determine whether the user can permanently delete the information.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Information  $information
     * @return mixed
     */
    public function forceDelete(User $user, Information $information)
    {
        return $this->accessable('Force Delete', 'KYC');
    }
}
