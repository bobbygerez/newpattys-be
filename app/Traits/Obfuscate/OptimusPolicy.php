<?php 

namespace App\Traits\Obfuscate;
use Jenssegers\Optimus\Optimus;
use App\Model\User;
use Auth;
trait OptimusPolicy
{
    
    public function optimus(){
        return new Optimus(1580030173, 59260789, 1163945558);

    }

    public function removeStringEncode($val){
        $res = preg_replace("/[^0-9]/", "", $val);
        return $this->optimus()->encode((int)$res);
    }

    public function accessable($accessRight, $menu){

        $request = app()->make('request');

        $user = User::where('id', Auth::User()->id)
            ->with(['roles.accessables.accessRight', 'roles.accessables.menu'])->first();

        $accessables = collect($user->roles)->map(function($val){
            return $val->accessables;
        })->flatten(1)->filter(function($val) use ($request, $accessRight, $menu) {
            return $val->accessRight['name'] == $accessRight && $val->Menu['name'] == $menu && $val->accessable_type == $request->accessable_type; //&& $this->removeStringEncode( $request->accessable_id );
        })->values()->all();

        if(count($accessables) > 0){
            return true;
        }
        return false;
       
    }

}